<?php
/* @var $this TeachersController */
/* @var $data Teachers */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('NameCN')); ?>:</b>
	<?php echo CHtml::encode($data->NameCN); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('NameEN')); ?>:</b>
	<?php echo CHtml::encode($data->NameEN); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('PicPath')); ?>:</b>
	<?php echo CHtml::encode($data->PicPath); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('subject')); ?>:</b>
	<?php echo CHtml::encode($data->subject); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('status')); ?>:</b>
	<?php echo CHtml::encode($data->status); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Type')); ?>:</b>
	<?php echo CHtml::encode($data->Type); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('webImg')); ?>:</b>
	<?php echo CHtml::encode($data->webImg); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('sorting')); ?>:</b>
	<?php echo CHtml::encode($data->sorting); ?>
	<br />

	*/ ?>

</div>