<?php
/* @var $this TeachersController */
/* @var $model Teachers */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'teachers-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'NameCN'); ?>
		<?php echo $form->textField($model,'NameCN',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'NameCN'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'NameEN'); ?>
		<?php echo $form->textField($model,'NameEN',array('size'=>40,'maxlength'=>40)); ?>
		<?php echo $form->error($model,'NameEN'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'PicPath'); ?>
		<?php echo $form->textField($model,'PicPath',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'PicPath'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'subject'); ?>
		<?php echo $form->textField($model,'subject',array('size'=>60,'maxlength'=>60)); ?>
		<?php echo $form->error($model,'subject'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'status'); ?>
		<?php echo $form->textField($model,'status',array('size'=>1,'maxlength'=>1)); ?>
		<?php echo $form->error($model,'status'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'Type'); ?>
		<?php echo $form->textField($model,'Type',array('size'=>30,'maxlength'=>30)); ?>
		<?php echo $form->error($model,'Type'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'webImg'); ?>
		<?php echo $form->textField($model,'webImg',array('size'=>30,'maxlength'=>30)); ?>
		<?php echo $form->error($model,'webImg'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'sorting'); ?>
		<?php echo $form->textField($model,'sorting'); ?>
		<?php echo $form->error($model,'sorting'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->