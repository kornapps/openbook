<?php
/* @var $this SubjectsController */
/* @var $data Subjects */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('NameCN')); ?>:</b>
	<?php echo CHtml::encode($data->NameCN); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('NameEN')); ?>:</b>
	<?php echo CHtml::encode($data->NameEN); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('PicPath')); ?>:</b>
	<?php echo CHtml::encode($data->PicPath); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Type')); ?>:</b>
	<?php echo CHtml::encode($data->Type); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('webImg')); ?>:</b>
	<?php echo CHtml::encode($data->webImg); ?>
	<br />


</div>