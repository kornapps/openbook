<?php
/* @var $this QuestionsController */
/* @var $model Questions */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'questions-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'SeqNo'); ?>
		<?php echo $form->textField($model,'SeqNo'); ?>
		<?php echo $form->error($model,'SeqNo'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'SubjectCode'); ?>
		<?php echo $form->textField($model,'SubjectCode',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'SubjectCode'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'TeacherCode'); ?>
		<?php echo $form->textField($model,'TeacherCode',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'TeacherCode'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'mStatus'); ?>
		<?php echo $form->textField($model,'mStatus',array('size'=>1,'maxlength'=>1)); ?>
		<?php echo $form->error($model,'mStatus'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'GroupID'); ?>
		<?php echo $form->textField($model,'GroupID',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'GroupID'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'DescriptionCN'); ?>
		<?php echo $form->textArea($model,'DescriptionCN',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'DescriptionCN'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'DescriptionEN'); ?>
		<?php echo $form->textArea($model,'DescriptionEN',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'DescriptionEN'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'PicPath'); ?>
		<?php echo $form->textField($model,'PicPath',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'PicPath'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'YouTubePath'); ?>
		<?php echo $form->textField($model,'YouTubePath',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'YouTubePath'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'CorrectAnswer'); ?>
		<?php echo $form->textField($model,'CorrectAnswer',array('size'=>1,'maxlength'=>1)); ?>
		<?php echo $form->error($model,'CorrectAnswer'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ChoiceACN'); ?>
		<?php echo $form->textArea($model,'ChoiceACN',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'ChoiceACN'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ChoiceACNPath'); ?>
		<?php echo $form->textField($model,'ChoiceACNPath',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'ChoiceACNPath'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ChoiceAEN'); ?>
		<?php echo $form->textArea($model,'ChoiceAEN',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'ChoiceAEN'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ChoiceAENPath'); ?>
		<?php echo $form->textField($model,'ChoiceAENPath',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'ChoiceAENPath'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ChoiceBCN'); ?>
		<?php echo $form->textArea($model,'ChoiceBCN',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'ChoiceBCN'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ChoiceBCNPath'); ?>
		<?php echo $form->textField($model,'ChoiceBCNPath',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'ChoiceBCNPath'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ChoiceBEN'); ?>
		<?php echo $form->textArea($model,'ChoiceBEN',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'ChoiceBEN'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ChoiceBENPath'); ?>
		<?php echo $form->textField($model,'ChoiceBENPath',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'ChoiceBENPath'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ChoiceCCN'); ?>
		<?php echo $form->textArea($model,'ChoiceCCN',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'ChoiceCCN'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ChoiceCCNPath'); ?>
		<?php echo $form->textField($model,'ChoiceCCNPath',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'ChoiceCCNPath'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ChoiceCEN'); ?>
		<?php echo $form->textArea($model,'ChoiceCEN',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'ChoiceCEN'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ChoiceCENPath'); ?>
		<?php echo $form->textField($model,'ChoiceCENPath',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'ChoiceCENPath'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ChoiceDCN'); ?>
		<?php echo $form->textArea($model,'ChoiceDCN',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'ChoiceDCN'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ChoiceDCNPath'); ?>
		<?php echo $form->textField($model,'ChoiceDCNPath',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'ChoiceDCNPath'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ChoiceDEN'); ?>
		<?php echo $form->textArea($model,'ChoiceDEN',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'ChoiceDEN'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ChoiceDENPath'); ?>
		<?php echo $form->textField($model,'ChoiceDENPath',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'ChoiceDENPath'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ChoiceECN'); ?>
		<?php echo $form->textArea($model,'ChoiceECN',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'ChoiceECN'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ChoiceECNPath'); ?>
		<?php echo $form->textField($model,'ChoiceECNPath',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'ChoiceECNPath'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ChoiceEEN'); ?>
		<?php echo $form->textArea($model,'ChoiceEEN',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'ChoiceEEN'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ChoiceEENPath'); ?>
		<?php echo $form->textField($model,'ChoiceEENPath',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'ChoiceEENPath'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'CreatedTime'); ?>
		<?php echo $form->textField($model,'CreatedTime'); ?>
		<?php echo $form->error($model,'CreatedTime'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'CreateUser'); ?>
		<?php echo $form->textField($model,'CreateUser',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'CreateUser'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'EditTime'); ?>
		<?php echo $form->textField($model,'EditTime'); ?>
		<?php echo $form->error($model,'EditTime'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'EditUser'); ?>
		<?php echo $form->textField($model,'EditUser',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'EditUser'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->