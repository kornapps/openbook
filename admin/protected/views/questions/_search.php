<?php
/* @var $this QuestionsController */
/* @var $model Questions */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'Code'); ?>
		<?php echo $form->textField($model,'Code',array('size'=>11,'maxlength'=>11)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'SeqNo'); ?>
		<?php echo $form->textField($model,'SeqNo'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'SubjectCode'); ?>
		<?php echo $form->textField($model,'SubjectCode',array('size'=>20,'maxlength'=>20)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'TeacherCode'); ?>
		<?php echo $form->textField($model,'TeacherCode',array('size'=>20,'maxlength'=>20)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'mStatus'); ?>
		<?php echo $form->textField($model,'mStatus',array('size'=>1,'maxlength'=>1)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'GroupID'); ?>
		<?php echo $form->textField($model,'GroupID',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'DescriptionCN'); ?>
		<?php echo $form->textArea($model,'DescriptionCN',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'DescriptionEN'); ?>
		<?php echo $form->textArea($model,'DescriptionEN',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'PicPath'); ?>
		<?php echo $form->textField($model,'PicPath',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'YouTubePath'); ?>
		<?php echo $form->textField($model,'YouTubePath',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'CorrectAnswer'); ?>
		<?php echo $form->textField($model,'CorrectAnswer',array('size'=>1,'maxlength'=>1)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ChoiceACN'); ?>
		<?php echo $form->textArea($model,'ChoiceACN',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ChoiceACNPath'); ?>
		<?php echo $form->textField($model,'ChoiceACNPath',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ChoiceAEN'); ?>
		<?php echo $form->textArea($model,'ChoiceAEN',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ChoiceAENPath'); ?>
		<?php echo $form->textField($model,'ChoiceAENPath',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ChoiceBCN'); ?>
		<?php echo $form->textArea($model,'ChoiceBCN',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ChoiceBCNPath'); ?>
		<?php echo $form->textField($model,'ChoiceBCNPath',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ChoiceBEN'); ?>
		<?php echo $form->textArea($model,'ChoiceBEN',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ChoiceBENPath'); ?>
		<?php echo $form->textField($model,'ChoiceBENPath',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ChoiceCCN'); ?>
		<?php echo $form->textArea($model,'ChoiceCCN',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ChoiceCCNPath'); ?>
		<?php echo $form->textField($model,'ChoiceCCNPath',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ChoiceCEN'); ?>
		<?php echo $form->textArea($model,'ChoiceCEN',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ChoiceCENPath'); ?>
		<?php echo $form->textField($model,'ChoiceCENPath',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ChoiceDCN'); ?>
		<?php echo $form->textArea($model,'ChoiceDCN',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ChoiceDCNPath'); ?>
		<?php echo $form->textField($model,'ChoiceDCNPath',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ChoiceDEN'); ?>
		<?php echo $form->textArea($model,'ChoiceDEN',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ChoiceDENPath'); ?>
		<?php echo $form->textField($model,'ChoiceDENPath',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ChoiceECN'); ?>
		<?php echo $form->textArea($model,'ChoiceECN',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ChoiceECNPath'); ?>
		<?php echo $form->textField($model,'ChoiceECNPath',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ChoiceEEN'); ?>
		<?php echo $form->textArea($model,'ChoiceEEN',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ChoiceEENPath'); ?>
		<?php echo $form->textField($model,'ChoiceEENPath',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'CreatedTime'); ?>
		<?php echo $form->textField($model,'CreatedTime'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'CreateUser'); ?>
		<?php echo $form->textField($model,'CreateUser',array('size'=>20,'maxlength'=>20)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'EditTime'); ?>
		<?php echo $form->textField($model,'EditTime'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'EditUser'); ?>
		<?php echo $form->textField($model,'EditUser',array('size'=>20,'maxlength'=>20)); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->