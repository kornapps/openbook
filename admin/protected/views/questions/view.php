<?php
/* @var $this QuestionsController */
/* @var $model Questions */

$this->breadcrumbs=array(
	'Questions'=>array('index'),
	$model->Code,
);

$this->menu=array(
	array('label'=>'List Questions', 'url'=>array('index')),
	array('label'=>'Create Questions', 'url'=>array('create')),
	array('label'=>'Update Questions', 'url'=>array('update', 'id'=>$model->Code)),
	array('label'=>'Delete Questions', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->Code),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Questions', 'url'=>array('admin')),
);
?>

<h1>View Questions #<?php echo $model->Code; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'Code',
		'SeqNo',
		'SubjectCode',
		'TeacherCode',
		'mStatus',
		'GroupID',
		'DescriptionCN',
		'DescriptionEN',
		'PicPath',
		'YouTubePath',
		'CorrectAnswer',
		'ChoiceACN',
		'ChoiceACNPath',
		'ChoiceAEN',
		'ChoiceAENPath',
		'ChoiceBCN',
		'ChoiceBCNPath',
		'ChoiceBEN',
		'ChoiceBENPath',
		'ChoiceCCN',
		'ChoiceCCNPath',
		'ChoiceCEN',
		'ChoiceCENPath',
		'ChoiceDCN',
		'ChoiceDCNPath',
		'ChoiceDEN',
		'ChoiceDENPath',
		'ChoiceECN',
		'ChoiceECNPath',
		'ChoiceEEN',
		'ChoiceEENPath',
		'CreatedTime',
		'CreateUser',
		'EditTime',
		'EditUser',
	),
)); ?>
