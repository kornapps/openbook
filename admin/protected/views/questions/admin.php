<?php
/* @var $this QuestionsController */
/* @var $model Questions */

$this->breadcrumbs=array(
	'Questions'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Questions', 'url'=>array('index')),
	array('label'=>'Create Questions', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#questions-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Questions</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php print_r($model->FindByPk(3)->teacher);?>
<?php //print_r(Teachers::Model()->FindByPk(3)->questions);?>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'questions-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		// 'Code',
		'SubjectCode',
		array(
			'header'=>'Subject',
            'type' => 'raw',
            'value' => 'CHtml::encode(Subjects::Model()->FindByPk($data->SubjectCode)->NameEN)'

			// 'value'=>Teachers::Model()->FindByPk(5)->NameCN
			// 'value'=>0
			),		
		'TeacherCode',
		'GroupID',
		array(
			'header'=>'Teacher',
            'type' => 'raw',
            'value' => 'CHtml::encode(Teachers::Model()->FindByPk($data->TeacherCode)->NameCN)'

			// 'value'=>Teachers::Model()->FindByPk(5)->NameCN
			// 'value'=>0
			),
		'SeqNo',
		'mStatus',

		/*
		'DescriptionCN',
		'DescriptionEN',
		'PicPath',
		'YouTubePath',
		'CorrectAnswer',
		'ChoiceACN',
		'ChoiceACNPath',
		'ChoiceAEN',
		'ChoiceAENPath',
		'ChoiceBCN',
		'ChoiceBCNPath',
		'ChoiceBEN',
		'ChoiceBENPath',
		'ChoiceCCN',
		'ChoiceCCNPath',
		'ChoiceCEN',
		'ChoiceCENPath',
		'ChoiceDCN',
		'ChoiceDCNPath',
		'ChoiceDEN',
		'ChoiceDENPath',
		'ChoiceECN',
		'ChoiceECNPath',
		'ChoiceEEN',
		'ChoiceEENPath',
		'CreatedTime',
		'CreateUser',
		'EditTime',
		'EditUser',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
