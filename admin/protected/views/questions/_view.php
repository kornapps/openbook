<?php
/* @var $this QuestionsController */
/* @var $data Questions */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('Code')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->Code), array('view', 'id'=>$data->Code)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('SeqNo')); ?>:</b>
	<?php echo CHtml::encode($data->SeqNo); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('SubjectCode')); ?>:</b>
	<?php echo CHtml::encode($data->SubjectCode); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('TeacherCode')); ?>:</b>
	<?php echo CHtml::encode($data->TeacherCode); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('mStatus')); ?>:</b>
	<?php echo CHtml::encode($data->mStatus); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('GroupID')); ?>:</b>
	<?php echo CHtml::encode($data->GroupID); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('DescriptionCN')); ?>:</b>
	<?php echo CHtml::encode($data->DescriptionCN); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('DescriptionEN')); ?>:</b>
	<?php echo CHtml::encode($data->DescriptionEN); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('PicPath')); ?>:</b>
	<?php echo CHtml::encode($data->PicPath); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('YouTubePath')); ?>:</b>
	<?php echo CHtml::encode($data->YouTubePath); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('CorrectAnswer')); ?>:</b>
	<?php echo CHtml::encode($data->CorrectAnswer); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ChoiceACN')); ?>:</b>
	<?php echo CHtml::encode($data->ChoiceACN); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ChoiceACNPath')); ?>:</b>
	<?php echo CHtml::encode($data->ChoiceACNPath); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ChoiceAEN')); ?>:</b>
	<?php echo CHtml::encode($data->ChoiceAEN); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ChoiceAENPath')); ?>:</b>
	<?php echo CHtml::encode($data->ChoiceAENPath); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ChoiceBCN')); ?>:</b>
	<?php echo CHtml::encode($data->ChoiceBCN); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ChoiceBCNPath')); ?>:</b>
	<?php echo CHtml::encode($data->ChoiceBCNPath); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ChoiceBEN')); ?>:</b>
	<?php echo CHtml::encode($data->ChoiceBEN); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ChoiceBENPath')); ?>:</b>
	<?php echo CHtml::encode($data->ChoiceBENPath); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ChoiceCCN')); ?>:</b>
	<?php echo CHtml::encode($data->ChoiceCCN); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ChoiceCCNPath')); ?>:</b>
	<?php echo CHtml::encode($data->ChoiceCCNPath); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ChoiceCEN')); ?>:</b>
	<?php echo CHtml::encode($data->ChoiceCEN); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ChoiceCENPath')); ?>:</b>
	<?php echo CHtml::encode($data->ChoiceCENPath); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ChoiceDCN')); ?>:</b>
	<?php echo CHtml::encode($data->ChoiceDCN); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ChoiceDCNPath')); ?>:</b>
	<?php echo CHtml::encode($data->ChoiceDCNPath); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ChoiceDEN')); ?>:</b>
	<?php echo CHtml::encode($data->ChoiceDEN); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ChoiceDENPath')); ?>:</b>
	<?php echo CHtml::encode($data->ChoiceDENPath); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ChoiceECN')); ?>:</b>
	<?php echo CHtml::encode($data->ChoiceECN); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ChoiceECNPath')); ?>:</b>
	<?php echo CHtml::encode($data->ChoiceECNPath); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ChoiceEEN')); ?>:</b>
	<?php echo CHtml::encode($data->ChoiceEEN); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ChoiceEENPath')); ?>:</b>
	<?php echo CHtml::encode($data->ChoiceEENPath); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('CreatedTime')); ?>:</b>
	<?php echo CHtml::encode($data->CreatedTime); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('CreateUser')); ?>:</b>
	<?php echo CHtml::encode($data->CreateUser); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('EditTime')); ?>:</b>
	<?php echo CHtml::encode($data->EditTime); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('EditUser')); ?>:</b>
	<?php echo CHtml::encode($data->EditUser); ?>
	<br />

	*/ ?>

</div>