<?php

/**
 * This is the model class for table "mQuestions".
 *
 * The followings are the available columns in table 'mQuestions':
 * @property string $Code
 * @property integer $SeqNo
 * @property string $SubjectCode
 * @property string $TeacherCode
 * @property string $mStatus
 * @property string $GroupID
 * @property string $DescriptionCN
 * @property string $DescriptionEN
 * @property string $PicPath
 * @property string $YouTubePath
 * @property string $CorrectAnswer
 * @property string $ChoiceACN
 * @property string $ChoiceACNPath
 * @property string $ChoiceAEN
 * @property string $ChoiceAENPath
 * @property string $ChoiceBCN
 * @property string $ChoiceBCNPath
 * @property string $ChoiceBEN
 * @property string $ChoiceBENPath
 * @property string $ChoiceCCN
 * @property string $ChoiceCCNPath
 * @property string $ChoiceCEN
 * @property string $ChoiceCENPath
 * @property string $ChoiceDCN
 * @property string $ChoiceDCNPath
 * @property string $ChoiceDEN
 * @property string $ChoiceDENPath
 * @property string $ChoiceECN
 * @property string $ChoiceECNPath
 * @property string $ChoiceEEN
 * @property string $ChoiceEENPath
 * @property string $CreatedTime
 * @property string $CreateUser
 * @property string $EditTime
 * @property string $EditUser
 */
class Questions extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'mQuestions';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('SeqNo', 'numerical', 'integerOnly'=>true),
			array('SubjectCode, TeacherCode, CreateUser, EditUser', 'length', 'max'=>20),
			array('mStatus, CorrectAnswer', 'length', 'max'=>1),
			array('GroupID', 'length', 'max'=>10),
			array('PicPath, YouTubePath, ChoiceACNPath, ChoiceAENPath, ChoiceBCNPath, ChoiceBENPath, ChoiceCCNPath, ChoiceCENPath, ChoiceDCNPath, ChoiceDENPath, ChoiceECNPath, ChoiceEENPath', 'length', 'max'=>255),
			array('DescriptionCN, DescriptionEN, ChoiceACN, ChoiceAEN, ChoiceBCN, ChoiceBEN, ChoiceCCN, ChoiceCEN, ChoiceDCN, ChoiceDEN, ChoiceECN, ChoiceEEN, CreatedTime, EditTime', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('Code, SeqNo, SubjectCode, TeacherCode, mStatus, GroupID, DescriptionCN, DescriptionEN, PicPath, YouTubePath, CorrectAnswer, ChoiceACN, ChoiceACNPath, ChoiceAEN, ChoiceAENPath, ChoiceBCN, ChoiceBCNPath, ChoiceBEN, ChoiceBENPath, ChoiceCCN, ChoiceCCNPath, ChoiceCEN, ChoiceCENPath, ChoiceDCN, ChoiceDCNPath, ChoiceDEN, ChoiceDENPath, ChoiceECN, ChoiceECNPath, ChoiceEEN, ChoiceEENPath, CreatedTime, CreateUser, EditTime, EditUser', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'teacher'=>array(self::BELONGS_TO,'Teachers','TeacherCode')
			// 'teacher'=>array(self::HAS_MANY, 'Teachers', 'id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'Code' => 'Code',
			'SeqNo' => 'Seq No',
			'SubjectCode' => 'Subject Code',
			'TeacherCode' => 'Teacher Code',
			'mStatus' => 'M Status',
			'GroupID' => 'Group',
			'DescriptionCN' => 'Description Cn',
			'DescriptionEN' => 'Description En',
			'PicPath' => 'Pic Path',
			'YouTubePath' => 'You Tube Path',
			'CorrectAnswer' => 'Correct Answer',
			'ChoiceACN' => 'Choice Acn',
			'ChoiceACNPath' => 'Choice Acnpath',
			'ChoiceAEN' => 'Choice Aen',
			'ChoiceAENPath' => 'Choice Aenpath',
			'ChoiceBCN' => 'Choice Bcn',
			'ChoiceBCNPath' => 'Choice Bcnpath',
			'ChoiceBEN' => 'Choice Ben',
			'ChoiceBENPath' => 'Choice Benpath',
			'ChoiceCCN' => 'Choice Ccn',
			'ChoiceCCNPath' => 'Choice Ccnpath',
			'ChoiceCEN' => 'Choice Cen',
			'ChoiceCENPath' => 'Choice Cenpath',
			'ChoiceDCN' => 'Choice Dcn',
			'ChoiceDCNPath' => 'Choice Dcnpath',
			'ChoiceDEN' => 'Choice Den',
			'ChoiceDENPath' => 'Choice Denpath',
			'ChoiceECN' => 'Choice Ecn',
			'ChoiceECNPath' => 'Choice Ecnpath',
			'ChoiceEEN' => 'Choice Een',
			'ChoiceEENPath' => 'Choice Eenpath',
			'CreatedTime' => 'Created Time',
			'CreateUser' => 'Create User',
			'EditTime' => 'Edit Time',
			'EditUser' => 'Edit User',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('Code',$this->Code,true);
		$criteria->compare('SeqNo',$this->SeqNo);
		$criteria->compare('SubjectCode',$this->SubjectCode,true);
		$criteria->compare('TeacherCode',$this->TeacherCode,true);
		$criteria->compare('mStatus',$this->mStatus,true);
		$criteria->compare('GroupID',$this->GroupID,true);
		$criteria->compare('DescriptionCN',$this->DescriptionCN,true);
		$criteria->compare('DescriptionEN',$this->DescriptionEN,true);
		$criteria->compare('PicPath',$this->PicPath,true);
		$criteria->compare('YouTubePath',$this->YouTubePath,true);
		$criteria->compare('CorrectAnswer',$this->CorrectAnswer,true);
		$criteria->compare('ChoiceACN',$this->ChoiceACN,true);
		$criteria->compare('ChoiceACNPath',$this->ChoiceACNPath,true);
		$criteria->compare('ChoiceAEN',$this->ChoiceAEN,true);
		$criteria->compare('ChoiceAENPath',$this->ChoiceAENPath,true);
		$criteria->compare('ChoiceBCN',$this->ChoiceBCN,true);
		$criteria->compare('ChoiceBCNPath',$this->ChoiceBCNPath,true);
		$criteria->compare('ChoiceBEN',$this->ChoiceBEN,true);
		$criteria->compare('ChoiceBENPath',$this->ChoiceBENPath,true);
		$criteria->compare('ChoiceCCN',$this->ChoiceCCN,true);
		$criteria->compare('ChoiceCCNPath',$this->ChoiceCCNPath,true);
		$criteria->compare('ChoiceCEN',$this->ChoiceCEN,true);
		$criteria->compare('ChoiceCENPath',$this->ChoiceCENPath,true);
		$criteria->compare('ChoiceDCN',$this->ChoiceDCN,true);
		$criteria->compare('ChoiceDCNPath',$this->ChoiceDCNPath,true);
		$criteria->compare('ChoiceDEN',$this->ChoiceDEN,true);
		$criteria->compare('ChoiceDENPath',$this->ChoiceDENPath,true);
		$criteria->compare('ChoiceECN',$this->ChoiceECN,true);
		$criteria->compare('ChoiceECNPath',$this->ChoiceECNPath,true);
		$criteria->compare('ChoiceEEN',$this->ChoiceEEN,true);
		$criteria->compare('ChoiceEENPath',$this->ChoiceEENPath,true);
		$criteria->compare('CreatedTime',$this->CreatedTime,true);
		$criteria->compare('CreateUser',$this->CreateUser,true);
		$criteria->compare('EditTime',$this->EditTime,true);
		$criteria->compare('EditUser',$this->EditUser,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Questions the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
