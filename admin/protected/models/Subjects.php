<?php

/**
 * This is the model class for table "mSubjects".
 *
 * The followings are the available columns in table 'mSubjects':
 * @property string $id
 * @property string $NameCN
 * @property string $NameEN
 * @property string $PicPath
 * @property string $Type
 * @property string $webImg
 */
class Subjects extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'mSubjects';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('NameCN', 'length', 'max'=>30),
			array('NameEN', 'length', 'max'=>80),
			array('PicPath, webImg', 'length', 'max'=>255),
			array('Type', 'length', 'max'=>20),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, NameCN, NameEN, PicPath, Type, webImg', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'NameCN' => 'Name Cn',
			'NameEN' => 'Name En',
			'PicPath' => 'Pic Path',
			'Type' => 'Type',
			'webImg' => 'Web Img',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('NameCN',$this->NameCN,true);
		$criteria->compare('NameEN',$this->NameEN,true);
		$criteria->compare('PicPath',$this->PicPath,true);
		$criteria->compare('Type',$this->Type,true);
		$criteria->compare('webImg',$this->webImg,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Subjects the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
