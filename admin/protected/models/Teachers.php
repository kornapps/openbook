<?php

/**
 * This is the model class for table "mTeachers".
 *
 * The followings are the available columns in table 'mTeachers':
 * @property string $id
 * @property string $NameCN
 * @property string $NameEN
 * @property string $PicPath
 * @property string $subject
 * @property string $status
 * @property string $Type
 * @property string $webImg
 * @property integer $sorting
 */
class Teachers extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'mTeachers';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('sorting', 'numerical', 'integerOnly'=>true),
			array('NameCN', 'length', 'max'=>20),
			array('NameEN', 'length', 'max'=>40),
			array('PicPath', 'length', 'max'=>255),
			array('subject', 'length', 'max'=>60),
			array('status', 'length', 'max'=>1),
			array('Type, webImg', 'length', 'max'=>30),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, NameCN, NameEN, PicPath, subject, status, Type, webImg, sorting', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'questions'=>array(self::HAS_MANY, 'Questions', 'TeacherCode'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'NameCN' => 'Name Cn',
			'NameEN' => 'Name En',
			'PicPath' => 'Pic Path',
			'subject' => 'Subject',
			'status' => 'Status',
			'Type' => 'Type',
			'webImg' => 'Web Img',
			'sorting' => 'Sorting',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('NameCN',$this->NameCN,true);
		$criteria->compare('NameEN',$this->NameEN,true);
		$criteria->compare('PicPath',$this->PicPath,true);
		$criteria->compare('subject',$this->subject,true);
		$criteria->compare('status',$this->status,true);
		$criteria->compare('Type',$this->Type,true);
		$criteria->compare('webImg',$this->webImg,true);
		$criteria->compare('sorting',$this->sorting);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Teachers the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
