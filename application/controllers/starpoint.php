<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
session_start(); //we need to call PHP's session object to access it through CI
class Starpoint extends CI_Controller {

 function __construct()
 {

   parent::__construct();
   redirect ('http://openbook.la/index.php/home3/#market.html','refresh');
   $this->load->model('starpoint_model');
 }

 function index()
 {
   // if($this->session->userdata('logged_in'))
   // {
     $session_data = $this->session->userdata('logged_in');

     $data['starpoints'] = $this->starpoint_model->get_starpoints();
     $data['title'] = '';
     $data['phoneNumber'] = $session_data['phoneNumber'];

     // $this->load->view('home_view', $data);
    $this->load->view('templates/header', $data);
    $this->load->view('templates/main-menu', $data);
    $this->load->view('starpoint/index');
    $this->load->view('templates/footer');

   // }
   // else
   // {
   //   //If no session, redirect to login page
   //   redirect('login', 'refresh');
   // }
 }

  function webview()
 {

  header ( 'Location : http://openbook.la/index.php/home2/#market.html');

   // if($this->session->userdata('logged_in'))
   // {
     $session_data = $this->session->userdata('logged_in');

     $data['starpoints'] = $this->starpoint_model->get_starpoints();
     $data['title'] = '';
     $data['phoneNumber'] = $session_data['phoneNumber'];

     // $this->load->view('home_view', $data);
    $this->load->view('templates/header', $data);
    // $this->load->view('templates/main-menu', $data);
    $this->load->view('starpoint/webview');
    $this->load->view('templates/footer');

   // }
   // else
   // {
   //   //If no session, redirect to login page
   //   redirect('login', 'refresh');
   // }
 }

 function logout()
 {
   $this->session->unset_userdata('logged_in');
   session_destroy();
   redirect('home', 'refresh');
 }

}