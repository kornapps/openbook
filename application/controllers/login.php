<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {

 function __construct()
 {
   parent::__construct();
 }

 function index()
 {
 	$data['title'] = 'Openbook 開學啦';
	$this->load->view('templates/header-w', $data);
	// $this->load->view('templates/main-menu', $data);
    $this->load->helper(array('form'));
    $this->load->view('login/login-master');
	$this->load->view('templates/footer');
 }

}
