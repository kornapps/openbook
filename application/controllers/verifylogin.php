<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class VerifyLogin extends CI_Controller {

 function __construct()
 {
   parent::__construct();
   $this->load->model('user_model','',TRUE);
 }

 function index()
 {
   //This method will have the credentials validation
   $this->load->library('form_validation');

   $this->form_validation->set_rules('inputPhoneNumber', 'PhoneNumber', 'trim|required|xss_clean');
   $this->form_validation->set_rules('inputPassword', 'Password', 'trim|required|xss_clean|callback_check_database');

   if($this->form_validation->run() == FALSE)
   {
     //Field validation failed.  User redirected to login page
    $data['title'] = 'Openbook 開學啦';
    $this->load->view('templates/header-w', $data);
    // $this->load->view('templates/main-menu', $data);
    // $this->load->helper(array('form'));
    $this->load->view('login/login-master');
    $this->load->view('templates/footer');
   }
   else
   {
     //Go to private area
     redirect('games', 'refresh');
   }

 }

 function check_database($password)
 {
   //Field validation succeeded.  Validate against database
   $phoneNumber = $this->input->post('inputPhoneNumber');

   //query the database
   $result = $this->user_model->login($phoneNumber, $password);

   if($result)
   {
     $sess_array = array();
     foreach($result as $row)
     {
       $sess_array = array(
         'id' => $row->id,
         'phoneNumber' => $row->phoneNumber
       );
       $this->session->set_userdata('logged_in', $sess_array);
     }
     return TRUE;
   }
   else
   {
     $this->form_validation->set_message('check_database', 'Invalid phoneNumber or password');
     return false;
   }
 }
}
