<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
session_start(); //we need to call PHP's session object to access it through CI
class Market extends CI_Controller {

 function __construct()
 {
   parent::__construct();
   $this->load->model('market_model');
 }

 function index()
 {
   // if($this->session->userdata('logged_in'))
   // {
     $session_data = $this->session->userdata('logged_in');


     $data['markets'] = $this->market_model->get_markets();
     $data['title'] = '';
     $data['phoneNumber'] = $session_data['phoneNumber'];

     // $this->load->view('home_view', $data);
    $this->load->view('templates/header', $data);
    $this->load->view('templates/main-menu', $data);
    $this->load->view('market/index');
    $this->load->view('templates/footer');

   // }
   // else
   // {
     //If no session, redirect to login page
     // redirect('login', 'refresh');
   // }
 }


  function webview()
 {
   // if($this->session->userdata('logged_in'))
   // {
     $session_data = $this->session->userdata('logged_in');


    //  $data['markets'] = $this->market_model->get_markets();
    //  $data['title'] = '';
    //  $data['phoneNumber'] = $session_data['phoneNumber'];

    //  // $this->load->view('home_view', $data);
    // $this->load->view('templates/header', $data);
    // // $this->load->view('templates/main-menu', $data);
    // $this->load->view('market/webview');
    // $this->load->view('templates/footer');

      // $data['markets'] = $this->market_model->get_markets();
      // $data['title'] = 'Openbook 開學啦';
      // $this->load->view('templates/market',$data);

     redirect('home3/#starpoint.html', 'refresh');

   // }
   // else
   // {
     //If no session, redirect to login page
     // redirect('login', 'refresh');
   // }
 }


 function logout()
 {
   $this->session->unset_userdata('logged_in');
   session_destroy();
   redirect('home', 'refresh');
 }

}