<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class GuestLogin extends CI_Controller {

 function __construct()
 {
   parent::__construct();
   $this->load->model('user_model','',TRUE);
 }

 function index()
 {
   //This method will have the credentials validation
   $this->load->library('form_validation');

   // $this->form_validation->set_rules('inputPhoneNumber', 'PhoneNumber', 'trim|required|xss_clean');
   // $this->form_validation->set_rules('inputPassword', 'Password', 'callback_check_database');

   // print_r($this->check_database('78777788'));

   if($this->check_database('78777788') == FALSE)
   {
     //Field validation failed.  User redirected to login page
    $data['title'] = 'Login';
    $this->load->view('templates/header-w', $data);
    // $this->load->view('templates/main-menu', $data);
    // $this->load->helper(array('form'));
    $this->load->view('login/login-master');
    $this->load->view('templates/footer');
   }
   else
   {
     //Go to private area
     redirect('games', 'refresh');
   }

 }

 function check_database($password)
 {
   //Field validation succeeded.  Validate against database
   $phoneNumber = 'guest';

   //query the database
   $result = $this->user_model->login($phoneNumber, $password);

   // var_dump($result);

   if($result)
   {
     $sess_array = array();
     foreach($result as $row)
     {
       $sess_array = array(
         'id' => $row->id,
         'phoneNumber' => $row->phoneNumber
       );
       $this->session->set_userdata('logged_in', $sess_array);
     }
     return TRUE;
   }
   else
   {
     $this->form_validation->set_message('check_database', 'Invalid phoneNumber or password');
     return false;
   }
 }
}
