<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
session_start(); //we need to call PHP's session object to access it through CI
class Quiz extends CI_Controller {

	 function __construct()
	 {
	   parent::__construct();
	   $this->load->model('questions_model');
	   $this->load->model('exams_model');
	 }

	public function index()
	{
		// print_r($this->session->userdata('logged_in'));
		$setting = $this->input->get();
		$session_data = $this->session->userdata('logged_in');

		$data['userid'] = $session_data['id'];

		if(!isset($setting['countDown'])){
			$setting['countDown'] = 0;
		}

		if(!isset($setting['p'])){
			$setting['qFrom'] = 1;
			$setting['qTo'] = 10;
			$qFrom = 1;
			$qTo = 100;
		}else{

			$qTo = 10;
			$qFrom = $setting['p'] * 10 - 10;

			// if($qFrom > 99){
				// $qTo = 99;
			// }
			echo $qTo.':'.$qFrom;

		}

		$data['examSetting'] = array('SubjectCode'=>$setting['s'],'TeacherCode'=>$setting['t'],'CountDown'=>$setting['countDown'],'qFrom'=>$qFrom,'qTo'=>$qTo);


		if($setting['type'] == 'S'){
			//普通模式
			$this->load->view('exam/quiz_type_n',$data);
		
		}elseif($setting['type'] == 'T'){
			// 計時模式
			$this->load->view('exam/quiz_type_t',$data);	
		}elseif($setting['type'] == 'L'){
			// 教學模式

			$data['qstatus'] = $this->quizStatus($setting['s'],$setting['t'],15);

			// print_r($status);

			// print_r($setting);
			$level = array(
				'level1' => '<img src="/assets/img/level/n-lv5.png" class="levelimg"> 你有實力挑機了！挑戰Time Attack Mode，成為5*達人吧！未達5**，也不要低估DSE呀！',
				'level2' => '<img src="/assets/img/level/n-lv4.png" class="levelimg"> 你的實力尚可！但伏也是會中的，距離目標還很遠呀！到｢教室模式(Tutorial Mode) ｣再修練吧！',
				'level3' => '<img src="/assets/img/level/n-lv3.png" class="levelimg"> 你是常人，而常人是沒有好的大學讀的。對不起，我是電腦程式，不懂說謊！但我們有一個｢教室模式(Tutorial Mode) ｣，可能幫到你！',
				'level4' => '<img src="/assets/img/level/n-lv2.png" class="levelimg"> 我想你未測試之前也應該知道自己廢吧！現在進一步證實了，心情如何？是否有如釋重負的感覺？不要扔電話或電腦發洩，雖然我們不介意…… 到｢教室模式(Tutorial Mode) ｣跟強者們一起修練吧！',
				'level5' => '<img src="/assets/img/level/n-lv1.png" class="levelimg"> 我想你未測試之前也應該知道自己廢吧！現在進一步證實了，心情如何？是否有如釋重負的感覺？不要扔電話或電腦發洩，雖然我們不介意…… 到｢教室模式(Tutorial Mode) ｣跟強者們一起修練吧！',
				'level6' => '<img src="/assets/img/level/n-lv0.png" class="levelimg"> 如果你是路人便算了！過主啦！讀書不適合你......趁年青，學一技之長還不至於成為社會包袱。不要到｢教室模式(Tutorial Mode) ｣你不行的！)',
			);

			$data['level'] = $level;

			$data['questions'] = $this->questions_model->get_questionsFroExam('CN',100,$setting['s'],$setting['t']);

		

			// print_r($data['questions']);

		    $data['title'] = '';
		    $this->load->view('templates/header', $data);
		    // $this->load->view('templates/main-menu', $data);
			$this->load->view('exam/quiz_type_l',$data);
			$this->load->view('templates/footer',$data);
		}

	}

	public function dotest(){

		//d
		return false;
	}

	public function countPoint(){

		// print_r($this->input->post());

		// $user = $this->session->userdata('logged_in');
		// print_r($user);

		if($this->session->userdata('logged_in')){
			//login
			$user = $this->session->userdata('logged_in');
			$memberID = $user['id'];

		}else{
			//no login , set to guest account
			$memberID = 22;			

		}

		$countPoint = $this->questions_model->countThePoint($memberID,$this->input->post('questions'));		

		
	}

	public function quizStatus($SubjectCode = null,$TeacherCode=null,$MemberCode = null){

		if($SubjectCode || $MemberCode || $TeacherCode){

			$qstatus = $this->exams_model->getMyQuizStatus($SubjectCode,$TeacherCode,$MemberCode);

			$qarray = array();

			foreach ($qstatus as $key => $value) {
				$qarray[$value['QuestionNo']] = $value['Ans'];
			}
			return $qarray;

		}else{

			return false;
		}


	}

	function endswith($string, $test) {
	    $strlen = strlen($string);
	    $testlen = strlen($test);
	    if ($testlen > $strlen) return false;
	    return substr_compare($string, $test, -$testlen) === 0;
	}

	public function getJson($lang = null,$numberOfQuestions = 10,$subject =null,$teacher=null,$qmode = null,$qFrom = 1,$qTo=10){

		$lang = $this->input->get('lang');
		$numberOfQuestions = $this->input->get('numberOfQuestions');
		$qFrom = $this->input->get('qfrom');
		$qTo = $this->input->get('qto');
		$subject = $this->input->get('subject');
		$teacher = $this->input->get('teacher');

		// echo $qFrom;


		$data['questions'] = $this->questions_model->get_questionsFroExam($lang,$numberOfQuestions,$subject,$teacher,$groupid = 1,$qFrom,$qTo);
		// print_r($data['questions']);

		$title = $this->questions_model->getTitle($subject,$teacher);

		// print($title);

		$quizJson =array();

		if($qmode <> 't'){
		//
		$quizJson['info'] = array(
				'name'=> $title['sortTitle'],
				'main'=>'<h3>'.$title['fullTitle'].'</h3>',
				'results'=>'<h4>完成後可得知結果及評分</h4>',
				'level1' => '<img src="/assets/img/level/n-lv5.png" class="level"> 你有實力挑機了！挑戰Time Attack Mode，成為5*達人吧！未達5**，也不要低估DSE呀！',
				'level2' => '<img src="/assets/img/level/n-lv4.png" class="level"> 你的實力尚可！但伏也是會中的，距離目標還很遠呀！到｢教室模式(Tutorial Mode) ｣再修練吧！',
				'level3' => '<img src="/assets/img/level/n-lv3.png" class="level"> 你是常人，而常人是沒有好的大學讀的。對不起，我是電腦程式，不懂說謊！但我們有一個｢教室模式(Tutorial Mode) ｣，可能幫到你！',
				'level4' => '<img src="/assets/img/level/n-lv2.png" class="level"> 我想你未測試之前也應該知道自己廢吧！現在進一步證實了，心情如何？是否有如釋重負的感覺？不要扔電話或電腦發洩，雖然我們不介意…… 到｢教室模式(Tutorial Mode) ｣跟強者們一起修練吧！',
				'level5' => '<img src="/assets/img/level/n-lv1.png" class="level"> 我想你未測試之前也應該知道自己廢吧！現在進一步證實了，心情如何？是否有如釋重負的感覺？不要扔電話或電腦發洩，雖然我們不介意…… 到｢教室模式(Tutorial Mode) ｣跟強者們一起修練吧！',
				'level6' => '<img src="/assets/img/level/n-lv0.png" class="level"> 如果你是路人便算了！過主啦！讀書不適合你......趁年青，學一技之長還不至於成為社會包袱。不要到｢教室模式(Tutorial Mode) ｣你不行的！)',
			);
		}else{
		// Time Attack mode
		$quizJson['info'] = array(
				'name'=>$title['sortTitle'],
				'main'=>'<h3>'.$title['fullTitle'].'</h3>',
				'results'=>'<h4>完成後可得知結果及評分</h4>',
				'level1' => '',
				'level2' => '',
				'level3' => '',
				'level4' => '',
				'level5' => '對不起，你還未有挑戰Time Attack Mode 的能力！回到 Standard Mode (標準模式)繼續修煉吧',

			);

		}

		 foreach($data['questions'] as $key => $value){

		 	$tmpCurrect = 'Choice'.$value['CorrectAnswer'];

		 	 // echo $tmpCurrect;

		 	$tmpList = array_keys($value);
		 	// print_r($tmpList);

		 	// echo substr($value['ChoiceA'],0,5);
		 	// if(sub_str($value['ChoiceA'],0,5) == 'asset') echo '111';

		 	 if(substr($value['ChoiceA'],0,6) === '/asset') $value['ChoiceA'] = '<img width="90%" style="vertical-align: middle;" src="'.$value['ChoiceA'].'">';
		 	 if(substr($value['ChoiceB'],0,6) === '/asset') $value['ChoiceB'] = '<img width="90%" style="vertical-align: middle;" src="'.$value['ChoiceB'].'">';
		 	 if(substr($value['ChoiceC'],0,6) === '/asset') $value['ChoiceC'] = '<img width="90%" style="vertical-align: middle;" src="'.$value['ChoiceC'].'">';
		 	 if(substr($value['ChoiceD'],0,6) === '/asset') $value['ChoiceD'] = '<img width="90%" style="vertical-align: middle;" src="'.$value['ChoiceD'].'">';
		 	 if(substr($value['ChoiceE'],0,6) === '/asset') $value['ChoiceE'] = '<img width="90%" style="vertical-align: middle;" src="'.$value['ChoiceE'].'">';

		 	// if($tmpList[5] == $tmpCurrect){	$tmpChoiceA = true;else{$tmpChoiceA = 'false';} 

		 	$tmpChoiceA = ($tmpList[6] == $tmpCurrect) ?true:false ;
		 	$tmpChoiceB = ($tmpList[7] == $tmpCurrect) ?true:false ;
		 	$tmpChoiceC = ($tmpList[8] == $tmpCurrect) ?true:false ;
		 	$tmpChoiceD = ($tmpList[9] == $tmpCurrect) ?true:false ;
		 	$tmpChoiceE = ($tmpList[10] == $tmpCurrect) ?true:false ;

		 	// echo $tmpChoiceA;

		 	 $tmpAns = array();
		 	 $tmpAns[] = array("option"=>$value['ChoiceA'],'correct'=>$tmpChoiceA);
		 	 $tmpAns[] = array("option"=>$value['ChoiceB'],'correct'=>$tmpChoiceB);
		 	 $tmpAns[] = array("option"=>$value['ChoiceC'],'correct'=>$tmpChoiceC);
		 	 $tmpAns[] = array("option"=>$value['ChoiceD'],'correct'=>$tmpChoiceD);
		 	 if($value['ChoiceE'] <> ''){
		 	 $tmpAns[] = array("option"=>$value['ChoiceE'],'correct'=>$tmpChoiceE);
		 	 }

		 	 if($value['PicPath'] == '' || $value['PicPath'] == null){

		 	 	$newDsic = '<pre>'.$value['Description'].'</pre>';

		 	 }else{

		 	 	$newDsic = '<pre>'.$value['Description'].'</pre>'.'<img width="90%" src="'.$value['PicPath'].'">';
		 	 }
		 
			$quizJson['questions'][] = array(
					'id'=>$value['Code'],
					// 'q'=>'<pre>'.$value['Description'].'</pre>',
					'q'=>$newDsic,
					'correct'=>'',
					'incorrect'=>'',
					"select_any"=>false,
					'a'=>$tmpAns,
					'ans' =>$value['CorrectAnswer']
				);
		}

         // print_r($data['questions']);
		$this->output->set_header("Access-Control-Allow-Origin: *");
		$this->output->set_header("Access-Control-Expose-Headers: Access-Control-Allow-Origin");
		$this->output->set_status_header(200);
		$this->output->set_content_type('application/json')->set_output(json_encode($quizJson));

	}
}