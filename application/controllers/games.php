<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Games extends CI_Controller {

	function index2()
	{
		if($this->session->userdata('logged_in'))
		{
			$session_data = $this->session->userdata('logged_in');

			$userid = $session_data['id'];
			$data['title'] = 'Openbook 開學啦';
			// $data['phoneNumber'] = $session_data['phoneNumber'];
			// $data['memberContent'] =  $this->user_model->getContent($userid);

     // print_r($data['memberContent']);

     // $this->load->view('home_view', $data);
			$this->load->view('templates/header-g', $data);
			// $this->load->view('templates/main-menu', $data);
			$this->load->view('home/openbook_game_select');
			$this->load->view('templates/footer'	);

		}
		else
		{
     //If no session, redirect to login page
			redirect('login', 'refresh');
		}
	}


	function index()
	{
		if($this->session->userdata('logged_in'))
		{
			$session_data = $this->session->userdata('logged_in');

			$userid = $session_data['id'];
			$data['title'] = 'Openbook 開學啦';
			// $data['phoneNumber'] = $session_data['phoneNumber'];
			// $data['memberContent'] =  $this->user_model->getContent($userid);

     // print_r($data['memberContent']);

     // $this->load->view('home_view', $data);
			$this->load->view('templates/header-g2', $data);
			// $this->load->view('templates/main-menu', $data);
			$this->load->view('home/openbook_game_select2');
			$this->load->view('templates/footer'	);

		}
		else
		{
     //If no session, redirect to login page
			redirect('login', 'refresh');
		}
	}

}