<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Comment extends CI_Controller {
	// function __construct() {
	// 	parent::Controller();
	// }

	public function __construct()
	{
		parent::__construct();
		$this->load->model('comments_model');
		$this->load->model('questions_model');
	}

	public function index()
	{
			$data['comments'] = $this->comments_model->get_comments();
			$data['title'] = 'comments archive';

			$this->load->view('templates/header', $data);
			$this->load->view('comments/index', $data);
			$this->load->view('templates/footer');
	}

	public function view($qid)
	{
		$this->load->helper('form');
		$session_data = $this->session->userdata('logged_in');

		$data['comments'] = $this->comments_model->get_comments($qid);
		$data['title'] = 'List by Quesation';
		$data['qid'] = $qid;
		$data['mid'] = $session_data['id'];

		$data['question'] = $this->questions_model->get_questions($qid);

			// if (empty($data['comments']))
			// {
			// 	show_404();
			// }

			$this->load->view('templates/header', $data);
			$this->load->view('comments/view', $data);
			$this->load->view('templates/footer');
	}

	public function webview($qid)
	{

		$this->load->helper('form');
		// $session_data = $this->session->userdata('logged_in');
		if ($this->input->post('memberID')){
			$memberID = $this->input->post('memberID');
		}else{
			$memberID = 2;
		}

		$data['comments'] = $this->comments_model->get_comments($qid);
		$data['title'] = 'List by Quesation';
		$data['qid'] = $qid;
		$data['mid'] = $memberID;

		$data['question'] = $this->questions_model->get_questions($qid);

			// if (empty($data['comments']))
			// {
			// 	show_404();
			// }

			$this->load->view('templates/header', $data);
			$this->load->view('comments/webview', $data);
			$this->load->view('templates/footer');
	}

	public function iframeview($qid)
	{
		$this->load->helper('form');
		$session_data = $this->session->userdata('logged_in');

		$data['comments'] = $this->comments_model->get_comments($qid);
		$data['title'] = 'List by Quesation';
		$data['qid'] = $qid;
		$data['mid'] = $session_data['id'];

		$data['question'] = $this->questions_model->get_questions($qid);

			// if (empty($data['comments']))
			// {
			// 	show_404();
			// }

			$this->load->view('templates/header', $data);
			$this->load->view('comments/iframeview', $data);
			$this->load->view('templates/footer');
	}


	public function create()
	{
	$this->load->helper('form');
	$this->load->library('form_validation');
	
	$data['title'] = 'Create a comment';
	$data['qid'] = $this->input->post('qid');
	$data['comment'] = $this->input->post('comment');
	
	$this->form_validation->set_rules('comment', '內文', 'required');
	$this->form_validation->set_rules('qid', 'Question No', 'required');
	$this->form_validation->set_rules('mid', 'Question No', 'required');
	
	if ($this->form_validation->run() === FALSE)
	{
		redirect('comment/view/'.$data['qid'], 'refresh');
		// $this->load->view('templates/header', $data);	
		// $this->load->view('comments/view');
		// $this->load->view('templates/footer');
		
	}
	else
	{
		$this->comments_model->set_comments();
		redirect('comment/view/'.$data['qid'], 'refresh');
		$this->news_model->set_news();
		// $this->load->view('news/success');
		// redirect('comment/'., 'refresh');
	}
	}	

	public function appcreate()
	{
	$this->load->helper('form');
	$this->load->library('form_validation');
	
	$data['title'] = 'Create a comment';
	$data['qid'] = $this->input->post('qid');
	$data['comment'] = $this->input->post('comment');
	
	$this->form_validation->set_rules('comment', '內文', 'required');
	$this->form_validation->set_rules('qid', 'Question No', 'required');
	$this->form_validation->set_rules('mid', 'Question No', 'required');
	
	if ($this->form_validation->run() === FALSE)
	{
		redirect('comment/webview/'.$data['qid'], 'refresh');
		
	}
	else
	{
		$this->comments_model->set_comments();
		redirect('comment/webview/'.$data['qid'], 'refresh');
		$this->news_model->set_news();
	}
	}	

}