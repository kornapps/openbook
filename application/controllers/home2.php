<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
session_start(); //we need to call PHP's session object to access it through CI
class Home2 extends CI_Controller {

var $session_data;

 function __construct()
 {
   parent::__construct();
   $this->load->model('questions_model');
   $this->load->model('user_model');
   $this->load->model('subjects_model');
   $this->load->model('teachers_model');
   $this->load->model('market_model');
   $this->load->model('starpoint_model');
   $this->session_data = $this->session->userdata('logged_in');
   // if(!$this->session->userdata('logged_in')) redirect('login', 'refresh');

 }

 function index()
 {
   if($this->session->userdata('logged_in'))
   {
     $data['phoneNumber'] = $this->session_data['phoneNumber'];
     $userid = $this->session_data['id'];
     $data['title'] = 'Openbook 開學啦';
     $data['memberContent'] =  $this->user_model->getContent($userid);

     // print_r($data['memberContent']);

     // $this->load->view('home_view', $data);
    // $this->load->view('templates/header', $data);
    // $this->load->view('templates/main-menu', $data);
    $this->load->view('templates/mainV2',$data);
    // $this->load->view('templates/footer');

   }
   else
   {
     //If no session, redirect to login page
     redirect('login', 'refresh');
   }
 }

 public function portfolio(){

      // $this->load->library('ciqrcode');
      // // echo FCPATH;
      // $params['data'] = 'http://openbook.la/';
      // $params['level'] = 'H';
      // $params['size'] = 10;
      // $params['savename'] = FCPATH.'assets/img/openbook_qr.png';
      // $this->ciqrcode->generate($params);

      $data['phoneNumber'] = $this->session_data['phoneNumber'];
      $userid = $this->session_data['id'];
      $data['memberContent'] =  $this->user_model->getContent($userid);
      $data['title'] = 'Openbook 開學啦';
      // print_r($data['phoneNumber']);
      $this->load->view('templates/userData',$data);
 }

 public function subjects(){

      $data['title'] = 'Openbook 開學啦';
      $data['toExam'] = false;
      $data['subjects'] = $this->subjects_model->get_subjects();

      $info = $this->subjects_model->get_subjectsWithTeacherinfo();
      // print_r($data['subjects']);

      $subjectsInfo = array();

      foreach ($data['subjects'] as $key) {
        
          // print_r($key);
          $subjectsInfo[$key['id']] = $key['id'];
          // $subjectsInfo[$key['id']] = array('PicPath'=>$key['PicPath']);
          $subjectsInfo[$key['id']] = array('PicPath'=>$key['webImg'],'info'=>array());
           foreach ($info as $ikey ) {
             if($key['id'] == $ikey['SubjectCode'] and  $ikey['SubjectCode'] <> ''){
               $subjectsInfo[$key['id']]['info'][] = $ikey;
                // $subjectsInfo[$key['id']][] = $ikey; 
             // $subjectsInfo[$key['id']]['teacherPicPath'] = $ikey['PicPath'];
             // $subjectsInfo[$key['id']]['teacherName'] = $ikey['PicPath'];
             // $subjectsInfo[$key['id']]['QuestionCode'] = $ikey['code'];
             }
           }

      }
      $data['subjectsInfo'] = $subjectsInfo;
      // print_r($subjectsInfo);


      // $this->load->view('home/list-subject',$data);

      $this->load->view('templates/subjects',$data);
 }

 public function teachers(){

      $data['title'] = 'Openbook 開學啦';
      $data['teachers'] = $this->teachers_model->get_teachers();
      // print_r($data['teachers']);
      $info = $this->teachers_model->get_teachersWithSubjectinfo();
      // print_r($info);

      foreach ($data['teachers'] as $key) {
        // print_r($key);
        $teachersInfo[$key['id']] = $key['id'];
        // $subjectsInfo[$key['id']] = array('PicPath'=>$key['PicPath']);
        $teachersInfo[$key['id']] = array('PicPath'=>$key['webImg'],'info'=>array());
         foreach ($info as $ikey ) {
           if($key['id'] == $ikey['TeacherCode'] and  $ikey['TeacherCode'] <> ''){
             $teachersInfo[$key['id']]['info'][] = $ikey;
           }
         }
      }
      // print_r($teachersInfo);
      $data['teachersInfo'] = $teachersInfo;

      $this->load->view('templates/teachers',$data);
 }

  public function rank($type = null){

      $data['title'] = 'Openbook 開學啦';
      $data['phoneNumber'] = $this->session_data['phoneNumber'];

      $this->load->model('exams_model');

      if ($type){
          switch ($type) {
            case 'hk':
            $data['Ranks'] = $this->exams_model->get_TopRank(0);

              # code...
              break;
            
            case 'friend':

              if($data['phoneNumber'] = 'guest'){
                $data['Ranks'] = $this->exams_model->get_TopRank(0);
              }else{
                $data['Ranks'] = $this->exams_model->get_FriendsRank(0,$userid = $this->session_data['id']);
              }
              break;

            default:
              # code...
              break;
          }
      }
      // print_r($data['Ranks']);
      $this->load->view('templates/rank',$data);
 }

  public function market(){

      $data['title'] = 'Openbook 開學啦';
      $data['starpoints'] = $this->starpoint_model->get_starpoints();  
      // print_r($data['starpoints']);    
      // $this->load->view('templates/starpoint',$data);
      $this->load->view('templates/comming',$data);

 }

  public function starpoint(){
      $data['markets'] = $this->market_model->get_markets();
      $data['title'] = 'Openbook 開學啦';
      // $this->load->view('templates/market2',$data);
      $this->load->view('templates/comming',$data);

 }

 public function share(){

     $data['title'] = 'Openbook 開學啦';
      $this->load->view('templates/share',$data);  
 }

  public function about(){

     $data['title'] = 'Openbook 開學啦';
      $this->load->view('templates/about',$data);  
 }

 public function showStaturChart(){

    $data['title'] = '能量BAR';

    $this->load->view('templates/header', $data);
    // $this->load->view('templates/main-menu', $data);
    $this->load->view('home/chart');
    $this->load->view('templates/footer');
 }


 function logout()
 {
   $this->session->unset_userdata('logged_in');
   session_destroy();
   redirect('home', 'refresh');
 }

}