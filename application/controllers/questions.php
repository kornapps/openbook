<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
session_start(); //we need to call PHP's session object to access it through CI
class Questions extends CI_Controller {

 function __construct()
 {
   parent::__construct();
   $this->load->model('market_model');
   $this->load->model('questions_model');
    
 }

 function index()
 {
   // if($this->session->userdata('logged_in'))
   // {
     $session_data = $this->session->userdata('logged_in');

     $data['markets'] = $this->market_model->get_markets();
     $data['phoneNumber'] = $session_data['phoneNumber'];

     // $this->load->view('home_view', $data);
    $this->load->view('templates/header', $data);
    $this->load->view('templates/main-menu', $data);
    $this->load->view('market/index');
    $this->load->view('templates/footer');

   // }
   // else
   // {
     //If no session, redirect to login page
     // redirect('login', 'refresh');
   // }
 }

  function getQuestionsForExam($no = 10){

  }

  function view($qid = null){
    $this->load->helper('form');

    if($this->session->userdata('logged_in'))
    {

     $session_data = $this->session->userdata('logged_in');

     $data['question'] = $this->questions_model->get_questions($qid);

    if (empty($data['question']))
    {
      show_404();
    }

     $data['session'] = $session_data;
     $data['title'] = '解題';
     // $data['phoneNumber'] = $session_data['phoneNumber'];

     // $this->load->view('home_view', $data);
    // $this->load->view('templates/header', $data);
    // $this->load->view('templates/main-menu', $data);
    $this->load->view('questions/view',$data);
    // $this->load->view('templates/footer');

    }
    else
    {
     //If no session, redirect to login page
     redirect('login', 'refresh');
    }


  }
}