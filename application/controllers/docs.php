<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Docs extends CI_Controller {

	 function __construct()
	 {
	   parent::__construct();
	   $this->load->model('docs_model');
	 }

	function index()
	{
		// if($this->session->userdata('logged_in'))
		// {
			// $session_data = $this->session->userdata('logged_in');

			// $userid = $session_data['id'];
			// $data['title'] = 'Openbook 開學啦';

			// $this->load->view('templates/header-g2', $data);
			// // $this->load->view('templates/main-menu', $data);
			// $this->load->view('home/openbook_game_select2');
			// $this->load->view('templates/footer'	);

		// }
		// else
		// {
  //    //If no session, redirect to login page
		// 	redirect('login', 'refresh');
		// }
	}

	function view($teacher=null,$paper=1){

			$data['title'] = 'Openbook 開學啦';
			$data['docs'] =  $this->docs_model->get_doc($teacher,$paper);

			if($data['docs'])
			{
				$this->load->view('templates/header-g2', $data);
				// echo $teacher;
				// echo $page;
				$this->load->view('templates/doc', $data);
				// $this->load->view('home/openbook_game_select2');
				$this->load->view('templates/footer-basic'	);				
			}else{
				show_404();
			}



	}

}