<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
session_start(); //we need to call PHP's session object to access it through CI
class Home3 extends CI_Controller {

var $session_data;

 function __construct()
 {
   parent::__construct();
   $this->load->model('questions_model');
   $this->load->model('user_model');
   $this->load->model('subjects_model');
   $this->load->model('teachers_model');
   $this->load->model('market_model');
   $this->load->model('starpoint_model');
   $this->session_data = $this->session->userdata('logged_in');
   // if(!$this->session->userdata('logged_in')) redirect('login', 'refresh');

 }

 function index()
 {
   // if($this->session->userdata('logged_in'))
   // {
     $data['phoneNumber'] = $this->session_data['phoneNumber'];
     $userid = $this->session_data['id'];
     $data['title'] = 'Openbook 開學啦';
     $data['memberContent'] =  $this->user_model->getContent($userid);

     // print_r($data['memberContent']);

     // $this->load->view('home_view', $data);
    // $this->load->view('templates/header', $data);
    // $this->load->view('templates/main-menu', $data);
    $this->load->view('templates/mainV3',$data);
    // $this->load->view('templates/footer');

   // }
   // else
   // {
   //   //If no session, redirect to login page
   //   redirect('login', 'refresh');
   // }
 }

 public function portfolio(){

      // $this->load->library('ciqrcode');
      // // echo FCPATH;
      // $params['data'] = 'http://openbook.la/';
      // $params['level'] = 'H';
      // $params['size'] = 10;
      // $params['savename'] = FCPATH.'assets/img/openbook_qr.png';
      // $this->ciqrcode->generate($params);

      $data['phoneNumber'] = $this->session_data['phoneNumber'];
      $userid = $this->session_data['id'];
      $data['memberContent'] =  $this->user_model->getContent($userid);
      $data['title'] = 'Openbook 開學啦';
      // print_r($data['phoneNumber']);
      $this->load->view('templates/userData',$data);
 }

 public function subjects(){

      $data['title'] = 'Openbook 開學啦';
      $data['toExam'] = false;
      $data['subjects'] = $this->subjects_model->get_subjects();

      $info = $this->subjects_model->get_subjectsWithTeacherinfo();
      // print_r($data['subjects']);

      $subjectsInfo = array();

      foreach ($data['subjects'] as $key) {
        
          // print_r($key);
          $subjectsInfo[$key['id']] = $key['id'];
          // $subjectsInfo[$key['id']] = array('PicPath'=>$key['PicPath']);
          $subjectsInfo[$key['id']] = array('PicPath'=>$key['webImg'],'info'=>array());
           foreach ($info as $ikey ) {
             if($key['id'] == $ikey['SubjectCode'] and  $ikey['SubjectCode'] <> ''){
               $subjectsInfo[$key['id']]['info'][] = $ikey;
                // $subjectsInfo[$key['id']][] = $ikey; 
             // $subjectsInfo[$key['id']]['teacherPicPath'] = $ikey['PicPath'];
             // $subjectsInfo[$key['id']]['teacherName'] = $ikey['PicPath'];
             // $subjectsInfo[$key['id']]['QuestionCode'] = $ikey['code'];
             }
           }

      }
      $data['subjectsInfo'] = $subjectsInfo;
      // print_r($subjectsInfo);


      // $this->load->view('home/list-subject',$data);

      $this->load->view('templates/subjects',$data);
 }

 public function teachers(){

      $data['title'] = 'Openbook 開學啦';
      $data['teachers'] = $this->teachers_model->get_teachers();
      print_r($data['teachers']);
      $info = $this->teachers_model->get_teachersWithSubjectinfo();
      print_r($info);

      foreach ($data['teachers'] as $key) {
        // print_r($key);
        $teachersInfo[$key['id']] = $key['id'];
        // $subjectsInfo[$key['id']] = array('PicPath'=>$key['PicPath']);
        $teachersInfo[$key['id']] = array('PicPath'=>$key['webImg'],'info'=>array());
         foreach ($info as $ikey ) {
           if($key['id'] == $ikey['TeacherCode'] and  $ikey['TeacherCode'] <> ''){
             $teachersInfo[$key['id']]['info'][] = $ikey;
           }
         }
      }
      // print_r($teachersInfo);
      $data['teachersInfo'] = $teachersInfo;

      $this->load->view('templates/teachers',$data);
 }

  public function rank($type = null){

      $data['title'] = 'Openbook 開學啦';

      if ($type){
          switch ($type) {
            case 'hk':
              # code...
              break;
            
            case 'friend':
              # code...
              break;

            default:
              # code...
              break;
          }
      }
      $this->load->view('templates/rank',$data);
 }

  public function market(){

      $data['title'] = 'Openbook 開學啦';
      $data['starpoints'] = $this->starpoint_model->get_starpoints();  
      print_r($data['starpoints']);    
      // $this->load->view('templates/starpoint',$data);
      $this->load->view('templates/comming',$data);

 }

  public function starpoint(){
      // $data['markets'] = $this->market_model->get_markets();
      $data['markets'] = $this->market_model->get_markets();
      $data['title'] = 'Openbook 開學啦';
      // $this->load->view('templates/market2',$data);
      $this->load->view('templates/comming',$data);

 }

 public function showStaturChart(){

    $data['title'] = '能量BAR';

    $this->load->view('templates/header', $data);
    // $this->load->view('templates/main-menu', $data);
    $this->load->view('home/chart');
    $this->load->view('templates/footer');
 }

//  public function subjects($subjectCode=null,$teacherCode = null){

//   // $this->load->model('subjects_model');
//   $data['phoneNumber'] = $this->session_data['phoneNumber'];


//   $data['title'] = '';

//     $this->load->view('templates/header', $data);
//     $this->load->view('templates/main-menu', $data);
//     if($subjectCode){
//       //有選科目
//       $data['teachers'] = $this->questions_model->getSubjectandTeacherList('',$subjectCode,$teacherCode);
//       $data['toExam'] = true;
//       $this->load->view('home/list-teacher',$data);
//     }else{
//       $data['toExam'] = false;
//       $data['subjects'] = $this->subjects_model->get_subjects();
//       // $data['subjects'] = $this->questions_model->getSubjectWithExamList();
//       $this->load->view('home/list-subject',$data);
//     }
    
//     $this->load->view('templates/footer');
//  }

//  public function teachers($teacherCode=null,$subjectCode=null){

//     // $this->load->model('teachers_model');
//     $data['phoneNumber'] = $this->session_data['phoneNumber'];

//     $data['title'] = '';
//     $this->load->view('templates/header', $data);
//     $this->load->view('templates/main-menu', $data);

//     if($teacherCode){
//       //有選科目
//       $data['subjects'] = $this->questions_model->getSubjectandTeacherList('',$subjectCode,$teacherCode);
//       $data['toExam'] = true;
      
// //      print_r($data['subjects']);
//       $this->load->view('home/list-subject',$data);
//     }else{
//       $data['toExam'] = false;
//       $data['teachers'] = $this->teachers_model->get_teachers();
//        $this->load->view('home/list-teacher',$data);
//     }

//     $this->load->view('templates/footer');
//  }

//  public function info(){

//    if($this->session->userdata('logged_in'))
//    {
//       $this->load->helper('form','url');
//       $this->load->library('form_validation');

//      $session_data = $this->session->userdata('logged_in');
//      $data['userid']= $session_data['id'];
//      // $data['title'] = '個人資料';
//      $data['phoneNumber'] = $session_data['phoneNumber'];
//      $data['memberContent'] =  $this->user_model->getContent($data['userid']);



//        $this->form_validation->set_rules('inputName', 'Name', 'trim|required|xss_clean');
//        $this->form_validation->set_rules('inputEmail', 'Email', 'trim|required|xss_clean');
//        $this->form_validation->set_rules('inputSchool', 'School', 'trim|required|xss_clean');
//        // $this->form_validation->set_rules('inputID', 'ID', 'required|xss_clean');

//        // print_r($data['memberContent']);

//        if($this->form_validation->run() == FALSE)
//        {

//              // echo 'sldkjfsd';
//          //Field validation failed.  User redirected to login page
//         // $data['title'] = 'Login';
//         // $this->load->view('templates/header', $data);
//         // $this->load->view('templates/main-menu', $data);
//         // // $this->load->helper(array('form'));
//         // $this->load->view('login/login-master');
//         // $this->load->view('templates/footer');

//         $data['title'] = '';
//         $this->load->view('templates/header', $data);
//         $this->load->view('templates/main-menu', $data);
//         $this->load->view('home/info',$data);
//         $this->load->view('templates/footer');
//        }
//        else
//        {

//           // print_r($this->input->post());
//         $this->user_model->set_info();

//         $data['memberContent'] =  $this->user_model->getContent($data['userid']);
//         $data['title'] = '資料己更新';
//         $this->load->view('templates/header', $data);
//         $this->load->view('templates/main-menu', $data);
//         $this->load->view('home/info',$data);
//         $this->load->view('templates/footer');
//          //Go to private area
//          // redirect('home', 'refresh');
//        }

//    }
//    else
//    {
//      //If no session, redirect to login page
//      redirect('login', 'refresh');
//    }

//  }

// public function rank(){

//     $data['phoneNumber'] = $this->session_data['phoneNumber'];


//     $data['subjects'] = $this->subjects_model->get_subjectsArray();

//       $data['title'] = '';

//         $friendsRank = array(
//           'Lung'=>'98:/assets/img/m/default.icon.png',
//           'Yuku'=>'98:/assets/img/m/default.icon.png',
//           'Taka'=>'86:/assets/img/m/default.icon.png',
//           'Mr.x'=>'98:/assets/img/m/default.icon.png',
//           'Mr.y'=>'78:/assets/img/m/default.icon.png',
//           'Mr.z'=>'98:/assets/img/m/default.icon.png',
//           'Mr.a'=>'83:/assets/img/m/default.icon.png',
//           'Mr.c'=>'52:/assets/img/m/default.icon.png',
//           'Mr.d'=>'67:/assets/img/m/default.icon.png',
//           'me'=>'100:/assets/img/m/1.png',
//         );

//         $HKRank = array(
//           'HK-17'=>'98:/assets/img/m/default.icon.png',
//           'HK-22'=>'98:/assets/img/m/default.icon.png',
//           'HK-16'=>'86:/assets/img/m/default.icon.png',
//           'HK-8'=>'98:/assets/img/m/default.icon.png',
//           'HK-11'=>'78:/assets/img/m/default.icon.png',
//           'HK-31'=>'98:/assets/img/m/default.icon.png',
//           'HK-7'=>'83:/assets/img/m/default.icon.png',
//           'HK-115'=>'52:/assets/img/m/default.icon.png',
//           'HK-21'=>'67:/assets/img/m/default.icon.png',
//           'me'=>'67:/assets/img/m/1.png'
//           );

//           $HKRank2 = array(
//           'HK-17'=>'100:/assets/img/m/default.icon.png',
//           'HK-22'=>'100:/assets/img/m/default.icon.png',
//           'HK-16'=>'100:/assets/img/m/default.icon.png',
//           'HK-8'=>'98:/assets/img/m/default.icon.png',
//           'HK-11'=>'78:/assets/img/m/default.icon.png',
//           'HK-31'=>'98:/assets/img/m/default.icon.png',
//           'HK-7'=>'83:/assets/img/m/default.icon.png',
//           'HK-115'=>'52:/assets/img/m/default.icon.png',
//           'HK-21'=>'67:/assets/img/m/default.icon.png',
//           'me'=>'67:/assets/img/m/1.png'
//           );
//         $Ranks[] = array('subjectName'=>1,'friendRank'=>$friendsRank,'HKRank'=>$HKRank);
//         $Ranks[] = array('subjectName'=>2,'friendRank'=>$friendsRank,'HKRank'=>$HKRank2);

//         $data['rank'] = $Ranks;

//         $this->load->view('templates/header', $data);
//         $this->load->view('templates/main-menu', $data);
//         $this->load->view('home/rank',$data);
//         $this->load->view('templates/footer');    

//   }

 function logout()
 {
   $this->session->unset_userdata('logged_in');
   session_destroy();
   redirect('home', 'refresh');
 }

}