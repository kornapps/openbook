<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
// session_start(); //we need to call PHP's session object to access it through CI
class Exam extends CI_Controller {

var $session_data;

 function __construct()
 {
   parent::__construct();
   $this->load->model('exams_model');
   $this->load->model('questions_model');
   $this->session_data = $this->session->userdata('logged_in');
 }

 function index($qid = null)
 {
    $data['phoneNumber'] = $this->session_data['phoneNumber'];
    $data['title'] = 'Exam';
    $data['question'] = $this->questions_model->get_questions($qid);

    if($data['question']){

    	$subject = $data['question']['subjectCode'];
    	$teacher = $data['question']['TeacherCode'];
	    $data['exams'] = $this->exams_model->get_exames($subject,$teacher);

    }

    // print_r($data['exam']);

    $this->load->view('templates/header', $data);
    $this->load->view('templates/main-menu', $data);
    // $this->load->view('login/login-master');
    $this->load->view('templates/footer');

 }

function view($qid = null)
 {
    $data['phoneNumber'] = $this->session_data['phoneNumber'];
    $data['title'] = 'Exam';
    $data['question'] = $this->questions_model->get_questions($qid);

    if($data['question']){

    	$subject = $data['question']['SubjectCode'];
    	$teacher = $data['question']['TeacherCode'];
	    $data['exams'] = $this->exams_model->get_exames($subject,$teacher);

    }

    $this->load->view('templates/header', $data);
    // $this->load->view('templates/main-menu', $data);
    $this->load->view('exam/view',$data);
    $this->load->view('templates/footer');

 }


}