<!doctype html>
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <!--[if ie]><meta http-equiv='X-UA-Compatible' content="IE=edge,IE=9,IE=8,chrome=1" /><![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    
    <link rel="icon" href="/assets/favicon.ico" type="image/x-icon" />
    <link rel="shortcut icon" href="/assets/favicon.ico" type="image/x-icon" />
    
	<title><?echo $title ?></title>
    
	<meta name="description" content="" />
	<meta name="author" content="MEDIACREED.COM" />
        
        <link rel="stylesheet" href="css/style.css" />
        <link href='http://fonts.googleapis.com/css?family=PT+Sans:400,700,400italic|PT+Sans+Caption:400,700' rel='stylesheet' type='text/css' />        
        
        <!-- SCRIPT IE FIXES -->  
        <!--[if lt IE 9]>
        <script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
        <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]--> 
        <!-- END SCRIPT IE FIXES-->
      
      
        <!-- START TEMPLATE JavaScript load -->
    	<script type="text/javascript" src="js/libs/jquery-1.7.2.min.js"></script>    
        <script type="text/javascript" src="js/libs/modernizr.custom.min.js"></script> 
        <script type="text/javascript" src="js/libs/jquery.wipetouch.js"></script>   
             
        <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=AIzaSyBttxrYoBq8srteD7evmDqnaK6V2Uun42o&amp;sensor=true"></script>
        
        <script type="text/javascript" src="js/libs/jquery.gmap.min.js"></script>
        <script type="text/javascript" src="js/greensock/minified/TweenMax.min.js"></script>    
        <script type="text/javascript" src="js/libs/jquery.timer.js"></script>
        <script type="text/javascript" src="js/libs/jqueryui/1.8/jquery-ui.min.js"></script>
        <script type="text/javascript" src="js/libs/jquery.mousewheel.min.js"></script>
        <script type="text/javascript" src="js/mediacreed/scrollbar/mc.custom.list.js"></script>
        <script type="text/javascript" src="js/mc.modules.animation.js"></script> 
        <link rel="stylesheet" href="js/video-js/video-js.min.css" media="screen" />
        <script type="text/javascript" src="js/video-js/video.min.js"></script>
        <!-- END TEMPLATE JavaScript load -->
        
        <!--<script src="http://vjs.zencdn.net/c/video.js"></script>    
        Careful when using the online version because the destroy method throws an error.    
        Our version has the fix on destroy method. Until it updates we recommend using the JS file from the template.    
        -->
        <script>
            _V_.options.flash.swf = "js/video-js/video-js.swf";
            _V_.options.techOrder = ["html5", "flash", "links"];
            var params = {};
            params.bgcolor = "#000000";
            params.allowFullScreen = "true";       
            _V_.options.flash.params = params;
    
        </script>   
</head>

<body>

<div class="main-template-loader"></div>

<div id="template-wrapper">
    
    <div id="module-container"><!-- start #module-container -->
        <div id="module-background-holder">   
            <div id="module-background-solid1" class="opacity_0"></div>     
        </div>
        <div id="module-container-holder" class="module-position-lc"  data-id="module-position-lc">
            <div id="module-full-width-gallery"  class="module-full-width-gallery " > <!-- .shadow-side-all-->
                <div id="module-full-width-holder">
                    <?php foreach ($markets as $key => $value): ?>
                    <div class="full-width-item">
                        <img src="/assets/img/market/<?php echo $value['webImg'];?>" class="opacity_0" onload="animateThumb(this)" alt=""  />
                        <div id="thumb-image-hover" class="hover-default">
                            <div class="background opacity_6"></div>
                            <div class="zoom-gallery"></div>
                            <div class="item-title"><p><?php echo $value['infoCN'];?></p></div>
                        </div>               
                    </div>
                    <?php endforeach; ?>
                </div>             
            </div>
            
        </div>
        <div id="module-scrollbar-holder_v2">
            <div id="module-scrollbar-background" class="opacity_8"></div>
            <div id="module-scrollbar-dragger"></div>
        </div>  
        
        <div id="full-width-preview"><!-- start .full-width-preview" -->
            <div class="full-width-preview-background opacity_9_7"></div>
            <div id="full-width-preview-media-holder">
                <div class="full-width-preview-media-holder-background opacity_0"></div>
                <div class="full-width-preview-media-loader"></div>
                <div id="preview-media-holder">   
                    <!--<div id="link" data-url="http://www.google.com" data-target="_blank"></div>-->
                    <?php foreach ($markets as $key => $value):?> 
                    <div id="preview-media-image" data-url="/assets/img/market/<?php echo $value['webImg'];?>" data-title="Custom Title 1" data-alt="Custom Alt 1"></div>
<!--                     <div id="video-wrapper" data-video-type="vimeo" data-width="640" data-height="390"
                          data-url="http://player.vimeo.com/video/31423544?portrait=0&color=ffffff">
                    </div> -->
                    <?php endforeach;?>
                </div>            
            </div>
            <div class="preview-arrow-close">
                <div class="preview-arrow-backg opacity_2"></div>
                <div class="preview-arrow-close-sign"></div>
            </div>
            <div class="preview-arrow-backward">
                <div class="preview-arrow-backg opacity_2"></div>
                <div class="preview-arrow-backward-sign"></div>
            </div>
            <div class="preview-arrow-forward">
                <div class="preview-arrow-backg opacity_2"></div>
                <div class="preview-arrow-forward-sign"></div>
            </div>  
            <div class="preview-smartphone-info">
                <div class="preview-smartphone-info-backg opacity_1"></div>
                <span class="show_info">顯示資料</span>
                <span class="hide_info">隠藏資料</span>
            </div>          
            <div id="full-width-preview-info-holder" class="module-position-rc">
                <div id="full-width-info-container" class="full-width-info-container shadow-side-left">
                    <div class="full-width-info-holder">
                        <?php foreach ($markets as $key => $value):?> 
                        <div class="full-width-info-holder-desc">
                            <p><?php echo $value['infoCN'];?></p>
                            <div class="custom-separator"></div>
                            <span><?php echo $value['content'];?></span>
                            <span><?php echo $value['contact'];?></span>
                            <!-- start social facebook, twitter and pinterest -->
                            <div id="social-holder">
                                <div id="social-holder-back" class="opacity_2"></div>
                                <div data-src="//www.facebook.com/plugins/like.php?href=http%3A%2F%2Fthemes.mediacreed.com%2F%23portfolio&amp;send=false&amp;layout=button_count&amp;width=85&amp;show_faces=false&amp;font=verdana&amp;colorscheme=light&amp;action=like&amp;height=21" 
                                     style="float:left; border:none; overflow:hidden; width:82px; height:21px; margin: 6px 5px 5px 10px;"></div>
                                <div data-src="https://platform.twitter.com/widgets/tweet_button.html?url=http://themes.mediacreed.com/html/synergy/#portfolio&text=Synergy - Awesome HTML Portfolio Template&via=mediacreed" 
                                     style="float:left; border:none; overflow:hidden; width:82px; height:21px; margin: 6px 5px 5px 10px;"></div>     
                                <a href="http://pinterest.com/pin/create/button/?url=http%3A%2F%2Fthemes.mediacreed.com%2Fhtml%2Fsynergy%2F&media=http%3A%2F%2Fthemes.mediacreed.com%2Fdesc_images%2Fprofile_preview_synergy.jpg&description=Synergy%20is%20a%20responsive%20%26%20interactive%20fullscreen%20portfolio%20for%20artists%2C%20photographers%2C%20media%20agencies%2C%20restaurants%20and%20for%20everyone%20that%20wants%20to%20showcase%20their%20portfolio%20in%20a%20professional%20way." class="pin-it-button" count-layout="horizontal" target="_blank"><img border="0" src="//assets.pinterest.com/images/PinExt.png" title="Pin It" /></a>                                
                            </div>
                            <!-- end social facebook, twitter and pinterest -->
                        </div>
                        <?php endforeach;?>                        
                    </div>
                </div>        
            </div>        
        </div><!-- end .full-width-preview" -->
    </div><!-- end #module-container -->
</div>    

<!-- START LOADING CONTAINER AND ANIMATION-->
<div id="load-container"></div>
<div id="loading-animation">
	<img src="/assets/loaders/loader.gif" width="16" height="11" alt="Synergy - loading animation"/>
</div>
<!-- END LOADING CONTAINER AND ANIMATION--> 

</body>
</html>
