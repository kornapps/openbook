﻿<!doctype html>
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head></hrad>

<body>

<div class="main-template-loader"></div>

<div id="template-wrapper">
    
    <div id="module-container"><!-- start #module-container -->
        <div id="module-background-holder">
            <img src="/assets/backg/bg07.jpg" onload="animateModuleBackground(this)" class="module-background" id="module-background" alt="" />
        </div>  
        <div id="module-container-holder" class="module-position-cc opacity_9 "  data-id="module-position-cc">
            <div id="module-text-page"  class="module-philosophy shadow-side-all">
                <div id="module-wrapper">
                    <div id="module-text-page-holder-philosophy">               
<!--                          <div class="title-holder">
                            <span class="title-text_normal">全城響應</span>
                        </div> -->
                        <!-- <div class="custom-separator"></div> -->
                        <div class="media-holder">
                            <img src="/assets/img/starpoint_title.png" width="100%" class="opacity_0" onload="animateThumb(this)"/>
                        </div>
                        <div class="custom-separator"></div>
                        <div class="main-text-holder">
                            <table class="market-table">
                                <tr>
                                    <th class="mtb1">投稿</th>
                                    <th class="mtb2">類別</th>
                                    <th class="mtb3">內容</th>
                                    <th class="mtb4">電話</th>
                                </tr>
                                <?php foreach ($markets as $key => $value): ?>
                                <tr>
                                    <td><img src="/assets/img/m/1.png" width="60px"></td>
                                    <td><?php echo $value['type'];?></td>
                                    <td><?php echo $value['content'];?></td>    
                                    <td><?php echo $value['contact'];?></td>
                                </tr>
                                <?php endforeach; ?>
                            </table>
                        </div>
                    </div>
                </div>                   
            </div>
            <div id="module-scrollbar-holder">
                <div id="module-scrollbar-background" class="opacity_4"></div>
                <div id="module-scrollbar-dragger"></div>
            </div>    
        </div>
            
    </div>
</div>

<!-- START LOADING CONTAINER AND ANIMATION-->
<div id="load-container"></div>
<div id="loading-animation">
	<img src="/assets/loaders/loader.gif" width="16" height="11" alt="Synergy - loading animation"/>
</div>
<!-- END LOADING CONTAINER AND ANIMATION--> 
    
</body>
</html>
