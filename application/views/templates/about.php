﻿<!doctype html>
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head></hrad>

<body>

<div class="main-template-loader"></div>

<div id="template-wrapper">
    
    <div id="module-container"><!-- start #module-container -->
        <div id="module-background-holder">
            <img src="/assets/backg/bg01.jpg" onload="animateModuleBackground(this)" class="module-background" id="module-background" alt="" />
        </div>  
        <div id="module-container-holder" class="module-position-cc opacity_9 "  data-id="module-position-cc">
            <div id="module-text-page"  class="module-philosophy shadow-side-all">
                <div id="module-wrapper">
                    <div id="module-text-page-holder-philosophy">               
                         <div class="title-holder">
                            <span class="title-text_normal">有關 OpenBook 開學</span>
                        </div>
                        <div class="custom-separator"></div>
                        <div class="media-holder">
                            <img src="/assets/img/share_main.jpg" width="100%" class="opacity_0" onload="animateThumb(this)"/>
                        </div>
                        <div class="custom-separator"></div>
                        <div class="main-text-holder">
                            <p>《OpenBook 開學》 自2014年創立以來，一直致力成為最全面滿足所有香港人的全方位免費教育平台，不單為用戶提供最全面的教育服務，更讓任何人發表心得、意見和交流。</p>
                            <p>我們相信教育和知識能夠改變下一代和社會的未來，就是這份信念，讓我們匯聚眾多專業的教育人士，包括歷任校長、中小學教師、教育界代表，將多年的教學經驗毫無保留、傾囊相授。</p>
                            <p>《OpenBook 開學》除免費指南外， 主打教育問答遊戲，配以專業導師即時短片解釋，令學生透過短片提升自己的水平。在未來的日子，我們將繼續與教育界人士並駕齊驅，為學生、家長和教育工作者不斷更新內容、提升教育服務及各項功能，務求令學生對學習重拾興趣，目標是成為全港最強的免費教育平台。</p>
                        </div>
                    </div>
                </div>                   
            </div>
            <div id="module-scrollbar-holder">
                <div id="module-scrollbar-background" class="opacity_4"></div>
                <div id="module-scrollbar-dragger"></div>
            </div>    
        </div>
            
    </div>
</div>

<!-- START LOADING CONTAINER AND ANIMATION-->
<div id="load-container"></div>
<div id="loading-animation">
	<img src="/assets/loaders/loader.gif" width="16" height="11" alt="Synergy - loading animation"/>
</div>
<!-- END LOADING CONTAINER AND ANIMATION--> 
    
</body>
</html>
