﻿a<!doctype html>
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,IE=9,IE=8,chrome=1" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta itemprop="image" content="/assets/favicon.png" />
    <link rel="icon" href="/assets/favicon.ico" type="image/x-icon" />
    <link rel="shortcut icon" href="assets/favicon.ico" type="image/x-icon" />
    
    
	<title>OpenBook</title>
    
	<meta name="description" content="" />
	<meta name="author" content="MEDIACREED.COM" />
    <!--<meta name="fragment" content="!">-->
    
        <link rel="stylesheet" href="/assets/css/style.css" />
        <link href='http://fonts.googleapis.com/css?family=PT+Sans:400,700,400italic|PT+Sans+Caption:400,700' rel='stylesheet' type='text/css' />        
        
        <!-- SCRIPT IE FIXES -->  
        <!--[if lt IE 9]>
        <script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
        <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]--> 
        <!-- END SCRIPT IE FIXES-->
      
      
        <!-- START TEMPLATE JavaScript load -->
    	<script type="text/javascript" src="/assets/js/libs/jquery-1.7.2.min.js"></script>    
        <script type="text/javascript" src="/assets/js/libs/modernizr.custom.min.js"></script> 
        <script type="text/javascript" src="/assets/js/libs/jquery.wipetouch.js"></script>   
             
        <!--<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&sensor=true"></script> -->
        
        <script type="text/javascript" src="/assets/js/libs/jquery.gmap.min.js"></script>
        <script type="text/javascript" src="/assets/js/greensock/minified/TweenMax.min.js"></script>    
        <script type="text/javascript" src="/assets/js/libs/jquery.timer.js"></script>
        <script type="text/javascript" src="/assets/js/libs/jqueryui/1.8/jquery-ui.min.js"></script>
        <script type="text/javascript" src="/assets/js/libs/jquery.mousewheel.min.js"></script>
		<script type="text/javascript" src="/assets/js/audio-js/jquery.jplayer.min.js"></script>
        <script type="text/javascript" src="/assets/js/audio-js/jplayer.playlist.min.js"></script>
        <script type="text/javascript" src="/assets/js/mediacreed/scrollbar/mc.custom.list.js"></script>
        <script type="text/javascript" src="/assets/js/mc.modules.animation.js"></script>
        <link rel="stylesheet" href="/assets/js/video-js/video-js.min.css" media="screen" />
        <script type="text/javascript" src="/assets/js/video-js/video.min.js"></script>
        <!-- END TEMPLATE JavaScript load ---->
        
        <!--<script src="http://vjs.zencdn.net/c/video.js"></script>    
        Careful when using the online version because the destroy method throws an error.    
        Our version has the fix on destroy method. Until it updates we recommend using the JS file from the template.    
        -->
        <script>
            _V_.options.flash.swf = "js/video-js/video-js.swf";
            _V_.options.techOrder = ["html5", "flash", "links"];
            var params = {};
            params.bgcolor = "#000000";
            params.allowFullScreen = "true";       
            _V_.options.flash.params = params;
    
        </script>    
</head>

<body>

<div class="main-template-loader"></div>

<div id="template-wrapper">
    <div id="menu-container"><!-- start #menu-container -->        
        <div class="menu-content-holder"><!-- start .menu-content-holder -->
            <div class="menu-background"></div>
            <div id="template-logo" class="template-logo" data-href="#portfolio.html"></div> 
            <div id="template-menu" class="template-menu" data-current-module-type="slideshow" data-side="none" data-href="#portfolio.html"><!-- start #template-menu -->
                <div class="menu-option-holder" data-module-type="external" data-side="height">
                    <div id="menu-option-background" class="menu-option-background"></div>
                    <!-- <div id="menu-option-text" class="menu-option-text"><a  href="#subjects.html" data-path-href="html/subjects/">首 頁</a></div>                 -->
                    <div id="menu-option-text" class="menu-option-text"><a  href="http://openbook.la/index.php/games/" >首 頁</a></div>  
                </div>
                <div class="menu-option-holder" data-module-type="home2" data-side="none">
                    <div id="menu-option-background" class="menu-option-background"> </div>
                    <div id="menu-option-text" class="menu-option-text"><a href="#portfolio.html" data-path-href="html/portfolio/">個 人 資 料</a></div>
                </div> 
                <div class="menu-option-holder" data-module-type="page_columns" data-side="height">
                    <div id="menu-option-background" class="menu-option-background"> </div>
                    <div id="menu-option-text" class="menu-option-text"><a href="#subjects.html" data-path-href="html/subjects/">學 科 類 型</a></div>
                </div>  
                <div class="menu-option-holder" data-module-type="page_columns" data-side="height">
                    <div id="menu-option-background" class="menu-option-background"> </div>
                    <div id="menu-option-text" class="menu-option-text"><a href="#teachers.html" data-path-href="html/teachers/">導 師 作 者</a></div>
                </div>
                <div class="menu-option-holder">
                    <div id="menu-option-background" class="menu-option-background"> </div>
                    <div id="menu-option-text" class="menu-option-text">
                        <a href="#" data-path-href="html/rank/">龍 虎 榜</a>
                        <div class="menu-option-sign">+</div>
                    </div>
                    <div class="sub-menu-holder">
                        <div class="sub-menu-option-holder" data-module-type="pricing_tables" data-side="height">
                            <div class="sub-menu-option-background"></div>
                            <div class="sub-menu-option-text"><a href="#hkrank.html" data-path-href="html/rank/hk/">全 港 排 名</a></div>
                        </div>
                        <div class="sub-menu-option-holder" data-module-type="pricing_tables" data-side="height">
                            <div class="sub-menu-option-background"></div>
                            <div class="sub-menu-option-text"><a href="#friendrank.html" data-path-href="html/rank/friend/">朋 友 排 名</a></div>
                        </div>
                    </div>  
                </div>
                <div class="menu-option-holder" data-module-type="text_page" data-side="none">
                    <div id="menu-option-background" class="menu-option-background"> </div>
                    <div id="menu-option-text" class="menu-option-text"><a href="#market.html" data-path-href="html/market/">大 橫 行</a></div>
                </div> 
                <div class="menu-option-holder" data-module-type="text_page" data-side="none">
                    <div id="menu-option-background" class="menu-option-background"> </div>
                    <div id="menu-option-text" class="menu-option-text"><a href="#starpoint.html" data-path-href="html/starpoint/">全 城 響 應</a></div>
                </div> 
                <div class="menu-option-holder" data-module-type="showreel" data-side="none">
                    <div id="menu-option-background" class="menu-option-background"> </div>
                    <div id="menu-option-text" class="menu-option-text"><a href="#showreel.html" data-path-href="html/showreel/">我 要 星 星</a></div>
                </div>                                         
                <div class="menu-option-holder" data-module-type="external" data-side="height">
                    <div id="menu-option-background" class="menu-option-background"> </div>
                    <div id="menu-option-text" class="menu-option-text"><a href="http://openbook.la/index.php/home/logout/" data-path-href="html/contact/">登 出</a></div>
                </div>            
            </div><!-- end #template-menu -->  
<!--             <div id="template-smpartphone-menu">
                <select>
                    <option value="http://openbook.la/index.php/games/">首頁</option>
                    <option value="#portfolio.html">個人資料</option>
                    <option value="#subjects.html">學 科 類 型</option>
                    <option value="#teachers.html">導 師 作 者</option>
                    <option value="#">龍 虎 榜 +</option>
                        <option value="#hkrank.html">  - 全港排行</option>
                        <option value="#friendrank.html">  - 朋友排行</option>
                    <option value="#market.html">大 橫 行</option>
                    <option value="#starpoint.html">全 城 響 應</option>    
                    <option value="#">我 要 星 星</option>
                    <option value="http://openbook.la/index.php/home/logout/">登 出</option>
                </select>
            </div> -->
			<div id="audio-player" data-auto-play="false" data-player-opened="true" data-player-volume="0.6">
                <div id="audio-items">
                    <div id="audio-track" data-src="/assets/audio/on_top_of_the_world" data-title="Tim McMorris - Top Of The World"></div>
                    <div id="audio-track" data-src="/assets/audio/overwhelmed" data-title="Tim McMorris - Overwhelmed"></div>
                    <div id="audio-track" data-src="/assets/audio/playing" data-title="Tim McMorris - Playing"></div>
                    <div id="audio-track" data-src="/assets/audio/happy" data-title="Tim McMorris - Happy"></div>                    
                </div>
                <div id="audio-player-tooltip" class="opacity_0">
                    <div id="audio-tooltip-holder">
                        <div class="audio-tooltip-backg"></div>
                        <div class="audio-player-tooltip-title"><span></span></div>
                    </div>
                    <div class="audio-player-tooltip-hook"></div>                    
                </div>
                <div id="audio-player-holder">
                    <div id="audio-options-button">
                        <div class="audio-options-button-backg"></div>
                        <div class="audio-options-button-sign"></div>
                    </div>
                    <div id="audio-player-options-holder">                    
                        <div id="audio-prev-track">
                            <div class="audio-controls-backg"></div>
                            <div class="audio-prev-track-sign"></div>
                        </div>
                        <div id="audio-play-pause-track">
                            <div class="audio-controls-backg"></div>
                            <div class="audio-play-track-sign"></div>
                            <div class="audio-pause-track-sign"></div>
                        </div>
                        <div id="audio-next-track">
                            <div class="audio-controls-backg"></div>
                            <div class="audio-next-track-sign"></div>
                        </div>
                        <div id="audio-volume-speaker">
                            <div class="audio-controls-backg"></div>
                            <div class="audio-volume-on-sign"></div>
                            <div class="audio-volume-off-sign"></div>
                        </div>
                        <div id="audio-volume-bar">
                            <div class="audio-volume-bar-scrub-backg"></div>
                            <div class="audio-volume-bar-scrub-current"></div>
                        </div>                        
                    </div>                    
                </div>
            </div>
            <footer>    
                <div id="footer-social">            
                    <div id="footer-social-holder">
                        <ul>
                            <li class="twitter"><a href="http://twitter.com/home?status=http://openbook.la" target="_blank"></a></li>
                            <li class="facebook"><a href="http://www.facebook.com/sharer/sharer.php?u=http://openbook.la" target="_blank"></a></li>
                            <li class="google"><a href="https://plus.google.com/share?url=http://openbook.la" target="_blank"></a></li>
                        </ul>
                    </div>                   
                </div>    
                <div id="footer-text"><!-- start #footer-text -->
                    <div id="footer-copyright"><a href="#">&copy; Openbook 2014</a></div>           
                </div><!-- end #footer -->    
            </footer> 
        </div><!-- end .menu-content-holder-->
        
        <div id="menu-hider">
            <div id="menu-hider-background"></div>
            <div id="menu-hider-icon"></div>
        </div>
    </div><!-- end #menu-container -->

    <div id="module-container">
    </div>
    
</div>

<div id="load-container">
</div>

<div id="loading-animation">
	<img src="/assets/loaders/loader3.gif" width="256" height="39" alt="Synergy - loading animation"/>
</div>
<div id="footer-social-tooltip"></div>

<div id="console-log"></div>
<!-- <script type="text/javascript" src="/assets.pinterest.com/js/pinit.js"></script>  -->
</body>
</html>
