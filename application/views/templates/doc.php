<div class="panel-group" id="accordion">
  <?php foreach ($docs as $doc => $value) : ?> 
  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $value['seqNo'];?>">
          <?php echo $value['title'];?>
        </a>
      </h4>
    </div>
    <div id="collapse<?php echo $value['seqNo'];?>" class="panel-collapse collapse">
      <div class="panel-body">
      	<?php echo $value['content'];?>
      </div>
    </div>
  </div>
<?php endforeach; ?>
</div>
