<!doctype html>
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <!--[if ie]><meta http-equiv='X-UA-Compatible' content="IE=edge,IE=9,IE=8,chrome=1" /><![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    
    <link rel="icon" href="/assets/favicon.ico" type="image/x-icon" />
    <link rel="shortcut icon" href="/assets/favicon.ico" type="image/x-icon" />
    
	<title><?echo $title ?></title>
    
	<meta name="description" content="" />
	<meta name="author" content="kornapps.com" />
        
</head>

<body>

<div class="main-template-loader"></div>

<div id="template-wrapper">
    
    <div id="module-container"><!-- start #module-container -->
        <div id="module-background-holder">
            <!--<div id="module-background-solid1" class="opacity_0"></div>-->
        	<img src="/assets/backg/bg02.jpg" onload="animateModuleBackground(this)" class="module-background" id="module-background" alt="" />
        </div>	
        <div id="module-container-holder" class="module-position-cc"  data-id="module-position-cc">
            <div id="module-home-layout2"  class="module-home-layout2" ><!-- .shadow-side-all -->
                <div id="module-home-layout2-holder">
                    <div id="home-layout2-banner" data-auto-play="false" data-time="4">
                        <div id='standard-banner'  class="shadow-side-all">
                        	<a href="#" target="_blank" class="selected" onClick="return false;">
                        		<img src='/assets/img/level/lv_1_bg.png' onload="animateThumb(this)" class="opacity_0"/>
                                <div class="banner-desc box-sizing banner-desc-tl">
                                    <div class="desc-background opacity_6"></div>
                                    <?php if ($phoneNumber == 'guest'):?>
                                    <span>
                            			<p><img src="/assets/img/m/default.icon.png" width="140"></p>
                                        <!-- <canvas id="canvas" height="320"></canvas> -->
                                        <!-- <div id="container" style="min-width: 310px; max-width: 400px; height: 400px; margin: 0 auto"></div> -->
                            		</span>
                                   <?php else: ?>
                                    <span>
                                        <p><img src="<?php echo $memberContent['imagePath']; ?>" width="140"></p>
                                        <p><?php echo $memberContent['displayName']; ?></p>
                                    </span>
                                    <?php endif;?>
                                </div>                        		
                        	</a>
<!--                             <a href="#" target="_blank">
                        		<div id="dataSrc" data-src="/assets/backg/p_06.jpg"></div>                                
                        		<div class="banner-desc box-sizing banner-desc-cr">
                                    <div class="desc-background opacity_6"></div>
                                    <span>
                            			<h1>DECRIPTION TITLE 2</h1>
                                        <p>Aenean diam urna, suscipit tempor consectetur sed, mattis quis libero. Suspendisse porta, dolor ac egestas pulvinar, orci ante viverra ligula, in convallis nisl orci euismod felis.</p>
                            		</span>
                                </div>
                        	</a> -->
                        </div>
						<!-- <div class="banner-timer-graphic opacity_4"></div> -->
                        <!-- <div id="standard-banner-controls"> -->
                            <!-- <div id="control-pin" class="selected"><div class="control-pin-hover opacity_1"></div></div> -->
                            <!-- <div id="control-pin" class=""><div class="control-pin-hover opacity_0"></div></div> -->
                            <!-- <div id="control-pin" class="last"><div class="control-pin-hover opacity_0"></div></div>                             -->
                        <!-- </div> -->
                    </div>                
                    <div class="home-layout2-content shadow-side-all">

                        <!-- <div class="home-layout2-title">Why Choose Us?</div>
                        <ul>
                            <li>
                                <div class="layout2-icon-holder1"></div>
                                <div class="layout2-description">
                                    <p>RESPONSIVE</p>
                                    <span>Phasellus sit amet sapien risus. Donec venenatis, enim in mollis bibendum, nisi ligula aliquet augue, ut faucibus massa ipsum sed ipsum.</span>    
                                </div>
                            </li>
                            <li>
                                <div class="layout2-icon-holder2"><div class="layout2-icon2"></div></div>
                                <div class="layout2-description">
                                    <p>EASILY CUSTOMIZABLE</p>
                                    <span>Phasellus sit amet sapien risus. Donec venenatis, enim in mollis bibendum, nisi ligula aliquet augue, ut faucibus massa ipsum sed ipsum.</span>    
                                </div>
                            </li>
                            <li>
                                <div class="layout2-icon-holder3"><div class="layout2-icon3"></div></div>
                                <div class="layout2-description">
                                    <p>SUPPORT</p>
                                    <span>Phasellus sit amet sapien risus. Donec venenatis, enim in mollis bibendum, nisi ligula aliquet augue, ut faucibus massa ipsum sed ipsum.</span>    
                                </div>
                            </li>
                            <li>
                                <div class="layout2-icon-holder4"><div class="layout2-icon4"></div></div>
                                <div class="layout2-description">
                                    <p>OPTIMIZED SEO</p>
                                    <span>Phasellus sit amet sapien risus. Donec venenatis, enim in mollis bibendum, nisi ligula aliquet augue, ut faucibus massa ipsum sed ipsum.</span>    
                                </div>
                            </li>
                        </ul> -->
                    </div>
<!--                     <div class="home-layout-clients shadow-side-all">
                        <div class="home-layout-clients-title">Our Clients</div>
                        <span class="clear"></span>
                        <a id="client1" href="#">
                            <div class="client-out"></div>
                            <div class="client-over opacity_0"></div>
                        </a>
                        <a id="client2" href="#">
                            <div class="client-out"></div>
                            <div class="client-over opacity_0"></div>
                        </a>
                        <a id="client3" href="#">
                            <div class="client-out"></div>
                            <div class="client-over opacity_0"></div>
                        </a>
                        <a id="client4" href="#">
                            <div class="client-out"></div>
                            <div class="client-over opacity_0"></div>
                        </a>
                        <a id="client5" href="#">
                            <div class="client-out"></div>
                            <div class="client-over opacity_0"></div>
                        </a>                    
                    </div> -->
                </div>
            </div>
            
        </div>
        <div id="module-scrollbar-holder_v2">
            <div id="module-scrollbar-background" class="opacity_8"></div>
            <div id="module-scrollbar-dragger"></div>
        </div>
    </div><!-- end #module-container -->
</div>    

<!-- START LOADING CONTAINER AND ANIMATION-->
<div id="load-container"></div>
<div id="loading-animation">
	<img src="/assets/loaders/loader.gif" width="16" height="11" alt="Synergy - loading animation"/>
</div>
<!-- END LOADING CONTAINER AND ANIMATION--> 

</body>

</html>
