<?php if($phoneNumber == 'guest'):?>
    <!-- Side Menu -->
    <a id="menu-toggle" href="#" class="btn btn-primary btn-lg toggle"><i class="fa fa-reorder"></i></a>
    <div id="sidebar-wrapper">
      <ul class="sidebar-nav ">
        <!-- <a id="menu-close" href="#" class="btn btn-default btn-lg pull-right toggle"><i class="fa fa-times"></i></a> -->
        <div><img src="/assets/img/menu_logo.jpg"></div>
        <!-- <li><a href="/index.php/home"><span class="menuicon sprite-home"></span>Home</a></li> -->
        <!-- <li><a href="/index.php/home"><span class="menuicon sprite-status"></span>個人資料</a></li> -->
        <li><a href="/index.php/home/subjects"><span class="menuicon sprite-subject"></span>學科類型</a></li>        
        <li><a href="/index.php/home/teachers"><span class="menuicon sprite-tutors"></span>導師作者</a></li>
        <li><a href="/index.php/home/rank"><span class="menuicon sprite-dragon"></span>龍虎榜</a></li>
        <li><a href="/index.php/market"><span class="menuicon sprite-memo"></span>全城響應</a></li>
        <li><a href="/index.php/starpoint"><span class="menuicon sprite-tai"></span>大橫行</a></li>
        <li><a href="/index.php/market"><span class="menuicon sprite-star"></span>我要星星</a></li>
        <!-- <li><a href="/index.php/home/info"><span class="menuicon sprite-pw"></span>其他個人資料</a></li> -->
        <li><a href="/index.php/home/logout"><span class="menuicon sprite-status"></span>Logout</a></li>
      </ul>

    </div>
    <!-- /Side Menu -->
<div class="header">
<ul class="nav nav-pills pull-right">
<h3 class="text-muted"><?php echo $title;?></h3>
</div>
<?php else:?>
    <!-- Side Menu -->
    <a id="menu-toggle" href="#" class="btn btn-primary btn-lg toggle"><i class="fa fa-reorder"></i></a>
    <div id="sidebar-wrapper">
      <ul class="sidebar-nav ">
        <!-- <a id="menu-close" href="#" class="btn btn-default btn-lg pull-right toggle"><i class="fa fa-times"></i></a> -->
        <div><img src="/assets/img/menu_logo.jpg"></div>
        <li><a href="/index.php/home"><span class="menuicon sprite-home"></span>Home</a></li>
        <li><a href="/index.php/home"><span class="menuicon sprite-status"></span>個人資料</a></li>
        <li><a href="/index.php/home/subjects"><span class="menuicon sprite-subject"></span>學科類型</a></li>        
        <li><a href="/index.php/home/teachers"><span class="menuicon sprite-tutors"></span>導師作者</a></li>
        <li><a href="/index.php/home/rank"><span class="menuicon sprite-dragon"></span>龍虎榜</a></li>
        <li><a href="/index.php/market"><span class="menuicon sprite-memo"></span>全城響應</a></li>
        <li><a href="/index.php/starpoint"><span class="menuicon sprite-tai"></span>大橫行</a></li>
        <li><a href="/index.php/market"><span class="menuicon sprite-star"></span>我要星星</a></li>
        <li><a href="/index.php/home/info"><span class="menuicon sprite-pw"></span>其他個人資料</a></li>
        <li><a href="/index.php/home/logout"><span class="menuicon sprite-status"></span>Logout</a></li>
        
        <li>
            <ul class="list-group userInfoBox">
                <li class="list-group-item">User<br /><?php echo $memberContent['displayName']; ?></li>
                <li class="list-group-item">Star Dollars<br /> :111</li>
                <li class="list-group-item">進度<br />12345</li>
            </ul>
        </li>
      </ul>

    </div>
    <!-- /Side Menu -->
<div class="header">
<ul class="nav nav-pills pull-right">
<h3 class="text-muted"><?php echo $title;?></h3>
</div>
<?php endif;?>