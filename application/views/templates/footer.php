</div> 
<!-- Container end -->
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
    <script src="/application/libraries/js/bootstrap.min.js"></script>
    <script src="/application/libraries/js/custom.js"></script>
    <script src="/assets/js/jquery.imagemapster.min.js"></script>
    <!-- Custom JavaScript for the Side Menu and Smooth Scrolling - Put in a custom JavaScript file to clean this up -->
     <script>
    //     $("#menu-close").click(function(e) {
    //         e.preventDefault();
    //         $("#sidebar-wrapper").toggleClass("active");
    //     });
    //     $("#menu-toggle").click(function(e) {
    //         e.preventDefault();
    //         $("#sidebar-wrapper").toggleClass("active");
    //     });
     </script>
    <script>

    var xref = {
            dip: "Everything here is good for you but this one. <b>Don't be a dip!</b>"
        };

    var defaultDipTooltip = 'I know you want the dip. But it\'s loaded with saturated fat, just skip it '
        +'and enjoy as many delicious, crisp vegetables as you can eat.';

    var image = $('#game_select');

    image.mapster(
    {
        noHrefIsMask: false,
        fillColor: '0a7a0a',
        fillOpacity: 0.7,
        mapKey: 'data-group',
        strokeWidth: 2,
        stroke: true,
        strokeColor: 'F88017',
        render_select: {
            fillColor: 'adadad'
            },
        areas: [
        {
          key: 'blue-circle',
          includeKeys: 'rectangle',
          stroke: false
        },
        {
          key: 'rectangle',
          stroke: true,
          strokeWidth: 3
        },
        {
          key: 'outer-circle',
          includeKeys: 'inner-circle-mask,outer-circle-mask',
          stroke: true
        },
        {
          key: 'outer-circle-mask',
          isMask: true,
          fillColorMask: 'ff002a'
        },
        {
          key: 'inner-circle-mask',
          fillColorMask: 'ffffff',
          isMask: true
        }
            ]
    });

      $(function() {
        $('a[href*=#]:not([href=#])').click(function() {
          if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') 
            || location.hostname == this.hostname) {

            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
            if (target.length) {
              $('html,body').animate({
                scrollTop: target.offset().top
              }, 1000);
              return false;
            }
          }
        });
      });

      // $('.carousel').carousel()
    $('#myTab a').click(function (e) {
      e.preventDefault()
      $(this).tab('show')
    })


    var myid ;//= $(this).attr('id');
    var setID ;//= '.'+myid+'-p';

     $('.gselect').hover(
         function () {
          myid = $(this).attr('id');
          setID = '#'+myid+'-p';
           $(setID).css({"display":"block"});
         }, 
         function () {
           $(setID).css({"display":"none"});
         }
     );



    </script>
  </body>
</html>