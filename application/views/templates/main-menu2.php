    <div id="menu-container"><!-- start #menu-container -->        
        <div class="menu-content-holder"><!-- start .menu-content-holder -->
            <div class="menu-background"></div>
            <div id="template-logo" class="template-logo" data-href="#portfolio.html"></div> 
            <div id="template-menu" class="template-menu" data-current-module-type="slideshow" data-side="none" data-href="#portfolio.html"><!-- start #template-menu -->
                <div class="menu-option-holder">
                    <div id="menu-option-background" class="menu-option-background"> </div>
                    <div id="menu-option-text" class="menu-option-text">
                        <a href="#">首 頁</a>
                        <div class="menu-option-sign">+</div>
                    </div>                
                    <div class="sub-menu-holder">
                        <div class="sub-menu-option-holder" data-module-type="slideshow" data-side="none">
                            <div class="sub-menu-option-background"></div>
                            <div class="sub-menu-option-text"><a href="#home_layout_1.html" data-path-href="html/home/">Home Layout 1</a></div>
                        </div>
                        <div class="sub-menu-option-holder" data-module-type="home2" data-side="none">
                            <div class="sub-menu-option-background"></div>
                            <div class="sub-menu-option-text"><a href="#home_layout_2.html" data-path-href="html/home/">Home Layout 2</a></div>
                        </div>
                        <div class="sub-menu-option-holder" data-module-type="home3" data-side="none">
                            <div class="sub-menu-option-background"> </div>
                            <div class="sub-menu-option-text"><a href="#home_layout_3.html" data-path-href="html/home/">Home Layout 3</a></div>
                        </div>
                        
                    </div>         
                </div>
                <div class="menu-option-holder" data-module-type="news" data-side="height">
                    <div id="menu-option-background" class="menu-option-background"> </div>
                    <div id="menu-option-text" class="menu-option-text"><a href="#subjects.html" data-path-href="html/subjects/">學科類型</a></div>
                </div>  
                <div class="menu-option-holder" data-module-type="news" data-side="height">
                    <div id="menu-option-background" class="menu-option-background"> </div>
                    <div id="menu-option-text" class="menu-option-text"><a href="#teachers.html" data-path-href="html/teachers/">導師作者</a></div>
                </div>  
                <div class="menu-option-holder" data-module-type="news" data-side="height">
                    <div id="menu-option-background" class="menu-option-background"> </div>
                    <div id="menu-option-text" class="menu-option-text"><a href="#teachers.html" data-path-href="html/teachers/">龍虎榜</a></div>
                </div>  
                <div class="menu-option-holder" data-module-type="news" data-side="height">
                    <div id="menu-option-background" class="menu-option-background"> </div>
                    <div id="menu-option-text" class="menu-option-text"><a href="#teachers.html" data-path-href="html/teachers/">全城響應</a></div>
                </div>  
                <div class="menu-option-holder" data-module-type="news" data-side="height">
                    <div id="menu-option-background" class="menu-option-background"> </div>
                    <div id="menu-option-text" class="menu-option-text"><a href="#teachers.html" data-path-href="html/teachers/">大橫行</a></div>
                </div>  
                <div class="menu-option-holder" data-module-type="news" data-side="height">
                    <div id="menu-option-background" class="menu-option-background"> </div>
                    <div id="menu-option-text" class="menu-option-text"><a href="#teachers.html" data-path-href="html/teachers/">我要星星</a></div>
                </div>  
                <div class="menu-option-holder" data-module-type="contact" data-side="custom">
                    <div id="menu-option-background" class="menu-option-background"> </div>
                    <div id="menu-option-text" class="menu-option-text"><a href="#contact.html" data-path-href="html/contact/">登出</a></div>
                </div>            
            </div><!-- end #template-menu -->  
            <div id="template-smpartphone-menu">
                <select>
                    <option value="#">Navigate to...</option>
                    <option value="#"> HOME +</option>
                        <option value="#home_layout_1.html">  - Home Layout 1</option>
                        <option value="#home_layout_2.html">  - Home Layout 2</option>
                        <option value="#home_layout_3.html">  - Home Layout 3</option>
                    <option value="#"> ABOUT US +</option>
                        <option value="#about_us.html">  - About us</option>
                        <option value="#philosophy.html">  - Philosophy</option>
                        <option value="#ethics.html">  - Ethics</option>
                        <option value="#careers.html">  - Careers</option>
                    <option value="#news.html"> NEWS</option>
                    <option value="#portfolio.html"> PORTFOLIO</option>
                    <option value="#"> OUR PROJECTS +</option>
                        <option value="#4_columns_projects.html">  - 4 Columns Projects</option>
                        <option value="#3_columns_projects.html">  - 3 Columns Projects</option>
                        <option value="#2_columns_projects.html">  - 2 Columns Projects</option>
                    <option value="#showreel.html"> SHOWREEL</option>    
                    <option value="#"> GALLERIES +</option>
                        <option value="#image_gallery.html">  - Image Gallery</option>
                        <option value="#mixed_gallery.html">  - Mixed Gallery</option>
                    <option value="#"> FEATURES +</option>
                        <option value="#full_width_text_and_image.html">  - Full Width Text + Image</option>
                        <option value="#full_width_text_and_video.html">  - Full Width Text + Video</option>
                        <option value="#fullscreen_video.html">  - Fullscreen Video</option>
                        <option value="#pricing_tables.html">  - Pricing Table</option>  
                    <option value="#contact.html"> CONTACT</option>
                </select>
            </div>
            <div id="audio-player" data-auto-play="false" data-player-opened="true" data-player-volume="0.6">
                <div id="audio-items">
                    <div id="audio-track" data-src="/assets/audio/on_top_of_the_world" data-title="Tim McMorris - Top Of The World"></div>
                    <div id="audio-track" data-src="/assets/audio/overwhelmed" data-title="Tim McMorris - Overwhelmed"></div>
                    <div id="audio-track" data-src="/assets/audio/playing" data-title="Tim McMorris - Playing"></div>
                    <div id="audio-track" data-src="/assets/audio/happy" data-title="Tim McMorris - Happy"></div>                    
                </div>
                <div id="audio-player-tooltip" class="opacity_0">
                    <div id="audio-tooltip-holder">
                        <div class="audio-tooltip-backg"></div>
                        <div class="audio-player-tooltip-title"><span></span></div>
                    </div>
                    <div class="audio-player-tooltip-hook"></div>                    
                </div>
                <div id="audio-player-holder">
                    <div id="audio-options-button">
                        <div class="audio-options-button-backg"></div>
                        <div class="audio-options-button-sign"></div>
                    </div>
                    <div id="audio-player-options-holder">                    
                        <div id="audio-prev-track">
                            <div class="audio-controls-backg"></div>
                            <div class="audio-prev-track-sign"></div>
                        </div>
                        <div id="audio-play-pause-track">
                            <div class="audio-controls-backg"></div>
                            <div class="audio-play-track-sign"></div>
                            <div class="audio-pause-track-sign"></div>
                        </div>
                        <div id="audio-next-track">
                            <div class="audio-controls-backg"></div>
                            <div class="audio-next-track-sign"></div>
                        </div>
                        <div id="audio-volume-speaker">
                            <div class="audio-controls-backg"></div>
                            <div class="audio-volume-on-sign"></div>
                            <div class="audio-volume-off-sign"></div>
                        </div>
                        <div id="audio-volume-bar">
                            <div class="audio-volume-bar-scrub-backg"></div>
                            <div class="audio-volume-bar-scrub-current"></div>
                        </div>                        
                    </div>                    
                </div>
            </div>
            <footer>    
                <div id="footer-social">            
                    <div id="footer-social-holder">
                        <ul>
                            <li class="twitter"><a href="http://www.twitter.com" target="_blank"></a></li>
                            <li class="facebook"><a href="http://www.facebook.com" target="_blank"></a></li>
                            <li class="google"><a href="http://www.google.com" target="_blank"></a></li>
                            <li class="dribbble"><a href="http://www.dribbble.com" target="_blank"></a></li>
                            <li class="flickr"><a href="http://www.flickr.com" target="_blank"></a></li>
                        </ul>
                    </div>                   
                </div>    
                <div id="footer-text"><!-- start #footer-text -->
                    <div id="footer-copyright"><a href="#">&copy;2014 All Star</a></div>           
                </div><!-- end #footer -->    
            </footer> 
        </div><!-- end .menu-content-holder-->
        
        <div id="menu-hider">
            <div id="menu-hider-background"></div>
            <div id="menu-hider-icon"></div>
        </div>
    </div><!-- end #menu-container -->