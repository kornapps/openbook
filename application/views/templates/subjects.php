<!doctype html>
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <!--[if ie]><meta http-equiv='X-UA-Compatible' content="IE=edge,IE=9,IE=8,chrome=1" /><![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    
    <link rel="icon" href="/assets/favicon.ico" type="image/x-icon" />
    <link rel="shortcut icon" href="/assets/favicon.ico" type="image/x-icon" />
    
	<title><?echo $title ?></title>
    
	<meta name="description" content="" />
	<meta name="author" content="MEDIACREED.COM" />
        
        <link rel="stylesheet" href="css/style.css" />
        <link href='http://fonts.googleapis.com/css?family=PT+Sans:400,700,400italic|PT+Sans+Caption:400,700' rel='stylesheet' type='text/css' />        
        
        <!-- SCRIPT IE FIXES -->  
        <!--[if lt IE 9]>
        <script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
        <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]--> 
        <!-- END SCRIPT IE FIXES-->
      
      
        <!-- START TEMPLATE JavaScript load -->
    	<script type="text/javascript" src="js/libs/jquery-1.7.2.min.js"></script>    
        <script type="text/javascript" src="js/libs/modernizr.custom.min.js"></script> 
        <script type="text/javascript" src="js/libs/jquery.wipetouch.js"></script>   
             
        <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=AIzaSyBttxrYoBq8srteD7evmDqnaK6V2Uun42o&amp;sensor=true"></script>
        
        <script type="text/javascript" src="js/libs/jquery.gmap.min.js"></script>
        <script type="text/javascript" src="js/greensock/minified/TweenMax.min.js"></script>    
        <script type="text/javascript" src="js/libs/jquery.timer.js"></script>
        <script type="text/javascript" src="js/libs/jqueryui/1.8/jquery-ui.min.js"></script>
        <script type="text/javascript" src="js/libs/jquery.mousewheel.min.js"></script>
        <script type="text/javascript" src="js/mediacreed/scrollbar/mc.custom.list.js"></script>
        <script type="text/javascript" src="js/mc.modules.animation.js"></script> 
        <link rel="stylesheet" href="js/video-js/video-js.min.css" media="screen" />
        <script type="text/javascript" src="js/video-js/video.min.js"></script>
        <!-- END TEMPLATE JavaScript load -->
        
        <!--<script src="http://vjs.zencdn.net/c/video.js"></script>    
        Careful when using the online version because the destroy method throws an error.    
        Our version has the fix on destroy method. Until it updates we recommend using the JS file from the template.    
        -->
        <script>
            _V_.options.flash.swf = "js/video-js/video-js.swf";
            _V_.options.techOrder = ["html5", "flash", "links"];
            var params = {};
            params.bgcolor = "#000000";
            params.allowFullScreen = "true";       
            _V_.options.flash.params = params;
    
        </script>    
</head>

<body>

<div class="main-template-loader"></div>

<div id="template-wrapper">
    
    <div id="module-container"><!-- start #module-container -->
        <div id="module-background-holder">   
            <!--<div id="module-background-solid1" class="opacity_0"></div> --> 
        	<img src="/assets/backg/bg07.jpg" onload="animateModuleBackground(this)" class="module-background" id="module-background" alt="" />
        </div>
        <div id="module-container-holder" class="module-position-cc"  data-id="module-position-cc">
            <div id="module-columns"  class="module-columns " > <!-- .shadow-side-all-->
                <div id="module-columns-wrapper">
                    <div id="module-columns-container">
                        <div class="columns-filter-holder">
                            <div id="filter-buttons-holder">
                                <div class="filter-button" data-filter="其他">其他</div>
                                <div class="filter-button" data-filter="術科">術科</div>
                                <div class="filter-button" data-filter="數學">數學</div>
                                <div class="filter-button" data-filter="英文">英文</div>
                                <div class="filter-button" data-filter="語文">語文</div> 
                                <div class="filter-button selected" data-filter="*">全部</div>
                            </div>
                        </div>  
                        <div class="columns-horizontal-separator"></div>         
                    	<div id="module-columns-holder">
                        <?php foreach ($subjects as $key => $value):?>
                        <div class="half-thumb-holder" data-id="<?php echo $value['Type'];?>">
                                <div class="half-thumb-image">
                                    <img src="/assets/img/s/<?php echo $value['webImg'];?>" class="opacity_0" onload="animateThumb(this)" alt=""  />
                                    <div id="thumb-image-hover" class="hover-default">
                                        <div class="background opacity_6"></div>
                                        <div class="zoom-gallery"></div>
                                    </div>
                                    
                                </div> 
                                <div class="thumb-tag">
                                    <h1><?php echo $value['NameCN'];?></h1>
                                    <h2><?php echo $value['Type'];?></h2>
                                </div>                  
                            </div>
                        <?php endforeach;?> 
                        </div>
                    </div><!-- end #module-columns-container -->
                </div>  
                
                <div id="module-columns-preview-wrapper">
                    <div id="module-columns-preview"><!-- start #module-columns-preview -->
                        <div id="module-columns-preview-holder">
                            <div id="columns-preview-controls">
                                  <div class="columns-preview-counter">
                                    <span>x/x</span>
                                  </div>  
                                  <div class="columns-preview-backward">
                                    <div class="columns-preview-backg"></div>
                                    <div class="columns-preview-backward-sign"></div>
                                  </div>
                                  <div class="columns-preview-close">
                                    <div class="columns-preview-backg"></div>
                                    <div class="columns-preview-close-sign"></div>
                                  </div>
                                  <div class="columns-preview-forward">
                                    <div class="columns-preview-backg"></div>
                                    <div class="columns-preview-forward-sign"></div>
                                  </div>  
                            </div>
                            <div class="columns-horizontal-separator"></div>
                            <div class="columns-preview-horizontal-fix">
								<div id="columns-slides-controls">
                                    <div class="col-prev-media-arrow-backward">
                                        <div class="col-prev-media-arrow-backg opacity_2"></div>
                                        <div class="col-prev-media-arrow-backward-sign"></div>
                                    </div>
                                    <div class="col-prev-media-arrow-forward">
                                        <div class="col-prev-media-arrow-backg opacity_2"></div>
                                        <div class="col-prev-media-arrow-forward-sign"></div>
                                    </div>                
                                </div>
                                <ul>
                                    <?php foreach ($subjectsInfo as $key => $value): ?>
                                    <li>
                                        <div class="columns-preview-media" data-src="/assets/img/s/<?php echo $value['PicPath'];?>"></div>
                                        <div class="columns-preview-description">
                                            <?foreach ($value['info'] as $fkey => $fvalue):?>
                                            <!-- <p><?php echo $fvalue['NameCN']?></p> -->
                                            <a href="/index.php/exam/view/<?php echo $fvalue['code'];?>" target="ExamWindows"><img src="/assets/img/t/sub/<?php echo $fvalue['webImg'];?>"></a>
                                            <?php endforeach;?>
                                        </div>
                                    </li>
                                    <?php endforeach;?>
                                </ul>    
                            </div>                    
                        </div>
                    </div> <!-- end #module-columns-preview -->
                </div>  
                    
                <!--<div id="galleries-background" class="opacity_8"></div>-->  
            </div>
            
        </div>
        <div id="module-scrollbar-holder_v2">
            <div id="module-scrollbar-background" class="opacity_8"></div>
            <div id="module-scrollbar-dragger"></div>
        </div>	   
    </div><!-- end #module-container -->
</div>

<!-- START LOADING CONTAINER AND ANIMATION-->
<div id="load-container"></div>
<div id="loading-animation">
	<img src="/assets/loaders/loader.gif" width="16" height="11" alt="Synergy - loading animation"/>
</div>
<!-- END LOADING CONTAINER AND ANIMATION-->  
    
</body>
</html>
