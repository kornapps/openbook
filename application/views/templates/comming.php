﻿<!doctype html>
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head></hrad>

<body>

<div class="main-template-loader"></div>

<div id="template-wrapper">
    
    <div id="module-container"><!-- start #module-container -->
        <div id="module-background-holder">
            <img src="/assets/backg/bg01.jpg" onload="animateModuleBackground(this)" class="module-background" id="module-background" alt="" />
        </div>  
        <div id="module-container-holder" class="module-position-cc opacity_9 "  data-id="module-position-cc">
            <div id="module-text-page"  class="module-philosophy shadow-side-all">
                <div id="module-wrapper">
                    <div id="module-text-page-holder-philosophy">               
                        <!-- <div class="custom-separator"></div> -->
                        <div class="media-holder">
                            <!-- <img src="/assets/img/share_main.jpg" width="100%" class="opacity_0" onload="animateThumb(this)"/> -->
                        </div>
                        <div class="custom-separator"></div>
                        <div class="main-text-holder">
                            <h1>Coming Soon.</h1>
                        </div>
                    </div>
                </div>                   
            </div>
            <div id="module-scrollbar-holder">
                <div id="module-scrollbar-background" class="opacity_4"></div>
                <div id="module-scrollbar-dragger"></div>
            </div>    
        </div>
            
    </div>
</div>

<!-- START LOADING CONTAINER AND ANIMATION-->
<div id="load-container"></div>
<div id="loading-animation">
	<img src="/assets/loaders/loader.gif" width="16" height="11" alt="Synergy - loading animation"/>
</div>
<!-- END LOADING CONTAINER AND ANIMATION--> 
    
</body>
</html>
