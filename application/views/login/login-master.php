<div class="loginbox"> 
  <div class="row  text-center">
    <!-- <img src="/assets/img/openbook_logo.png"> -->
    <?php $attributes = array('class'=>'form-signin' ,'role' => 'form','id'=>'memberLogin'); ?>
    <?php echo form_open('verifylogin',$attributes); ?>
    <!-- <form class="form-signin"> -->
    <!-- <h2 class="form-signin-heading">登入</h2> -->
    <input type="text" id="inputPhoneNumber" name="inputPhoneNumber" class="form-control" placeholder="輸入已登記手提電話號碼" required autofocus>
    <input type="password" id="inputPassword" name="inputPassword" class="form-control" placeholder="密碼" required>
<!--         <label class="checkbox">
    <input type="checkbox" value="remember-me"> Remember me
  </label> -->
  <!-- <a href="#">忘記密碼</a> -->
  <button class="btn btn-lg btn-primary btn-block" type="submit">會 員 登 入</button>
  <div><?php echo validation_errors(); ?></div>
</form>
<?php $attributes = array('class'=>'form-signin' ,'role' => 'form','id'=>'guestLogin'); ?>
  <?php echo form_open('guestlogin',$attributes); ?>
  <button class="btn btn-lg btn-primary btn-block" type="submit" >進 入</button>
  <input type="checkbox" id="tandc" name="tandc" value="1" checked="checked"> 按以上 [會員登入] 或 [進入] 鍵, 即代表你願意接受OpenBook之私隱權聲明及<a href="#" data-toggle="modal" data-target=".bs-modal-lg2">使用條款</a>細則所約束
</form>

<div class="remark">
    <div class="text-right">
          <button class="btn btn-primary" data-toggle="modal" data-target=".bs-modal-sm">免責聲明</button> | <button class="btn btn-primary" data-toggle="modal" data-target=".bs-modal-lg">私隠政策聲明</button> | <button class="btn btn-primary" data-toggle="modal" data-target=".bs-modal-sm2">版權</button>
    </div> 
</div>
  <!-- Large modal -->

<div class="modal fade bs-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
<p><u>Openbook</u><u>開學 </u><u>網頁私隱政策聲明</u></p>
<p>Openbook開學 致力支持並遵守認可的個人資料私隱保安及保密標準，以符合《個人資料（私穩）條例》的規定。<br />
  <br />
  Openbook開學 僅在處理使用者以填表方式提出的要求或查詢時，方會收集可識別本網頁瀏覽者身份的個人資料。資料的收集及用途將依據下列的<strong>《收集個人資料聲明》</strong>處理。<br />
  <br />
  <br />
  <strong><u>收集個人資料聲明</u></strong></p>
<ol>
  <li>本網頁所收集閣下的個人資料，只會用於處理閣下的要求或查詢。</li>
  <li> 使用者以電子方式傳遞的資料，如在傳送過程中被第三者竊取。使用者如因此引致損失，本網頁將不會負責。</li>
  <li> 本網頁所收集的個人資料將會絕對保密。Openbook開學 可將資料提供予有關部門作與行政及學術用途。除非獲得資料當事人的同意或在法律強制要求下，否則本網頁絕不會向外界機構或團體，披露當事人在作出要求或查詢時所提供的任何個人資料。</li>
  <li> 除非特別指明，使用者所有要求或查詢時所提供的資料，本網頁只會保留十二個月。</li>
  <li> 資料當事人有權查閱及更正本網頁持有其個人的資料，本網站或會在提供有關資料前收取費用。</li>
  <li> 在本網頁各部門均有不同個別信息收集政策，詳情請參閱相關的網站了解詳情。</li>
  <li> 如欲查閱或更改Openbook開學所持有閣下的個人資料或其他查詢，請電郵至<u><strong> info@openbook.la</strong></u><strong><u>。</u></strong></li>
</ol>
<p>注意：如以上中文譯本與英文文義如有歧異，一概以英文文本為準。</p>
<p>&nbsp;</p>
<p><u>Entrance Privacy Policy Statement for </u><u>Openbook Website</u></p>
<p>The website fully supports and observes recognised standards of protection in the security and confidentiality of personal data, as stipulated in the requirements of the Personal Data (Privacy) Ordinance. </p>
<p>We do not collect any personally identifiable information from visitors to our website except on the various request/enquiry forms where the collection and use of personal information is subject to our <strong>Personal Information Collection Statement</strong> below.</p>
<p><strong><u>Personal Information Collection Statement</u></strong></p>
<p>1. The collection of personal data is necessary in order to process and follow up on the request/enquiry you submitted via this form.</p>
<p>2. It is possible that any information submitted electronically could be observed by a third party in transit. We shall not be liable for any loss or damage to you as a result of this.</p>
<p>3. Information we collect about you is strictly confidential. Data may be provided to the appropriate departments/offices in the website for administrative and academic purposes. We will not disclose any personal information provided in your request/enquiry to any external bodies or organizations unless you have been consulted or we are required to do so by law.</p>
<p>4. Information provided in your request/enquiry will normally be kept for a period of 12 months unless otherwise specified.</p>
<p>5. You have the right to request access to and correction of information about you held by us. We may charge you a fee before you can access the information.</p>
<p>6. Individual departments/offices in the website may have different/supplementary personal information collection policies. Please refer to relevant websites for details.</p>
<p>7. If you wish to access or correct your personal data held by us, or other enquiry please submit your request to <strong><u>info@openbook.la</u></strong></p>
    </div>
  </div>
</div>

<div class="modal fade bs-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
<p><u>Disclaimer</u></p>
<p>Information on this web site is subjected to change and periodic review. Openbook makes no representations whatsoever about any other web sites which you may access through our web site. A link to a non-Openbook web site does not mean that Openbook accepts any responsibility included but not limited to financial losses for the content or the use of the web site. If you find a problem with this web site, please notify us at <strong><u>info@openbook.la</u>.</strong></p>
<p><u>免責聲明</u><u> </u></p>
<p>本網站有權定期更新和修正資料而不作另行個別通知。假如您可以透過我們的網站鏈接到其他非 Openbook 開學 之網站而造成任何包括並不限於財政、金錢之損失，本網站恕不負責。假如您發現本網站有任何意見，請透過<strong><u> info@openbook.la</u></strong>通知網站管理員。</p>
    </div>
  </div>
</div>

<div class="modal fade bs-modal-sm2" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
<p><u>Copy Right</u></p>
<p>All materials including but not limited to handouts and video, audio recording in any form provided by Openbook are for the personal use and are for the properties of Openbook. No Part of the same shall be reproduced, reprinted, resold, auctioned, put on rental subscriptions, stored in a retrieval system, or transmitted, in any form or by any means electronic, mechanical, photocopying, recording, or otherwise without the prior written permission of Openbook. All rights (including copyrights) of the same are reserved.</p>
<p><u>版權</u><u> </u></p>
<p>Openbook開學 以任何形式提供的講義、視像教材、錄音，祗供學員個在Openbook開學 之網站或應用程式中使用，屬Openbook開學 之財產；所有Openbook開學 提供的講義、視像教材、錄音之任何部分（包括但不限於文字、圖片、錄音、視像等），如事前未獲Openbook開學 的書面允許，不得以任何方式抄襲、翻印、轉載、轉售、拍賣、或租用等等。Openbook開學保留一切追究權利（包括版權）。</p>
    </div>
  </div>
</div>

<div class="modal fade bs-modal-lg2" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
<p><strong>OpenBook</strong> and its sub-domains (hereinafter collectively referred to as the &quot;Sites&quot;) are powered by <strong>OpenBook Limited</strong> (hereinafter referred to as the &quot;Company&quot;). By using the Sites and any other site accessed through the Sites, users acknowledge and agree that the Privacy Statement and the terms and conditions (hereinafter referred to as the &quot;T&amp;C&quot;) set out below are binding upon them. If users do not accept either or both of the Privacy Statement and the T&amp;C, please do not use the Sites. </p>
<p>The Company may revise the Privacy Statement and the T&amp;C at any time without prior notice. Once posted on the Sites, the amended Privacy Statement and the T&amp;C shall apply to all users. Users are advised to visit this page periodically to review the latest Privacy Statement and the T&amp;C. Users&rsquo; access to the Sites and the Services (as defined below) will be terminated upon his/her notice to the Company that any change is unacceptable; otherwise the continued use shall constitute the acceptance of all changes and they shall be binding upon the user.</p>
<ol>
  <li><strong><u>General Terms</u></strong><strong><u> </u></strong></li>
</ol>
<p>1.     The Company provides an online platform for individuals, companies and organizations to list the services or products related to education services which they offer and provides online marketing and forums to users of the Sites (the &quot;Services&quot;).<br />
  <br />
  2.     The Company is not a party of nor is it involved in any actual transaction involving any user of the Sites.<br />
  <br />
  3.     The Company is committed to protect the privacy of the users of the Sites. The Company uses the information of the users according to the terms as described in the Privacy Statement.<br />
  <br />
  <strong><u>2. Prohibited Uses for all users</u></strong><strong><u><br />
  </u></strong><br />
  Users of the Sites agree not to use any of the Sites for any of the following purposes which are expressly prohibited: -<br />
  <br />
  1.     All users are prohibited from violating or attempting to violate the security of the Sites including, but not limited to, accessing data not intended for them or logging into a server or account which they are not authorized to access, attempting to probe, scan or test the vulnerability of the system or network or attempting to breach security or authentication measures without proper authorization, attempting to interfere with service to any user, host or network or sending unsolicited e-mail. Violation of system or network security may result in civil or criminal liability.<br />
  <br />
  2.     All users shall not use the Sites (1) for transmitting, distributing or storing material in violation of any applicable law or regulation; or (2) in any manner that will infringe the copyright, trademark, trade secrets or other intellectual property rights of others or violate the privacy or publicity or other personal rights of others; or (3) in any manner that is libelous, obscene, threatening, abusive or hateful. In particular, all users shall not print, download, duplicate or otherwise copy any personally identifiable information about other uses (if any). All unsolicited communications of any type to users are strictly prohibited.<br />
  <br />
  3.     Users shall not use the Sites if they do not have legal capacity to form legally binding contracts.<br />
  <br />
  4.     Users shall not post any advertisement or material on the Sites which contains any false, inaccurate, misleading or libelous content or contains any computer virus. Also, the advertisement shall not be fraudulent or involve the sale of illegal products.<br />
  <br />
  5.     Users shall not engage in spamming, including but not limited to any form of emailing that is unsolicited.<br />
  <br />
  <br />
  <strong><u>3. Acceptable uses of the Sites</u></strong></p>
<p>a. Specific uses - Users posting advertisements or materials on the Sites (the &quot;Users&quot;)<br />
  Users shall, subject to the terms and conditions set out below and in the Privacy Statement, be entitled to post any advertisements or other materials on the Sites. <br />
  The User agrees that he/she/it shall only use the Sites for lawful purposes. The Company reserves the right to edit any advertisement / material as it sees appropriate. <br />
  In the event that the User is an individual, he/she shall not post his/her HK ID card or passport number on the advertisement or material listed on the Sites.<br />
  The User agrees and consents that the Company and/or its affiliates may use his/her personal data for purposes relating to the provision of services offered by the Company and marketing services and special events of the Company and its affiliates. <br />
  <br />
  b. Specific uses - Users viewing the advertisements (the &quot;Viewer&quot;)</p>
<p>Viewer agrees that any personal data received from the Sites or the Company shall only be used for the purpose of identifying and / or locating advertisements or materials or any content therein. Any personal data which are irrelevant to the above purposes shall be disregarded. Viewer also agrees that any personal data collected from the Sites or the Company shall be promptly and properly deleted when the above purposes have lapsed or been achieved.<br />
  <br />
  <br />
  <strong><u>4. Content License</u></strong></p>
<p>By posting content on the Sites, the User unconditionally grants the Company a non-exclusive, worldwide, irrevocable, royalty-free right to exercise the copyright, publicity and database rights (but no other rights) he/she/it has in the content in order that the Company can host, display, promote, copy, download, forward, distribute, reproduce, sell and re-use the content in any form, and carry out the purposes set out in the Privacy Statement and herein.</p>
<p><strong><u>5. Intellectual Property Rights</u></strong></p>
<p>The texts, images, layouts, databases, graphics, pictures, sounds, videos or audio formats, software, brands and all other materials on the Sites are the intellectual properties of the Company or the Users (as the case may be) which are protected by copyright and trademark laws and may not be downloaded or otherwise duplicated without the express written permission of the Company. Re-use of any of the foregoing is strictly prohibited and the Company reserves all rights (including copyright).</p>
<p><strong><u>6. Contents</u></strong></p>
<p>Users acknowledge that the Company does not pre-screen or approve any content posted on the Sites or any content sent through the Sites. In any event, the Company takes no responsibility whatsoever for the content on the Sites or any content sent through the Sites, or for any content lost and does not make any representations or warranties regarding the content or accuracy of any material on the Sites. <br />
  Any material or detail posted on the Sites by the User may be viewed by users of other web sites linked to the Sites and the Company is not responsible for any improper use by any user or third party from linked third party web sites of any data or materials posted on the Sites. All Users acknowledge and agree that they are solely responsible for the form, content and accuracy of any advertisement, web page or material contained therein placed by them. The Company is not responsible for the content of any third party web sites linked to the Sites and does not make any representations or warranties regarding the contents or accuracy of materials on such third-party web sites.<br />
  The Company shall have the right to remove any advertisement or materials posted on the Sites at its sole discretion without any compensation or recourse to the Users. In the event that the Company decides to remove any paid advertisement for any reasons not relating to any breach of the provisions herein, the Company may, after deducting the fees charged for the period that the advertisement has been posted on the Sites, refund the remaining fees (if any) to the related User.<br />
  <br />
  <br />
  <strong><u>7. Responsibility</u></strong></p>
<p>The Company may not monitor the Sites at all times but reserves the right to do so. The Company does not warrant that any advertisement or web page or material will be viewed by any specific number of users or that it will be viewed by any specific user. The Company shall not in any way be considered an agent of users with respect to any use of the Sites and shall not be responsible in any way for any direct or indirect damage or loss that may arise or result from the use of the Sites, for whatever reason made. <strong>WHILST ENDEAVOURING TO PROVIDE QUALITY SERVICE TO USERS, THE COMPANY DOES NOT WARRANT THAT THE SITES WILL OPERATE ERROR-FREE OR THAT THE SITES AND ITS SERVER ARE FREE OF VIRUSES OR OTHER HARMFUL MECHANISMS. IF USE OF THE SITES OR THEIR CONTENTS RESULT IN THE NEED FOR SERVICING OR REPLACING EQUIPMENT OR DATA BY ANY USER, THE COMPANY SHALL NOT BE RESPONSIBLE FOR THOSE COSTS.</strong> THE SITES AND THEIR CONTENTS ARE PROVIDED ON AN &quot;AS IS&quot; BASIS WITHOUT ANY WARRANTIES OF ANY KIND. TO THE FULLEST EXTENT PERMITTED BY LAW, THE COMPANY DISCLAIMS ALL WARRANTIES, INCLUDING WITHOUT PREJUDICE TO THE FOREGOING, ANY IN RESPECT OF MERCHANTABILITY, NON-INFRINGEMENT OF THIRD PARTY RIGHTS, FITNESS FOR PARTICULAR PURPOSE, OR ABOUT THE ACCURACY, RELIABILITY, COMPLETENESS OR TIMELINESS OF THE CONTENTS, SERVICES, TEXTs, GRAPHICS AND LINKS OF THE SITES.<br />
  <br />
  <br />
  <strong><u>8. Own Risk</u></strong></p>
<p>All USERS SHALL USE THE SITES AND ANY OTHER WEB SITES ACCESSED THROUGH IT, AT ENTIRELY THEIR OWN RISK. ALL Users are responsible for the consequences of their postings. The Company does not represent or guarantee the truthfulness, accuracy or reliability of any advertisement or material posted by other Users or endorse any opinions expressed by Users. Any reliance by users on material posted by the Users will be at their own risk. The Company reserves the right to expel any user and prevent their further access to the Sites, at any time for breaching this <strong>Agreement</strong> or violating the law and also reserves the right to remove any material which is abusive, illegal, disruptive or inappropriate in the Company&rsquo;s sole discretion.</p>
<p><strong><u>9. Links to Other Sites</u></strong></p>
<p>The Sites may contain links to third party web sites. These are provided solely as a convenience to users and not in any way as an endorsement by the Company of the contents on such third-party web sites. If any user accesses any linked third party web sites, he/she does so entirely at their own risk.</p>
<p><strong><u>10. Indemnity</u></strong></p>
<p>All users agree to indemnify, and hold harmless the Company, its officers, directors, employees and agents from and against any claims, actions, demands, losses or damages arising from or resulting from their use of the Sites or their breach of the terms of this Agreement. The Company will provide prompt notice of any such claim, suit or proceedings to the relevant user.</p>
<p><strong><u>11. Limitation of the Service</u></strong></p>
<p>The Company shall have the right to limit the use of the Services, including the period of time during which contents will be posted on the Sites, the size of the contents, email messages or any other contents which are transmitted by the Services. The Company reserves the right, in its sole discretion, to modify or remove any contents or materials posted on the Sites, for any reason, without giving any prior notice or reason to users. Users acknowledge that the Company shall not be liable to any party for any modification, suspension or discontinuance of the Service.</p>
<p><strong><u>12. Termination of Service</u></strong></p>
<p>The Company shall have the right to delete or deactivate any account, or block the email or IP address of any user, or terminate the access of users to the Services, and remove any content within the Services immediately without notice for any reason. The Company shall have no obligation to deliver any material or content posted on the Sites to any user at any time, both before or after cessation of the Services or upon removal of the related contents from the Sites.</p>
<p><strong><u>13. Disclaimer</u></strong></p>
<p>The Company does not have control over and does not guarantee the truth or accuracy of listings of any advertisement or material on the Sites. <br />
  IN ANY EVENT, THE COMPANY, ITS OFFICERS, DIRECTORS, EMPLOYEES OR AGENTS SHALL NOT BE LIABLE IN ANY EVENT FOR ANY LOSSES, CLAIMS OR DAMAGES SUFFERED BY ANY USER WHATSOEVER AND HOWSOEVER ARISING OR RESULTING FROM HIS/HER USE OR INABILITY TO USE THE SITES AND THEIR CONTENTS, INCLUDING NEGLIGENCE AND DISPUTES BETWEEN ANY PARTIES.</p>
<p><strong><u>14. Limitation of Liability</u></strong></p>
<p>Without prejudice to the above and subject to the applicable laws, the aggregate liability by the Company to any user for all claims arising from their use of the Services and the Sites shall be limited to the amount of HK$1,000.<br />
  <br />
<strong><u>15. Security Measures</u></strong></p>
<p>The Company will use its reasonable endeavours to ensure that its officers, directors, employees, agents and/or contractors will exercise their prudence and due diligence in handling the personal data submitted by the Users, and the access to and processing of the personal data by such persons is on a &quot;need-to-know&quot; and &quot;need-to-use&quot; basis. The Company will use its reasonable endeavours to protect the personal data against any unauthorized or accidental access, processing or erasure of the personal data.</p>
<p><strong><u>16. Governing Law and Jurisdiction</u></strong></p>
<p>The terms and conditions as set out herein and any dispute or matter arising from or incidental to the use of the Sites shall be governed by and construed in accordance with the laws of the Hong Kong Special Administrative Region of the People&rsquo;s Republic of China (&quot;Hong Kong&quot;). Both users and the Company shall submit to the exclusive jurisdiction of the courts of Hong Kong</p>
    </div>
  </div>
</div>