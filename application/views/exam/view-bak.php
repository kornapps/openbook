<div class="jumbotron">
    <div class="row">
      <div class="col-md-6 col-md-offset-3 text-center">
        <ul class="list-inline">
          <li>
          	<button type="button" class="btn btn-default btn-lg examBtn">
          		<a href="/index.php/quiz?type=N&qid=<? echo $question['Code'];?>&t=<? echo $question['TeacherCode'];?>&s=<? echo $question['SubjectCode'];?>">
          			<span class="glyphicon glyphicon-check fa-2x"><span>普通模式
          		</a>
          	</button>
          </li>
		<?php if(isset($exams)):?>
			<?php foreach ($exams as $key => $value): ?>
				<li><button type="button" class="btn btn-default btn-lg examBtn">
					<a href="/index.php/quiz?type=T&countDown=<?echo $value['CountDown'];?>&qid=<? echo $question['Code'];?>&t=<? echo $question['TeacherCode'];?>&s=<? echo $question['SubjectCode'];?>">
						<span class="glyphicon glyphicon-time fa-2x"><span>
							<?php echo $value['CountDown'];?>秒計時賽
					</a>
					</button>
				</li>
			<?php endforeach?>
		<?php endif ?>
        </ul>
      </div>
    </div>
  </div>
      <ul>
        <li>標準模式 (Standard Mode) 為Level 1至Level 5的基本學習挑戰模式，完成99題該學科的題目，將有評分、全港排名及題解可供參考。</li>
        <li>限時模式 (Time Attack Mode) 為Level 5*及Level 5**的進階限時挑戰模式，會有正式考試限時的壓迫感，20秒內完成10題該學科的題目，並以所需時間長短來決定能力及全港排名！</li>
        <li>教室模式 (Tutorial Mode) 在標準模式(Standard Mode)中，每完成一條題目，便可解一把鎖(unlock)，完成每一題MC題目，便可免費觀看名師作者該題的精闢視像解題！免費習得各家所長，成為HKDSE全方位達人！</li>
      </ul>
</div>

