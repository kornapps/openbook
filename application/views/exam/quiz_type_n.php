<!DOCTYPE html>
<html>
    <head>
        <meta content="text/html; charset=utf-8" http-equiv="content-type">

        <title></title>

        <link href="/application/libraries/css/reset.css" media="screen" rel="stylesheet" type="text/css">
        <!-- <link href="/application/libraries/css/bootstrap.min.css" rel="stylesheet" media="screen"> -->
        <!-- <link href="/application/libraries/font-awesome/css/font-awesome.min.css" rel="stylesheet"> -->
        <link href="/application/libraries/css/slickQuiz.css" media="screen" rel="stylesheet" type="text/css">
        <link href="/application/libraries/css/master.css" media="screen" rel="stylesheet" type="text/css">
    </head>

    <body id="slickQuiz">
        <h1 class="quizName"><!-- where the quiz name goes --></h1>

        <div class="quizArea">
            <div class="quizHeader">
                <!-- where the quiz main copy goes -->

                <a class="button startQuiz" href="#">開始！</a>
            </div>

            <!-- where the quiz gets built -->

        </div>

        <div class="quizResults">
            <h3 class="quizScore">分數: <span><!-- where the quiz score goes --></span><button name="submit" id="submit" class="button1">上傳成績</button><a class="button" href="#"> upload </a></h3>


            <h3 class="quizLevel"><strong>Ranking:</strong> <span><!-- where the quiz ranking level goes --></span></h3>

            <div class="quizResultsCopy">
                <!-- where the quiz result copy goes -->
            </div>
        </div>

        <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
        <script src="/application/libraries/js/slickQuiz_n.js"></script>
        <script>

            function countAns(){

                ansList =  $(".answers").find('input:checked');

                var myAns = new Array();
                // var preAns = new Array();
                // myAns['userid'] = <?php echo $userid;?>
                // myAns['ans'] = new Array();

                for (var i=0,len=ansList.length; i<len; i++){
                   myAns[i] = ansList[i].id;
                }

                // alert(qDatas);
                var idList = new Array();
                for (var k=0,len=qDatas.length; k<len; k++){
                    var qData = qDatas[k]
                    var qA = '';
                    for(var p=0,pl = qData.a.length; p<pl; p++){
                        if(qData.a[p].correct == true){
                            qA = ':'+p;
                        }
                    }
                    idList[k] = qData.id+qA;

                }

                for(var i=0,len = idList.length;i<len;i++){
                    if(myAns[i]){
                        idList[i] = idList[i]+':'+myAns[i];
                    }else{
                        idList[i] = idList[i]+':'+'NULL';
                    }
                }

                $.post("/index.php/quiz/countPoint", {
                    'userid' : <?php echo $userid;?>,
                    'postData': myAns,
                    'questions': idList
                });


            }


            $(function () {

            $('#submit').click(function(quiz){

                countAns();

                // var win = window.open('', '_self');
                // win.location.href = "#";
                // win.close();
            })


            $.getJSON("http://openbook.la/index.php/quiz/getJson",{
                lang:'CN',
                numberOfQuestions : 99,
                subject:<?php echo $examSetting['SubjectCode'];?>,
                teacher:<?php echo $examSetting['TeacherCode'];?>,
                qmode:'n',
                qfrom:<?php echo $examSetting['qFrom'];?>,
                qto:<?php echo $examSetting['qTo'];?>
            }).done(function(data){

                $('#slickQuiz').slickQuiz({json: data,
                nextQuestionText: '下一題 >',
                backButtonText: '< 上一題',
                finshQuizButtonText: '即時提交答案',
                // disableResponseMessaging: false,
                // perQuestionResponseMessaging: 'test...',
                // completionResponseMessaging: 'done.....'
                });

                });

            });
        </script>
    </body>
</html>
