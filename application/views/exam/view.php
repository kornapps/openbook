
<?php if($phoneNumber == 'guest'):?>
<div class="jumbotron exam">
    <div class="row text-center">
      <img src="/assets/img/web-hkdse-logo.png">
      <h4>請選擇挑戰類別</h4>
      <div class="col-md-4 text-center examMode">
        <a target="examWindow" href="/index.php/quiz?type=S&qid=<? echo $question['Code'];?>&t=<? echo $question['TeacherCode'];?>&s=<? echo $question['SubjectCode'];?>"><img src="/assets/img/web-exam_mode_01.png"></a>
        <!-- <a onclick ="javascript:showSubList();"  href="#"><img src="/assets/img/web-exam_mode_01.png"></a> -->
      </div>
    </div>
<!--       <div id="examSubList" class＝"col-md-12">
        <ul>
          <?php for($x=1;$x <=100;$x++):?>
          <?php if($x%10 == 0):?>
          <li> 
            <a target="examWindow" href="/index.php/quiz?type=S&qid=<? echo $question['Code'];?>&t=<? echo $question['TeacherCode'];?>&s=<? echo $question['SubjectCode'];?>&p=<?php echo $x/10?>">第 <?php echo $x/10?> 套題目</a>
          </li>
          <?php endif;?>
        <?php endfor;?>
      </ul>
    </div>-->
      <ul class="list-unstyled">
        <li><b>標準模式 (Standard Mode)</b> 為Level 1至Level 5的基本學習挑戰模式，完成99題該學科的題目，將有評分、全港排名及題解可供參考。</li>
      </ul>
  </div>
<?php else:?>
<div class="jumbotron exam">
    <div class="row text-center">
      <img src="/assets/img/web-hkdse-logo.png">
      <h4>請選擇挑戰類別</h4>
      <div class="col-md-4 text-center examMode">
        <a target="examWindow" href="/index.php/quiz?type=S&qid=<? echo $question['Code'];?>&t=<? echo $question['TeacherCode'];?>&s=<? echo $question['SubjectCode'];?>"><img src="/assets/img/web-exam_mode_01.png"></a>
        <!-- <a onclick ="javascript:showSubList();"  href="#"><img src="/assets/img/web-exam_mode_01.png"></a> -->
      </div>
      <div class="col-md-4  text-center examMode">
        <a target="examWindow" href="/index.php/quiz?type=T&qid=<? echo $question['Code'];?>&t=<? echo $question['TeacherCode'];?>&s=<? echo $question['SubjectCode'];?>"><img src="/assets/img/web-exam_mode_02.png"></a>
      </div>
      <div class="col-md-4 text-center examMode">
        <a target="examWindow" href="/index.php/quiz?type=L&qid=<? echo $question['Code'];?>&t=<? echo $question['TeacherCode'];?>&s=<? echo $question['SubjectCode'];?>"><img src="/assets/img/web-exam_mode_03.png"></a>
      </div>
    </div>
       <!-- <div id="examSubList" class＝"col-md-12">
        <ul>
          <?php for($x=1;$x <=100;$x++):?>
          <?php if($x%10 == 0):?>
          <li> 
            <a target="examWindow" href="/index.php/quiz?type=S&qid=<? echo $question['Code'];?>&t=<? echo $question['TeacherCode'];?>&s=<? echo $question['SubjectCode'];?>&p=<?php echo $x/10?>">第 <?php echo $x/10?> 套題目</a>
          </li>
          <?php endif;?>
        <?php endfor;?>
      </ul>
    </div> -->
      <ul class="list-unstyled">
        <li><b>標準模式 (Standard Mode)</b> 為Level 1至Level 5的基本學習挑戰模式，完成99題該學科的題目，將有評分、全港排名及題解可供參考。</li>
        <li><b>限時模式 (Time Attack Mode)</b> 為Level 5*及Level 5**的進階限時挑戰模式，會有正式考試限時的壓迫感，20秒內完成10題該學科的題目，並以所需時間長短來決定能力及全港排名！</li>
        <li><b>教室模式 (Tutorial Mode)</b> 在標準模式(Standard Mode)中，每完成一條題目，便可解一把鎖(unlock)，完成每一題MC題目，便可免費觀看名師作者該題的精闢視像解題！免費習得各家所長，成為HKDSE全方位達人！</li>
      </ul>
  </div>

<?php endif;?>