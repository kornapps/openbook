<!DOCTYPE html>
<html>
    <head>
        <meta content="text/html; charset=utf-8" http-equiv="content-type">

        <title>SlickQuiz Demo</title>

        <link href="/application/libraries/css/reset.css" media="screen" rel="stylesheet" type="text/css">
        <link href="/application/libraries/css/slickQuiz.css" media="screen" rel="stylesheet" type="text/css">
        <link href="/application/libraries/css/master.css" media="screen" rel="stylesheet" type="text/css">
    </head>

    <body id="slickQuiz">
        <h1 class="quizName"><!-- where the quiz name goes --></h1>
        
        <div class="quizArea">
            <div class="quizHeader"><span id="countdown">00:01:00</span>
                <!-- where the quiz main copy goes -->

                <a class="button startQuiz" href="#">Get Started!</a>
            </div>

            <!-- where the quiz gets built -->
        </div>

        <div class="quizResults">
            <h3 class="quizScore">You Scored: <span><!-- where the quiz score goes --></span> <button name="submit" id="submit">上傳成績</button></h3>

            <h3 class="quizLevel"><strong>Ranking:</strong> <span><!-- where the quiz ranking level goes --></span></h3>

            <div class="quizResultsCopy">
                <!-- where the quiz result copy goes -->
            </div>
        </div>

        <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
        <script src="/application/libraries/js/slickQuiz.js"></script>
        <script src="/assets/js/jquery.timer.js"></script>
        <script>






        $(function () {

            $.getJSON("http://openbook.la/index.php/quiz/getJson",{
                lang:'CN',
                numberOfQuestions : 99,
                subject:<?php echo $examSetting['SubjectCode'];?>,
                teacher:<?php echo $examSetting['TeacherCode'];?>
            }).done(function(data){

                $('#slickQuiz').slickQuiz({json: data,
                    nextQuestionText: '下一題 >',
                    backButtonText: '< 上一題',
                    finshQuizButtonText: '時間到，查看結果及提交答案',
                    randomSortQuestions: true
                });
                $("a.finishQuiz").hide();

            });
        });


            function countAns(){

                ansList =  $(".answers").find('input:checked');

                var myAns = new Array();
                // var preAns = new Array();
                // myAns['userid'] = <?php echo $userid;?>
                // myAns['ans'] = new Array();

                for (var i=0,len=ansList.length; i<len; i++){
                   myAns[i] = ansList[i].id;
                }

                // alert(qDatas);
                var idList = new Array();
                for (var k=0,len=qDatas.length; k<len; k++){
                    var qData = qDatas[k]
                    idList[k] = qData.id;
                    // alert(qData.id);
                }

                $.post("/index.php/quiz/countPoint", {
                    'userid' : <?php echo $userid;?>,
                    'postData': myAns,
                    'questions': idList
                });


            }




            $(function () {

            $('#submit').click(function(quiz){

                countAns();

                var win = window.open('', '_self');
                win.location.href = "#";
                win.close();
            })
            });



        var Example2 = new (function() {

            var $countdown;
            var $form;
            var incrementTime = 70;
            var currentTime = 60000; // 5 minutes (in milliseconds)
            
            $(function() {

                // Setup the timer
                $countdown = $('#countdown');
                Example2.Timer = $.timer(updateTimer, incrementTime, true);
                // $("a.finishQuiz").hide();
                // // Setup form
                // $form = $('#example2form');
                // $form.bind('submit', function() {
                //     Example2.resetCountdown();
                //     return false;
                // });

            });

            function updateTimer() {

                // Output timer position
                var timeString = formatTime(currentTime);
                $countdown.html(timeString);

                // If timer is complete, trigger alert
                if (currentTime == 0) {
                    Example2.Timer.stop();
                    alert('時間到!');
                    // Example2.resetCountdown();
                    $("a.finishQuiz").show();
                    $("a.nextQuestion").hide();
                    $("a.backToQuestion").hide();
                    return;
                }

                // Increment timer position
                currentTime -= incrementTime;
                if (currentTime < 0) currentTime = 0;

            }

            // this.resetCountdown = function() {

            //     // Get time from form
            //     var newTime = parseInt($form.find('input[type=text]').val()) * 1000;
            //     if (newTime > 0) {currentTime = newTime;}

            //     // Stop and reset timer
            //     Example2.Timer.stop().once();

            // };
            var count = 0,
                timer = $.timer(function() {
                    count++;
                    $('#counter').html(count);
                });
            timer.set({ time : 1000, autostart : false });


            // Common functions
            function pad(number, length) {
                var str = '' + number;
                while (str.length < length) {str = '0' + str;}
                return str;
            }
            function formatTime(time) {
                time = time / 10;
                var min = parseInt(time / 6000),
                    sec = parseInt(time / 100) - (min * 60),
                    hundredths = pad(time - (sec * 100) - (min * 6000), 2);
                return (min > 0 ? pad(min, 2) : "00") + ":" + pad(sec, 2) + ":" + hundredths;
            }


        });

        </script>
    </body>
</html>
