<div class="container">
<div class="jumbotron">
<div class="row">
<?php echo $question['DescriptionCN']?>
</div>
<ul>
	<li><?php echo $question['ChoiceACN']?></li>
	<li><?php echo $question['ChoiceBCN']?></li>
	<li><?php echo $question['ChoiceCCN']?></li>
	<li><?php echo $question['ChoiceDCN']?></li>
</ul>
<div class="flex-video">
<iframe class="youtube-player" type="text/html" width="640" height="330" src="http://www.youtube.com/embed/<?php echo substr($question['YouTubePath'],9); ?>" allowfullscreen frameborder="0">
</iframe>
</div>
<h4>討論區</h4>
<div class="commentsWindow">
<iframe  width="100%" height="400" src="/index.php/comment/iframeview/<?php echo $question['Code']; ?>" frameborder="0"></iframe>
</div>
	<div class="row">
        <?php $attributes = array('class' => 'form-inline', 'role' => 'form'); ?>
        <?php echo form_open('comment/create',$attributes); ?>
			<textarea name="comment" class="form-control" rows="3"></textarea>
			<input type="hidden" name="qid" value="<?php echo $question['Code']?>">
			<input type="hidden" name="mid" value="<?php echo $session['id'];?>">
			<button type="submit" class="btn btn-default">留言</button>
		</form>
	</div>
</div>
</div>
</div>