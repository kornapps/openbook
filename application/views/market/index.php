<div class="jumbotron market">
	<div class="row">
		<h2 class="text-center">全城響應</h2>
		<div>
		<p>
			<b>「全城響應」</b>是給予所有 OpenBook 用戶一個經由教育去交流心得、互動提升及換領禮品的全方位平台。
			只要免費登記成為 OpenBook 用戶，你便可以得到一個<b>星星(Star Dollar)</b>，擁有一顆星星，便可瀏覽「全城響應」、參與教育遊戲及觀賞免費教育頻道。(每一張 Memo 紙消費 5 粒星星，內容亦受管理員監管，敬請留意) 星星的多少視乎你有多少朋友成為 OpenBook 用戶，每周更有<b>星級換領! 全城響應!</b> 星星愈多，禮品愈豐! 逢星期日 11:59pm 截數! 本周星級換領是 :
		</p>
		</div>
		<div>
			<ul>
				<li>我的星星!?</li>
				<li>我要星星!!!</li>
				<li>我要加入...</li>
			</ul>
		</div>
		<?php foreach($markets as $item): ?>
		<div class="col-sm-4  col-xs-6 smallNote <?php echo $item['type'];?> text-center">
			
				<span><a href="#" data-toggle="modal" data-target="#myModal<?php echo $item['id'];?>"><?php echo $item['infoCN'];?>
					<br />REF :<?php echo  str_pad($item['id'],6,"0", STR_PAD_LEFT);?>
					<br /><?php echo date('d/m',strtotime($item['created_date']));?></a>
				</span>
			
		</div>
		<div class="modal fade" id="myModal<?php echo $item['id'];?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		  <div class="modal-dialog">
		    <div class="modal-content popupnote">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		        <h3 class="modal-title" id="myModalLabel"><?php echo $item['infoCN'];?></h3>
		      </div>
		      <div class="modal-body">
				<?php echo $item['content'];?>
		      </div>
		    </div><!-- /.modal-content -->
		  </div><!-- /.modal-dialog -->
		</div><!-- /.modal -->
	<?php endforeach ?>

</div>

</div>