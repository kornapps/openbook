    <!-- Login -->
    <div id="login" class="services">
    <div class="callout">
      <div class="vert-text">
          <form class="form-inline" role="form">
      <div class="form-group">
        <label for="inputPhoneNumber">電話號碼</label>
        <input type="number" class="form-control" id="inputPhoneNumber" placeholder="輸入已登記手提電話號碼">
      </div>
      <div class="form-group">
        <label for="inputPassword">密碼</label>
        <input type="password" class="form-control" id="inputPassword" placeholder="密碼">
      </div>
<!--       <div class="checkbox">
        <label>
          <input type="checkbox"> Remember me
        </label>
      </div>
 -->      <button type="submit" class="btn btn-default">登 入</button>
    </form>    
      </div>
    </div>
  </div>
   
    <!-- Footer -->
    <footer>
      <div class="container">
        <div class="row">
          <div class="col-md-6 col-md-offset-3 text-center">
            <ul class="list-inline">
              <li><i class="fa fa-facebook fa-3x"></i></li>
              <li><i class="fa fa-twitter fa-3x"></i></li>
              <li><i class="fa fa-dribbble fa-3x"></i></li>
            </ul>
            <div class="top-scroll">
              <a href="#top"><i class="fa fa-circle-arrow-up scroll fa-4x"></i></a>
            </div>
            <hr>
            <p>Copyright &copy; Allstar 2013</p>
          </div>
        </div>
      </div>
    </footer>
    <!-- /Footer -->

