<div id="question">
	<div class="container">
	<div class="row">
		<!--<?php print_r($_SERVER['HTTP_USER_AGENT']);?> -->
		<?php if($question['YouTubePath']  <> ''): ?>
			<?php if(strpos($_SERVER['HTTP_USER_AGENT'],'Android')): ?>
				<div><a href="https://m.youtube.com<?php echo $question['YouTubePath'];?>" target="_blank"><img src="http://img.youtube.com/vi/<?php echo substr($question['YouTubePath'],9,100);?>/0.jpg" width="100%"></a></div>
			<?php else: ?>
				<!-- <iframe  width="300" height="185" frameborder="0" type="text/html" src="https://www.youtube.com/embed/<?php echo substr($question['YouTubePath'],9,100);?>?feature=player_detailpage" allowfullscreen="false" class="youtube-player">&amp;lt;br /&amp;gt;</iframe>  -->
				<div><a href="https://m.youtube.com<?php echo $question['YouTubePath'];?>" target="_blank"><img src="http://img.youtube.com/vi/<?php echo substr($question['YouTubePath'],9,100);?>/0.jpg" width="100%"></a></div>
			<?php endif ?>
		<?php endif ?>
		<a href="/assets/img/banner/banner02.jpg"><img src="/assets/img/banner/banner02.jpg" width="100%"></a>
		</div>
	</div>
</div>

<div id="comment">
	<div class="container">
<div calss="row">	
<?php foreach ($comments as $item): ?>
	<div class="col-md-1 col-xs-2">
		<img src="/assets/img/default.icon.png" width="50">
	</div>
    <div id="main" class="col-md-11 col-xs-10">
        <blockquote><?php echo $item['comment'] ?></blockquote>
    </div>
    <!-- <p><a href="../comment/<?php echo $item['qid'] ?>">View article</a></p> -->
<?php endforeach ?>
</div>
</div>
<div class="container">
	<div class="row">
		<!-- <?php echo '<p>Member ID : '.$mid.'</p>';?> -->
        <?php $attributes = array('class' => 'form-inline', 'role' => 'form'); ?>
        <?php echo form_open('comment/appcreate',$attributes); ?>
			<textarea name="comment" class="form-control" rows="3"></textarea>
			<input type="hidden" name="qid" value="<?php echo $qid?>">
			<input type="hidden" name="mid" value="<?php echo $mid?>">
			<button type="submit" class="btn btn-default">留言</button>
		</form>
	</div>
	<hr />
	<div>
		條款及細則
		<ol>
<li>本電子優惠劵每次只可使用一張。</li>
<li>必須於繳交學費前出示此優惠券，列印收據後不能後補優惠。</li>
<li>必須繳付現金，方可享用優惠。</li>
<li>本優惠劵只適用於暑假常規課程，學額有限，額滿即止。</li>
<li>此優惠券只限新生使用。</li>
<li>此優惠券不可兌換現金或其他課堂，並不設找贖。</li>
<li>如有任何爭議，星河教育保留最終決定權。</li>
<li>星河教育保留修改所有優惠條款及細則與終止推廣之權利。</li>
<li>優惠有效期至2014年7月31日。</li>
		</ol>
	</div>
</div>
</div>
