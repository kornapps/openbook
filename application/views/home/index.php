<div class="jumbotron">
<div class="row userinfo">
<div class="col-lg-5">
<?php if(isset($memberContent['imagePath'])):?>
	<img src="<?php echo $memberContent['imagePath'];?>" width="160">
<?php else:?>
	<img src="/assets/img/m/default.icon.png" width="160">
<?php endif;?>
</div>
<div class="col-lg-5"><h3><?php echo $memberContent['displayName']; ?></h3>
<h4>LV <?php if(isset($memberContent['Level'])){ echo $memberContent['Level']; }else{echo '0';} ?></h4>
<p>能力值</p>
<p>稱號 : <?php if(isset($memberContent['Title'])){ echo $memberContent['Title']; }else{echo ' ';} ?></p>
</div>
</div>
<div class="row">
<div class="col-lg-5">
<p>修讀學科</p>
<p>中文,歴史,英文,科學,地理,數學,電腦</P>
<p>最高排名(標準模式)</p>
<p>未有資料</p>
<p>最高排名(計時模式)</p>
<p>未有資料</p>
</div>
<div class ="col-lg-5">
<canvas id="canvas" height="320"></canvas>
</div>
</div>

<script src="/assets/js/Chart.js"></script>
	<script>

		var radarChartData = {
			labels : ["中文","歴史","英文","科學","地理","數學","電腦"],
			datasets : [
				{
					fillColor : "rgba(255,32,75,1)",
					strokeColor : "rgba(220,220,220,1)",
					pointColor : "rgba(220,220,220,0)",
					pointStrokeColor : "#fff",
					data : [65,59,90,81,56,55,60]					
				},

			]
			
		}


	var myRadar = new Chart(document.getElementById("canvas").getContext("2d")).Radar(radarChartData,
		{
			scaleLineColor : "rgba(100,100,100,.5)",
			scaleShowLabels : false,
			pointLabelFontSize : 20,
			pointLabelFontColor : "#fff",
			pointLabelFontStyle : "normal"

		});
	
	</script>