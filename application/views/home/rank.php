<div class="jumbotron">
	<div class="row">

		<!-- Nav tabs -->
		<ul id="myTab" class=" nav nav-tabs">
		<?php foreach ($rank as $key => $value):?>
		  <li><a href="#<?php echo $subjects[$value['subjectName']]['nameEN']?>" data-toggle="tab"><?php echo $subjects[$value['subjectName']]['nameCN']?></a></li>		
		<?php endforeach ?>
		</ul>

		<!-- Tab panes -->
		<div class="tab-content">
		<?php foreach ($rank as $key => $value):?>
		  <div class="tab-pane fade" id="<?php echo $subjects[$value['subjectName']]['nameEN']?>">
		  <div class="col-md-6 frank">
		  	<h4>朋友排名</h4>
		  		<ul class="list-group">
		  		<?php foreach ($value['friendRank'] as $name => $data):?>
		  		<?php list($rank, $imgurl) = preg_split('/[\s:]+/', $data)?>
		  		<li class="list-group-item"><!--<img  width="50" src="<?php echo $imgurl?>">--><span class="badge"><?php echo $rank?></span><b><?php echo $name?></b>
		  		<br /><img src="/assets/img/rank_buttom_line.jpg">
		  		</li>
		  		<?php endforeach ?>
		  		</ul>
		  	</div>
		  	<div class="col-md-6 hkrank">
		  	<h4>全港排名</h4>
		  		<ul class="list-group">
		  		<?php foreach ($value['HKRank'] as $name => $data):?>
		  		<?php list($rank, $imgurl) = preg_split('/[\s:]+/', $data)?>
		  		<li class="list-group-item"><!--<img  width="50" src="<?php echo $imgurl?>">--><span class="badge"><?php echo $rank?></span><b><?php echo $name?></b>
		  		<br /><img src="/assets/img/rank_buttom_line.jpg">
		  		</li>
		  		<?php endforeach ?>
		  		</ul>
		  	</div>		  
		  </div>
		<?php endforeach ?>
		</div>
	</div>
</div>
