    
  <div id="gameselect">
    <table>
      <tr>
        <td><img src="/assets/img/gamespointer_01.png" class="gamepoint" id="g1-p"></td>
        <td><img src="/assets/img/gamespointer_02.png" class="gamepoint" id="g2-p"></td>
        <td><img src="/assets/img/gamespointer_03.png" class="gamepoint" id="g3-p"></td>
        <td><img src="/assets/img/gamespointer_04.png" class="gamepoint" id="g4-p"></td>
        <td><img src="/assets/img/gamespointer_05.png" class="gamepoint" id="g5-p"></td>
      </tr>
      <tr>
        <td><img src="/assets/img/g-1.png" id="g1" class="gselect"></td>
        <td><img src="/assets/img/g-3.png" id="g2" class="gselect"></td>
        <td><img src="/assets/img/g-4.png" id="g3" class="gselect"></td>
        <td><img src="/assets/img/g-5.png" id="g4" class="gselect"></td>
        <td><a href="/index.php/home2/"><img src="/assets/img/g-2.png" id="g5" class="gselect"></a></td>
      </tr>
    </table>
  </div>

<div class="remark">
    <div class="text-right">
          <button class="btn btn-primary" data-toggle="modal" data-target=".bs-modal-sm">免責聲明</button> | <button class="btn btn-primary" data-toggle="modal" data-target=".bs-modal-lg">私隠政策聲明</button> | <button class="btn btn-primary" data-toggle="modal" data-target=".bs-modal-sm2">版權</button>
    </div> 
</div>
  <!-- Large modal -->

<div class="modal fade bs-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
<p><u>Openbook</u><u>開學 </u><u>網頁私隱政策聲明</u></p>
<p>Openbook開學 致力支持並遵守認可的個人資料私隱保安及保密標準，以符合《個人資料（私穩）條例》的規定。<br />
  <br />
  Openbook開學 僅在處理使用者以填表方式提出的要求或查詢時，方會收集可識別本網頁瀏覽者身份的個人資料。資料的收集及用途將依據下列的<strong>《收集個人資料聲明》</strong>處理。<br />
  <br />
  <br />
  <strong><u>收集個人資料聲明</u></strong></p>
<ol>
  <li>本網頁所收集閣下的個人資料，只會用於處理閣下的要求或查詢。</li>
  <li> 使用者以電子方式傳遞的資料，如在傳送過程中被第三者竊取。使用者如因此引致損失，本網頁將不會負責。</li>
  <li> 本網頁所收集的個人資料將會絕對保密。Openbook開學 可將資料提供予有關部門作與行政及學術用途。除非獲得資料當事人的同意或在法律強制要求下，否則本網頁絕不會向外界機構或團體，披露當事人在作出要求或查詢時所提供的任何個人資料。</li>
  <li> 除非特別指明，使用者所有要求或查詢時所提供的資料，本網頁只會保留十二個月。</li>
  <li> 資料當事人有權查閱及更正本網頁持有其個人的資料，本網站或會在提供有關資料前收取費用。</li>
  <li> 在本網頁各部門均有不同個別信息收集政策，詳情請參閱相關的網站了解詳情。</li>
  <li> 如欲查閱或更改Openbook開學所持有閣下的個人資料或其他查詢，請電郵至<u><strong> info@openbook.la</strong></u><strong><u>。</u></strong></li>
</ol>
<p>注意：如以上中文譯本與英文文義如有歧異，一概以英文文本為準。</p>
<p>&nbsp;</p>
<p><u>Entrance Privacy Policy Statement for </u><u>Openbook Website</u></p>
<p>The website fully supports and observes recognised standards of protection in the security and confidentiality of personal data, as stipulated in the requirements of the Personal Data (Privacy) Ordinance. </p>
<p>We do not collect any personally identifiable information from visitors to our website except on the various request/enquiry forms where the collection and use of personal information is subject to our <strong>Personal Information Collection Statement</strong> below.</p>
<p><strong><u>Personal Information Collection Statement</u></strong></p>
<p>1. The collection of personal data is necessary in order to process and follow up on the request/enquiry you submitted via this form.</p>
<p>2. It is possible that any information submitted electronically could be observed by a third party in transit. We shall not be liable for any loss or damage to you as a result of this.</p>
<p>3. Information we collect about you is strictly confidential. Data may be provided to the appropriate departments/offices in the website for administrative and academic purposes. We will not disclose any personal information provided in your request/enquiry to any external bodies or organizations unless you have been consulted or we are required to do so by law.</p>
<p>4. Information provided in your request/enquiry will normally be kept for a period of 12 months unless otherwise specified.</p>
<p>5. You have the right to request access to and correction of information about you held by us. We may charge you a fee before you can access the information.</p>
<p>6. Individual departments/offices in the website may have different/supplementary personal information collection policies. Please refer to relevant websites for details.</p>
<p>7. If you wish to access or correct your personal data held by us, or other enquiry please submit your request to <strong><u>info@openbook.la</u></strong></p>
    </div>
  </div>
</div>

<div class="modal fade bs-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
<p><u>Disclaimer</u></p>
<p>Information on this web site is subjected to change and periodic review. Openbook makes no representations whatsoever about any other web sites which you may access through our web site. A link to a non-Openbook web site does not mean that Openbook accepts any responsibility included but not limited to financial losses for the content or the use of the web site. If you find a problem with this web site, please notify us at <strong><u>info@openbook.la</u>.</strong></p>
<p><u>免責聲明</u><u> </u></p>
<p>本網站有權定期更新和修正資料而不作另行個別通知。假如您可以透過我們的網站鏈接到其他非 Openbook 開學 之網站而造成任何包括並不限於財政、金錢之損失，本網站恕不負責。假如您發現本網站有任何意見，請透過<strong><u> info@openbook.la</u></strong>通知網站管理員。</p>
    </div>
  </div>
</div>

<div class="modal fade bs-modal-sm2" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
<p><u>Copy Right</u></p>
<p>All materials including but not limited to handouts and video, audio recording in any form provided by Openbook are for the personal use and are for the properties of Openbook. No Part of the same shall be reproduced, reprinted, resold, auctioned, put on rental subscriptions, stored in a retrieval system, or transmitted, in any form or by any means electronic, mechanical, photocopying, recording, or otherwise without the prior written permission of Openbook. All rights (including copyrights) of the same are reserved.</p>
<p><u>版權</u><u> </u></p>
<p>Openbook開學 以任何形式提供的講義、視像教材、錄音，祗供學員個在Openbook開學 之網站或應用程式中使用，屬Openbook開學 之財產；所有Openbook開學 提供的講義、視像教材、錄音之任何部分（包括但不限於文字、圖片、錄音、視像等），如事前未獲Openbook開學 的書面允許，不得以任何方式抄襲、翻印、轉載、轉售、拍賣、或租用等等。Openbook開學保留一切追究權利（包括版權）。</p>
    </div>
  </div>
</div>