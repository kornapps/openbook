<div class="jumbotron">
	<div class="row userinfo">
		<div class="col-lg-5">
			<?php if(isset($memberContent['imagePath'])):?>
			<img src="<?php echo $memberContent['imagePath'];?>" width="160">
		<?php else:?>
		<img src="/assets/img/m/default.icon.png" width="160">
	<?php endif;?>
</div>
<div class="col-lg-5"><h3><?php echo $memberContent['displayName']; ?></h3>
	<h4>LV <?php if(isset($memberContent['Level'])){ echo $memberContent['Level']; }else{echo '0';} ?></h4>
	<p>能力值</p>
	<p>稱號 : <?php if(isset($memberContent['Title'])){ echo $memberContent['Title']; }else{echo ' ';} ?></p>
</div>
</div>
<div class="row">
	<div >
		<!-- <form role="form"> -->
		<?php $attributes = array('role' => 'form'); ?>
        <?php echo form_open('home/info',$attributes); ?>
			<div class="form-group">
				<label for="inputName">真實姓名</label>
				<input type="text" class="form-control" id="inputName" name="inputName" placeholder="Enter Name" <?php if(isset($memberContent['Name'])){ echo 'value="'.$memberContent['Name'].'"';}?> >
			</div>
			<div class="form-group">
				<label for="inputSchool">學校名稱</label>
				<input type="text" class="form-control" id="inputSchool" name="inputSchool" placeholder="Enter School Name" <?php if(isset($memberContent['School'])){ echo 'value="'.$memberContent['School'].'"';}?> >
			</div>
			<div class="form-group">
				<label for="inputEmail">電郵</label>
				<input type="email" class="form-control" id="inputEmail" name="inputEmail" placeholder="Enter email" <?php if(isset($memberContent['Email'])){ echo 'value="'.$memberContent['Email'].'"';}?> >
				<input type="hidden" id="inputID" name="inputID" value="<?php echo $userid;?>">
			</div>
<!-- 			<div class="form-group">
				<label for="inputPassword">修改登入密碼</label>
				<input type="password" class="form-control" id="inputPassword" placeholder="Password">
			</div> -->
			<button type="submit" class="btn btn-default">Submit</button>
		</form>

	</div>
</div>