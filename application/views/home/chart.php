  <div class="container">
    <div class="row">
      <div class="col-md-6 col-md-offset-3 text-center">
		<canvas id="canvas" height="320"></canvas>
      </div>
    </div>
<!-- <div id="createPNGButton">
    <button onclick="putImage()">Save as Image</button>        
</div> -->
  </div>
<!-- <div><button onclick="to_image()">Draw to Image</button></div> -->
        <!-- <image id="theimage"></image> -->

<script src="http://www.nihilogic.dk/labs/canvas2image/canvas2image.js"></script>
<script src="/assets/js/Chart.js"></script>
	<script>

		var radarChartData = {
			labels : [<?php if($mypoint){ echo $mypoint['NameCN'];}else{echo 'NIL';}?>],
			datasets : [
				{
					fillColor : "rgba(255,32,75,1)",
					strokeColor : "rgba(220,220,220,1)",
					pointColor : "rgba(220,220,220,0)",
					pointStrokeColor : "#fff",
					data : [<?php if($mypoint){echo $mypoint['POINT'];}else{echo '0';}?>],					
				},


			]
			
		}

// var Radar2.defaults = {
				
// 	//Boolean - If we show the scale above the chart data			
// 	scaleOverlay : false,
	
// 	//Boolean - If we want to override with a hard coded scale
// 	scaleOverride : false,
	
// 	//** Required if scaleOverride is true **
// 	//Number - The number of steps in a hard coded scale
// 	scaleSteps : null,
// 	//Number - The value jump in the hard coded scale
// 	scaleStepWidth : null,
// 	//Number - The centre starting value
// 	scaleStartValue : null,
	
// 	//Boolean - Whether to show lines for each scale point
// 	scaleShowLine : true,

// 	//String - Colour of the scale line	
// 	scaleLineColor : "rgba(100,100,100,.5)",
	
// 	//Number - Pixel width of the scale line	
// 	scaleLineWidth : 1,

// 	//Boolean - Whether to show labels on the scale	
// 	scaleShowLabels : false,
	
// 	//Interpolated JS string - can access value
// 	scaleLabel : "<%=value%>",
	
// 	//String - Scale label font declaration for the scale label
// 	scaleFontFamily : "'Arial'",
	
// 	//Number - Scale label font size in pixels	
// 	scaleFontSize : 12,
	
// 	//String - Scale label font weight style	
// 	scaleFontStyle : "normal",
	
// 	//String - Scale label font colour	
// 	scaleFontColor : "#666",
	
// 	//Boolean - Show a backdrop to the scale label
// 	scaleShowLabelBackdrop : true,
	
// 	//String - The colour of the label backdrop	
// 	scaleBackdropColor : "rgba(255,255,255,0.75)",
	
// 	//Number - The backdrop padding above & below the label in pixels
// 	scaleBackdropPaddingY : 2,
	
// 	//Number - The backdrop padding to the side of the label in pixels	
// 	scaleBackdropPaddingX : 2,
	
// 	//Boolean - Whether we show the angle lines out of the radar
// 	angleShowLineOut : true,
	
// 	//String - Colour of the angle line
// 	angleLineColor : "rgba(0,0,0,.1)",
	
// 	//Number - Pixel width of the angle line
// 	angleLineWidth : 1,			
	
// 	//String - Point label font declaration
// 	pointLabelFontFamily : "'Arial'",
	
// 	//String - Point label font weight
// 	pointLabelFontStyle : "normal",
	
// 	//Number - Point label font size in pixels	
// 	pointLabelFontSize : 12,
	
// 	//String - Point label font colour	
// 	pointLabelFontColor : "#666",
	
// 	//Boolean - Whether to show a dot for each point
// 	pointDot : true,
	
// 	//Number - Radius of each point dot in pixels
// 	pointDotRadius : 3,
	
// 	//Number - Pixel width of point dot stroke
// 	pointDotStrokeWidth : 1,
	
// 	//Boolean - Whether to show a stroke for datasets
// 	datasetStroke : true,
	
// 	//Number - Pixel width of dataset stroke
// 	datasetStrokeWidth : 2,
	
// 	//Boolean - Whether to fill the dataset with a colour
// 	datasetFill : true,
	
// 	//Boolean - Whether to animate the chart
// 	animation : true,

// 	//Number - Number of animation steps
// 	animationSteps : 60,
	
// 	//String - Animation easing effect
// 	animationEasing : "easeOutQuart",

// 	//Function - Fires when the animation is complete
// 	onAnimationComplete : null
	
// }

	var myRadar = new Chart(document.getElementById("canvas").getContext("2d")).Radar(radarChartData,
		{
			scaleLineColor : "rgba(100,100,100,.5)",
			scaleShowLabels : false,
			pointLabelFontSize : 14,
			pointLabelFontColor : "#fff",
			pointLabelFontStyle : "normal",
			scaleOverride:true,
			scaleSteps:5,
			scaleStepWidth:20,
			scaleStartValue : 1

		});

	// function putImage()
	// {
	//   var canvas1 = document.getElementById("canvas");        
	//   if (canvas1.getContext) {
	//      var ctx = canvas1.getContext("2d");                
	//      var myImage = canvas1.toDataURL("image/png");      
	//   }
	//   var imageElement = document.getElementById("MyPix");  
	//   imageElement.src = myImage;                           

	// }  

	function to_image(){
                var canvas = document.getElementById("canvas");
                document.getElementById("theimage").src = canvas.toDataURL();
                Canvas2Image.saveAsPNG(canvas);
            }

	</script>

