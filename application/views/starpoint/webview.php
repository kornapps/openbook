	<div class="">
	<div class="row">
		<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
		  <!-- Indicators -->
		  <ol class="carousel-indicators">
		    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
		    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
		  </ol>

		  <!-- Wrapper for slides -->
		  <div class="carousel-inner">
		  	<?php $active = 1;?>
    	    <?php foreach ($starpoints as $key => $value):?>
            <?php if ($value['level'] == 1): ?>
		    <div class="item <?php if($active == 1) { echo 'active';} ?>">
		      <img src="/assets/img/starpoint/main-1.jpg" alt="...">
<!-- 		      <div class="carousel-caption">
		      	MEGGIE BEAUTY1
		      </div>
 -->		    </div>
            <?php  $active++; endif;?>
            <?php endforeach;?>
		  </div>
		  <!-- Controls -->
		  <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
		    <span class="glyphicon glyphicon-chevron-left"></span>
		  </a>
		  <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
		    <span class="glyphicon glyphicon-chevron-right"></span>
		  </a>
		</div>

	<div class="level2">
    <?php foreach ($starpoints as $key => $value):?>
    <?php if ($value['level'] == 2): ?>
    <div class="col-xs-12 col-md-6">
	    <a href="#" class="thumbnail">
	      <img src="/assets/img/starpoint/<?php echo $value['Path']?>" alt="...">
	    </a>
	</div>
    <?php endif;?>
    <?php endforeach;?>
	</div>

	<div class="level3">
    <?php foreach ($starpoints as $key => $value):?>
    <?php if ($value['level'] == 3): ?>
    <div class="col-xs-12 col-md-4">
	    <a href="#" class="thumbnail">
	      <img src="/assets/img/starpoint/<?php echo $value['Path']?>" alt="...">
	    </a>
	</div>
    <?php endif;?>
    <?php endforeach;?> 
	</div>

	<div class="level4">
    <?php foreach ($starpoints as $key => $value):?>
    <?php if ($value['level'] == 4): ?>
    <div class="col-xs-12 col-md-4">
	    <a href="#" class="thumbnail">
	      <img src="/assets/img/starpoint/<?php echo $value['Path']?>" alt="...">
	    </a>
	</div>
    <?php endif;?>
    <?php endforeach;?> 
	</div>
	</div>
	<div>
<!-- 	<?php foreach($starpoints as $item): ?>
		<img src="/assets/img/market/<?php echo $item['Path'] ?>" class="img-responsive">
		<h5><?php echo $item['infoCN']; ?></h5>
		<h6><?php echo $item['StartDate']; ?> 至 <?php echo $item['EndDate']; ?></h6>
		<hr>
	<?php endforeach ?> -->
	</div>