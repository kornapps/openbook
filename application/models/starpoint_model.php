<?php
class starpoint_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}

	public function get_starpoints($id = FALSE)
	{
		$this->db->order_by('level');

		if ($id === FALSE)
		{
			$query = $this->db->get('StarPointContent');
			return $query->result_array();
		}
		
		$query = $this->db->get_where('StarPointContent', array('id' => $id));
		return $query->result_array();
	}

}