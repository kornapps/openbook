<?php
class Comments_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}

	public function get_comments($qid = FALSE)
	{
		if ($qid === FALSE)
		{
			$query = $this->db->get('Comments');
			return $query->result_array();
		}
		
		$query = $this->db->get_where('Comments', array('qid' => $qid));
		return $query->result_array();
	}

	public function set_comments()
	{
		$this->load->helper('url');
			
		$data = array(
			'qid' => $this->input->post('qid'),
			'mid' => $this->input->post('mid'),
			'comment' => $this->input->post('comment'),
			'createDate'=> date("Y-m-d H:i:s")
		);
		
		return $this->db->insert('Comments', $data);
	}

}