<?php
class subjects_model extends CI_Model {

  public function __construct()
  {
    $this->load->database();
  }

  public function get_subjects($id = FALSE)
  {
    if ($id === FALSE)
    {
      $query = $this->db->get('mSubjects');
      return $query->result_array();
    }
        
    $query = $this->db->get_where('mSubjects', array('id' => $id));
    return $query->result_array();
  }

  public function get_subjectsArray(){
      $this->db->select('id,nameCN,nameEN');
      $query = $this->db->get('mSubjects');
      // return $query->result_array();

      $rs = array();

      if ($query->num_rows() > 0)
      {
         foreach ($query->result() as $row)
         {
            $rs[$row->id] = array('nameCN'=>$row->nameCN,'nameEN'=>$row->nameEN);
         }
      } 


      return $rs;
  }

  public function get_subjectsWithTeacherinfo(){

    // $query = $this->db->get('mTeachers');
    $query = $this->db->get_where('mTeachers', array('status' => 'A'));
    $teachers = $query->result_array();

    $query = $this->db->query("select * from `mTeachers` a
left join (select `code` ,`SubjectCode` ,`TeacherCode` from `mQuestions` group by `TeacherCode` order by cast(`SubjectCode`  as unsigned)) b
on a.`id` = b.`TeacherCode`
where a.`status` = 'A';");

    return $query->result_array();

  }


}