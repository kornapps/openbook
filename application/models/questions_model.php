<?php
class Questions_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}

	public function get_questions($qid = FALSE)
	{
		if ($qid === FALSE)
		{
			$query = $this->db->get('mQuestions');
			return $query->result_array();
		}
		
		$query = $this->db->get_where('mQuestions', array('Code' => $qid));
		if($query->num_rows  == 1){
			return $query->row_array();	
		}else{
			return FALSE;
		}
		
	}


	public function get_questionsFroExam($lang ='CN',$count=5,$SubjectCode=FALSE,$TeacherCode = FALSE,$GroupID = FALSE,$qFrom = null,$qTo=null){

		// if($GroupID){
		// 	$whereArray = array('SubjectCode'=>$SubjectCode,'TeacherCode'=>'$TeacherCode','GroupID' => $GroupID);
		// }else{
		// 	$whereArray = array('SubjectCode'=>$SubjectCode);
		// }

		//$this->db->select('Code,seqNo,Description'.$lang.' as Description,PicPath,YouTubePath,CorrectAnswer,ChoiceA'.$lang.' as ChoiceA ,ChoiceB'.$lang.' as ChoiceB,ChoiceC'.$lang.' as ChoiceC,ChoiceD'.$lang.' as ChoiceD ,ChoiceE'.$lang.' as ChoiceE',FALSE);
		$this->db->select('`Code`,seqNo,Description'.$lang.' as Description,PicPath,YouTubePath,CorrectAnswer,CONCAT(ChoiceA'.$lang.',ChoiceA'.$lang.'Path) as ChoiceA ,CONCAT(ChoiceB'.$lang.',ChoiceB'.$lang.'Path) as ChoiceB,CONCAT(ChoiceC'.$lang.',ChoiceC'.$lang.'Path) as ChoiceC,CONCAT(ChoiceD'.$lang.',ChoiceD'.$lang.'Path) as ChoiceD ,CONCAT(ChoiceE'.$lang.',ChoiceE'.$lang.'Path) as ChoiceE',FALSE);

		if($SubjectCode){$this->db->where('SubjectCode',$SubjectCode);}
		if($TeacherCode){$this->db->where('TeacherCode',$TeacherCode);}
		if($GroupID){$this->db->where('GroupID',$GroupID);}

		$this->db->order_by('seqNo');

		// $this->db->limit($count);

		// $query = $this->db->get_where('mQuestions',$whereArray);
		// echo $qFrom;

		// if($qFrom <> null){
			// $query = $this->db->get('mQuestions',$qTo,$qFrom);
		// }else{
			$query = $this->db->get('mQuestions',$count);
		// }

		// echo $this->db->last_query();

		// print_r($query->result_array());

		return $query->result_array();
	}

	function getSubjectandTeacherList($token = null, $subject = null,$teacher = null){

		$sql = "SELECT Code, subjectCode, TeacherCode, b.`NameCN` as 'subNameCN', b.`NameEN` as 'subNameEN',b.`PicPath` as 'subPicPath',c.`NameCN` as 'tecNameCN', c.`NameEN` as 'tecNameEN',c.`PicPath` as 'tecPicPath' from `mQuestions` a ";
		$sql = $sql . " left join mSubjects b on a.subjectCode = b.id left join mTeachers c on a.TeacherCode = c.id ";
		if($subject  && $teacher == null ){

			$sql = $sql . " where SubjectCode = $subject ";
		}
		if($teacher && !$subject){
			$sql =  $sql . " where TeacherCode = $teacher ";
		}
		if($teacher && $subject){
			$sql =  $sql . " where TeacherCode = $teacher  and SubjectCode = $subject";
		}		
		$sql =  $sql . " group by a.subjectCode,a.teacherCode order by b.id ";

		// echo $sql;

		$query =$this->db->query($sql);

		return $query->result_array();
	}

	function getSubjectWithExamList(){
		$sql = 'SELECT distinct a.subjectCode, id, NameCN as "subNameCN" ,NameEN as "subNameEN" ,b.PicPath as "subPicPath" from mQuestions a left join mSubjects b on a.subjectCode = b.id order by b.id';
		$query = $this->db->query($sql);
		// print_r($query->result_array());
		return $query->result_array();

	}

	function getTeacherWithExamList(){
		$sql = 'SELECT distinct a.TeacherCode, id, b.NameCN as "tecNameCN" ,b.NameEN as "tecNameEN",b.PicPath as "tecPicPath" from mQuestions a left join mTeachers b on a.TeacherCode = b.id';
		$query = $this->db->query($sql);
		return $query->result_array();

	}

	function numberToChar($number){

			$char = '';

			switch ($number) {
				case '0':
					$char = 'A';
					break;
				case '1':
					$char = 'B';
					break;
				case '2':
					$char = 'C';
					break;
				case '3':
					$char = 'D';
					break;
				case '4':
					$char = 'E';
					break;
			}

			return $char;
	}


	function createLog($memberID = null,$gameStart = null,$gameEnd = null){

		if($memberID){

			if(!$gameStart) $gameStart =  date('Y-m-d H:i:s');
			if(!$gameEnd) $gameEnd =  date('Y-m-d H:i:s',time()+ 999);

			$data = array(
				'ExamCode'=>1,
				'MemberCode'=>$memberID,
				'StartTime'=>$gameStart,
				'EndTime'=>$gameEnd
				);

			$this->db->insert('tExamH', $data);
			$LogNo = $this->db->insert_id();

			if($LogNo){
				return $LogNo;
			}else{
				die("Database Error at insert tExamH");
			}

		}else{

			die('Not have memberID');
		}

	}


	public function countThePoint($memberID,$ans = FALSE){

		$count = 0;

		$insertData = array();

		//get log no

		$LogNo = $this->createLog($memberID);


		foreach ($ans as $key => $value) {
			list($qid, $char, $qans) = explode(":", $value);
			//計分
			if($char == substr($qans, -1)){$count++; $Correct = 'Y';}else{$Correct ='N';}

			$insertData[] = array('ExamCode'=>1,
								'memberCode'=>$memberID,
								'LogNo'=>$LogNo,
								'qCode'=>$qid,
								'QuestionNo'=> $key+1,
								'MemberAns'=>$this->numberToChar(substr($qans, -1)),
								'Correct' => $Correct
								);

		}

		$this->db->insert_batch('tExamD',$insertData);

		return $count;
	}

	public function getTitle($SubjectCode = null,$TeacherCode = null ,$GroupID = 1){

		$query = $this->db->get_where('mQuestionsH',array('subjectCode'=>$SubjectCode,'teacherCode'=>$TeacherCode,'GroupID'=>$GroupID));
		
		// print_r($query->row_array());
		return $query->row_array();

	}


}