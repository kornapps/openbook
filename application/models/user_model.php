<?php

Class User_model extends CI_Model {

    public function __construct() {
        $this->load->database();
    }

    function login($phoneNumber, $password) {
        $this->db->select('id, phoneNumber, password, token');
        $this->db->from('members');
        $this->db->where('phoneNumber', $phoneNumber);
        $this->db->where('password', $password);
        $this->db->limit(1);

        $query = $this->db->get();

        if ($query->num_rows() == 1) {
            return $query->result();
        } else {
            return false;
        }
    }

    // function getMemberIDBytoken($token){
    // }

    function contents($type, $value) {
        return Array($type => $value);
    }

    function getContentArray($rows) {

        $tmpArray = array();

        foreach ($rows as $key => $value) {
            # code...
            $tmpArray[$value['Type']] = $value['Value'];
        }
        return $tmpArray;
    }

    function getContent($id) {

        $query = $this->db->query('SELECT Type,Value from memberContent where id ="' . $id . '"');
        $rows = $query->result_array();
        $result = $this->getContentArray($rows);
        return $result;


        // $rs->execute();
        // $rs->setFetchMode(PDO::FETCH_ASSOC);
        // $rows = $rs->fetchAll(PDO::FETCH_FUNC, "contents");
        // return ($rows)? $rows : false;
    }

    function countMemberPoint($MemberId) {

        $usedPoint = 20;
        $totalPoint = 99;
        $Points = array('used' => $usedPoint, 'totalPoint' => $totalPoint);

        return $Points;
    }

    public function set_info()
    {
        // $this->load->helper('url');

        $id = $this->input->post('inputID');
        $Types = array('Name','School','Email');

        $this->db->where('id', $id);
        $this->db->where_in('Type', $Types);
        $this->db->delete('memberContent'); 
            
        $data[] = array(
            'id' => $this->input->post('inputID'),
            'Type' => 'Name',
            'Value' => $this->input->post('inputName'),
            'createDate' => date("Y-m-d H:i:s"),
            'updateDate'=> date("Y-m-d H:i:s")
        );

        $data[] = array(
            'id' => $this->input->post('inputID'),
            'Type' => 'School',
            'Value' => $this->input->post('inputSchool'),
            'createDate' => date("Y-m-d H:i:s"),
            'updateDate'=> date("Y-m-d H:i:s")
        );


        $data[] = array(
            'id' => $this->input->post('inputID'),
            'Type' => 'Email',
            'Value' => $this->input->post('inputEmail'),
            'createDate' => date("Y-m-d H:i:s"),
            'updateDate'=> date("Y-m-d H:i:s")
        );

        $this->db->insert_batch('memberContent',$data);


        // echo $query;

        // return $this->db->insert('Comments', $data);
    }

    public function updateExam($MemberId = null,$subjectCode = null,$cout = 0){

        // 更新科目，成績資料
        // $query = $this->db->get_where('userExamData','memberID'=>$memberID);
        // if($query->num_rows()){
        //     $data = array(
        //         'subjectCode'=>$count,
        //         'memberID'=>$memberID,
        //         'updateDate'=>date('Y-M-D H:i:s');
        //         );
        // }else{
        //     $data = array(
        //         'subjectCode'=>$count,
        //         'memberID'=>$memberID,
        //         'createDate'=>date('Y-M-D H:i:s');
        //         // 'updateDate'=>date('Y-M-D H:i:s');
        //         );

        // }

    }

}
