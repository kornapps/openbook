<?php
class market_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}

	public function get_markets($id = FALSE)
	{
		if ($id === FALSE)
		{
			$query = $this->db->get_where('Markets', array('status' => 'A'));
			// $query = $this->db->get('Markets');
			return $query->result_array();
		}
		
		$query = $this->db->get_where('Markets', array('id' => $id));
		return $query->result_array();
	}

}