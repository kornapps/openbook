<?php
class exams_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}

	public function get_exames($id = FALSE)
	{
		if ($id === FALSE)
		{
			$query = $this->db->get('mExamH');
			return $query->result_array();
		}
		
		$query = $this->db->get_where('mExamH', array('id' => $id));
		return $query->result_array();
	}

	public function get_TopRank($SubjectCode = 0){

		$a_procedure = "CALL getTopRank(?)";

		if($SubjectCode > 0){
			$a_result = $this->db->query( $a_procedure, array('first'=>$SubjectCode) );			
		}else{
			$a_result = $this->db->query( $a_procedure, array('first'=>0) );			
		}

		$ranks = $this->rankDataForWeb($a_result->result_array());

		// return $a_result->result_array();
		return $ranks;
	}

	public function get_MyRank($SubjectCode=null,$MemberCode=null){

		$a_procedure = "CALL getMyRank(?,?)";

		// echo $SubjectCode . '' . $MemberCode;

		if($MemberCode){

			$a_result = $this->db->query( $a_procedure, array('setSubjectCode'=>$SubjectCode,'setMemberCode'=>$MemberCode));

			return $a_result->result_array();	

		}else{
			return FALSE;
		}
	}

	public function toChartData($mypoint = null){
		if($mypoint){

			$toChartData = array();
			$name = '';
			$point = '';
			$haveCN = '';
			$haveEN = '';
			$haveMA = '';

			if(count($mypoint) >= 6){
				$x = 5;
			}else{
				$x = count($mypoint) -1;
			}

			// 最少有四科顯示
			if($x < 3){
				for($i=0;$i <= $x;$i++){
					if($mypoint[$i]['NameCN'] == '中文科') $haveCN = true;
					if($mypoint[$i]['NameCN'] == '英文科') $haveEN = true;
					if($mypoint[$i]['NameCN'] == '數學科') $haveMA = true;
				}
				if(!$haveCN){ $mypoint[] = array('NameCN'=>'中文科','POINT'=>0); $x++;}
				if(!$haveEN){ $mypoint[] = array('NameCN'=>'英文科','POINT'=>0); $x++;}
				if(!$haveMA){ $mypoint[] = array('NameCN'=>'數學科','POINT'=>0); $x++;}

			}

			// print_r($mypoint);
			// get max top 6 data
			for($i=0;$i <= $x;$i++){
				$name .= '"'.$mypoint[$i]['NameCN'].'('.$mypoint[$i]['POINT'].')",';
				$point .= $mypoint[$i]['POINT'].',';
			}
			// echo $name .':'.$point;
			return $toChartData = array('NameCN'=>$name,'POINT'=>$point);

		}else{
			return FALSE;
		}

	}

	public function get_FriendsRank($SubjectCode=null,$MemberCode=null){

		$a_procedure = "CALL getFriendsRank(?,?)";

		if($SubjectCode && $MemberCode){

			$a_result = $this->db->query( $a_procedure, array('setSubjectCode'=>$SubjectCode,'setMemberCode'=>$MemberCode));

		$ranks = $this->rankDataForWeb($a_result->result_array());

		// return $a_result->result_array();
		return $ranks;

			// return $a_result->result_array();	

		}else{
			return FALSE;
		}
	}

	public function rankDataForWeb(array $ranks = null){

		if($ranks){

			$newRanks = array();

			foreach ($ranks as $key) {

				$subjectID = $key['subjectCode'];

				// if($key['row_number'] < 10){
					$newRanks['SubjectCode'][$key['subjectCode']]['data'][] = $key;
					$newRanks['SubjectCode'][$key['subjectCode']]['name'] = $key['NameCN'];
				// }
			}
			// echo 'test';
			// print_r($newRanks);
			return $newRanks;
		}else{

			return FALSE;
		}
	}

	public function getMyQuizStatus($SubjectCode = null,$TeacherCode=null,$MemberCode = null){

		$sql = "select QuestionNo,
				case 
				when MemberAns <> '' and Correct = 'Y' then 'C' 
				when MemberAns <> '' and Correct = 'N' then 'W'
				end as `Ans`
				,MemberAns,Correct,`SubjectCode`,teacherCode from `tExamD` a 
				left join  mQuestions b on a.`QuestionNo` = b.`Code`
				 where 
				 b.SubjectCode = ?
				 and b.teacherCode = ?
				 and a.memberCode = ?
				 group by QuestionNo
				 order by QuestionNo";


		$result = $this->db->query($sql,array('setSubjectCode'=>$SubjectCode,'setTeacherCode'=>$TeacherCode,'setMemberCode'=>$MemberCode));

		$qstatus = $result->result_array();

		// print_r($qstatus);

		return ($qstatus);
	}


}