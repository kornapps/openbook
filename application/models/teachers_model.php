<?php
class teachers_model extends CI_Model {

  public function __construct()
  {
    $this->load->database();
  }

  public function get_teachers($id = FALSE)
  {
    if ($id === FALSE)
    {
      $this->db->group_by("NameCN"); 
      $this->db->order_by("sorting"); 
      $query = $this->db->get_where('mTeachers',array('status'=>'A'));

      // $this->db->group_by("NameCN"); 
      // $query = $this->db->get('mTeachers');
      return $query->result_array();
    }
    
    $query = $this->db->get_where('mTeachers', array('id' => $id));
    return $query->result_array();
  }

  public function get_teachersWithSubjectinfo(){

    // $query = $this->db->get('mTeachers');
    $query = $this->db->get_where('mTeachers', array('status' => 'A'));
    $teachers = $query->result_array();

    $query = $this->db->query("select * from `mSubjects` a
left join (select `code` ,`SubjectCode` ,`TeacherCode` from `mQuestions` group by `TeacherCode` order by cast(`TeacherCode`  as unsigned)) b
on a.`id` = b.`SubjectCode` ;");

    return $query->result_array();

  }
}