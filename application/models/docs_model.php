<?php
class docs_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}

	public function get_doc($teacherCode = FALSE,$paper =1)
	{
		if($teacherCode === FALSE)
		{
			// die 'ERROR';
			return FALSE;
		}
		
		$this->db->order_by("seqNo", "asc"); 
		$query = $this->db->get_where('docs', array('teacherCode' => $teacherCode,'paper'=>$paper));

		return $query->result_array();
	}

}