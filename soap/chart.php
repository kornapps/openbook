<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Welcome to CodeIgniter</title>
	<script src="assets/Chart.js"></script>
</head>
<body>
<canvas id="canvas" height="800" width="800"></canvas>


	<script>

		var radarChartData = {
			labels : ["中文","歴史","英文","科學","地理","數學","電腦"],
			datasets : [
				{
					fillColor : "rgba(220,220,220,0.5)",
					strokeColor : "rgba(220,220,220,1)",
					pointColor : "rgba(220,220,220,1)",
					pointStrokeColor : "#fff",
					data : [65,59,90,81,56,55,40]
				}
			]
			
		}

	var myRadar = new Chart(document.getElementById("canvas").getContext("2d")).Radar(radarChartData,{scaleShowLabels : false, pointLabelFontSize : 14});
	
	</script></body>

</html>
