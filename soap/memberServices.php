<?php
ini_set('display_errors', '1');
include('./lib/nusoap.php');

$server  = new nusoap_server;
$server->configureWSDL('MemberServices', 'http://openbook.la/soap/');
$server->wsdl->schemaTargetNamespace = 'http://soapinterop.org/xsd/';
$server->soap_defencoding = 'UTF-8';
$server->decode_utf8 = false;
$server->register('myFunction');
$server->register(
	'getFriendsList',
	array('token' => 'xsd:string','phoneNumber' => 'xsd:string'),
	array('friends' => 'xsd:Array','History'=>'xsd:Array'),
	'http://soapinterop.org/'
	);
$server->register(
	'postFriendsList',
	array('token' => 'xsd:string','phoneNumber' => 'xsd:string'),
	array('Lists' => 'xsd:Array'),
	'http://soapinterop.org/'
	);
$server->register(
	'getMemberContents',
	array('token' => 'xsd:string','phoneNumber' => 'xsd:string'),
	array('friends' => 'xsd:Array'),
	'http://soapinterop.org/'
	);
$server->register(
	'putFriendsList',
	array('token' => 'xsd:string','uploadPhoneList' => 'xsd:string'),
	array('NumberOfFriend'=>'xsd:int','friendContent' => 'xsd:Array'),
	'http://soapinterop.org/'
	);

$server->register(
	'putMemberImage',
	array('token' => 'xsd:string','base64' => 'xsd:string'),
	array('Success' => 'xsd:string'),
	'http://soapinterop.org/'
	);

$server->register(
	'putMemberDisplayName',
	array('token' => 'xsd:string','DisplayName' => 'xsd:string'),
	array('Success' => 'xsd:string'),
	'http://soapinterop.org/'
	);

$server->register(
	'getSubjectList',
	array('token' => 'xsd:string','teacher' => 'xsd:string'),
	array('Subjects' => 'xsd:Array'),
	'http://soapinterop.org/'
	);

$server->register(
	'getTeacherList',
	array('token' => 'xsd:string','subject' => 'xsd:string'),
	array('Teachers' => 'xsd:Array'),
	'http://soapinterop.org/'
	);

$server->register(
	'getExamList',
	array('token' => 'xsd:string','subject' => 'xsd:string','teacher' => 'xsd:string'),
	array('exams' => 'xsd:Array'),
	'http://soapinterop.org/'
	);

$server->register(
	'getQuestionsByTeacher',
	array('token' => 'xsd:string','subject' => 'xsd:string','teacher' => 'xsd:string','GroupID'=>'xsd:string'),
	array('exams' => 'xsd:Array'),
	'http://soapinterop.org/'
	);

$server->register(
	'getSubjectandTeacherList',
	array('token' => 'xsd:string','subject' => 'xsd:string','teacher' => 'xsd:string'),
	array('exams' => 'xsd:Array'),
	'http://soapinterop.org/'
	);

$server->register(
	'putExamData',
	array('token' => 'xsd:string','examID' => 'xsd:string','qr' => 'xsd:string','examStart' => 'xsd:string','examEnd' => 'xsd:string','devices' => 'xsd:string'),
	array('Success' => 'xsd:boolean'),
	'http://soapinterop.org/'
	);

$server->register(
	'putTimeAttackExamData',
	array('token' => 'xsd:string','examID' => 'xsd:string','qr' => 'xsd:string','examStart' => 'xsd:string','examEnd' => 'xsd:string','devices' => 'xsd:string'),
	array('Success' => 'xsd:boolean'),
	'http://soapinterop.org/'
	);


$server->register(
	'getMemberRank',
	array('token' => 'xsd:string','subjectCode' => 'xsd:string'),
	array('Ranks' => 'xsd:Array'),
	'http://soapinterop.org/'
	);

$server->register(
	'getQuestionUpdate',
	array('token' => 'xsd:string','lastupdate' => 'xsd:string','subjectCode' => 'xsd:string','teacherCode' => 'xsd:string'),
	array('haveUpdate' => 'xsd:Array'),
	'http://soapinterop.org/'
	);

$server->register(
	'getSubjectAndTeacherUpdate',
	array('token' => 'xsd:string','subjectCode' => 'xsd:string','teacherCode' => 'xsd:string'),
	array('haveUpdate' => 'xsd:Array'),
	'http://soapinterop.org/'
	);

$server->register(
	'getMemberPoint',
	array('token' => 'xsd:string'),
	array('Points' => 'xsd:Array'),
	'http://soapinterop.org/'
	);

$server->register(
	'getVersionUpdate',
	array('app' => 'xsd:string'),
	array('lastversion' => 'xsd:Array'),
	'http://soapinterop.org/'
	);

$server->register(
	'getStarStatus',
	array('token' => 'xsd:string'),
	array('starStstus' => 'xsd:Array'),
	'http://soapinterop.org/'
	);

$server->register(
	'putStarPoint',
	array('token' => 'xsd:string','point' => 'xsd:int','pointStartDate' => 'xsd:string'),
	array('Success' => 'xsd:boolean'),
	'http://soapinterop.org/'
	);

$server->service(isset($HTTP_RAW_POST_DATA) ? $HTTP_RAW_POST_DATA : '');



// function CallAPI($method, $url, $data = false)
// {
//     $curl = curl_init();

//     switch ($method)
//     {
//         case "POST":
//             curl_setopt($curl, CURLOPT_POST, 1);

//             if ($data)
//                 curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
//             break;
//         case "PUT":
//             curl_setopt($curl, CURLOPT_PUT, 1);
//             break;
//         default:
//             if ($data)
//                 $url = sprintf("%s?%s", $url, http_build_query($data));
//     }

//     // Optional Authentication:
//     // curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
//     // curl_setopt($curl, CURLOPT_USERPWD, "username:password");

//     curl_setopt($curl, CURLOPT_URL, $url);
//     curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

//     return curl_exec($curl);
// }

/* =================================== */

function db(){

	$username = 'openbook';
	$password = 'openbook-2013';
	
	$conn = new PDO('mysql:host=;dbname=allstar;', $username, $password);

	return $conn;
}

function contents($type,$value){
	return Array($type=>$value);
}

function IDcontents($rows){

	// print_r($rows);

	foreach ($rows as $key => $value) {
		 // print_r($value);
		# code...
		if(isset($value['id'])){
			$row_id = $value['id'];
		}

		$myarray['fr_'.$row_id][] = array($value['Type']=>$value['Value']);
		// echo $row_id;

		// foreach ($value as $key2 => $value2) {
		// 	# code...
		// 	// if($key2 == 'id'){
		// 	// 	$row_id = $value2;	
		// 	// }
			
		// 	if($key2 <> 'Type'){
		// 		$myarray[] = array($key2=>$value2);	
		// 	}
			
		// 	print_r($myarray);
		// }

		// $myrows[$row_id] = $myarray;	
		 
	}
	// print_r($myarray);
	// $rows["fr_".$id] = array($type=>$value);
	// return Array($type=>$value);
	// print_r($myrows);
	return $myarray;
}


function getContent($id){
	$conn = db();
	$rs = $conn->prepare('SELECT Type,Value from memberContent where id ="'.$id.'"');
	$rs->execute();
	$rs->setFetchMode(PDO::FETCH_ASSOC);
	$rows = $rs->fetchAll(PDO::FETCH_FUNC, "contents");
	return ($rows)? $rows : false;
}

function getMemberId($token){

	$conn = db();
	$sql = 'SELECT id from members where token = "'.$token.'" and status = "A"';
	$row = $conn->query($sql);

	if($row->rowCount() > 0){
		return $row->fetchColumn();
	}else{
		return false;
	}
}

function getLastExamLogID(){
	$conn = db();
	$sql = 'SELECT MAX(LogNo) from tExamH';
	$row = $conn->query($sql);
	return $row->fetchColumn();
}

function getLastTimeAttackExamLogID(){
	$conn = db();
	$sql = 'SELECT MAX(LogNo) from tExamH_t';
	$row = $conn->query($sql);
	return $row->fetchColumn();
}

function countMemberPoint($MemberId){
    
        $usedPoint = 20;
        $totalPoint = 50;
        // 要處理
        $Points = array('used'=>$usedPoint,'totalPoint'=>$totalPoint);
        
//        print_r($Points);
	return $Points;

}

function savePostData($post){

	$conn = db();
	$sql = "INSERT INTO PostLog (data,createdDate) values ('$post', NOW())";
	$row = $conn->query($sql);
	// echo $sql;
	// return $row->fetchColumn();

}

/* ====================================== */

function getMemberContents($token,$phoneNumber){

	$MemberId = getMemberId($token);

	$conn =db();
	$rs = $conn->prepare('SELECT Type,Value from memberContent where id ="'.$MemberId.'"');
	$rs->execute();
	$rs->setFetchMode(PDO::FETCH_ASSOC);
	$rows = $rs->fetchAll(PDO::FETCH_FUNC, "contents");

	if($rs->rowCount() > 0){
		return array('Success' =>true,'Contents'=>$rows);
	}
    //return  $rows;
}


// function getFriendsList($token,$$list){
// 	//get the friends list from server

// 	split(',', $list);

// 	return array('Success' =>true);

// }

function getMemberPoint($token){
	$conn = db();

	$MemberId = getMemberId($token);

	return $Points = countMemberPoint($MemberId);
}

function getMemberMark($MemberId,$subjectCode=null){
	
	return $Mark = 70;
}

function getMemberLevel($MemberId){

	return $Level = 3;
}


function getFriendsList($token,$phoneNumber){

	$conn = db();

	$MemberId = getMemberId($token);

	// $rs = $conn->prepare("select mid,phoneNumber,c.Type,c.Value from memberPhoneStatus a left join members b on a.mid = b.id left join memberContent c on a.mid = c.id  where a.type ='F' and a.id = $MemberId");
	// $rs->execute();
	// $rs->setFetchMode(PDO::FETCH_ASSOC);
	// $rows = $rs->fetchAll(PDO::FETCH_FUNC, "contents");
	// return array('Success' =>true);

	$rs = $conn->query("SELECT mid from memberPhoneStatus where id = $MemberId");
	$rs->setFetchMode(PDO::FETCH_ASSOC);
	$idList = $rs->fetchAll();

	foreach ($idList as $key => $value) {
		$friends['fr_'.$value['mid']] = getContent($value['mid']);
	}

	 // print_r($friends);

	$History[] = array('subject'=>'中文科','version'=>'1','Time'=>'2013-10-29 17:08:50','Count'=>'8');
	$History[] = array('subject'=>'英文科','version'=>'1','Time'=>'2013-10-29 16:08:50','Count'=>'4');
	$History[] = array('subject'=>'中文科','version'=>'1','Time'=>'2013-10-29 15:08:50','Count'=>'3');


	return array($friends,$History);

}

function saveToFriendList($id,$fid,$phoneNumber = ''){

	$conn = db();
	$conn->prepare("insert into memberFriendList (id, friendId, phoneNumber, status) values(?,?,?,?)");
	$conn->ececute(array($id,$fid,$phoneNumber,''));

}

function splitUploadPhoneNumberList($uploadPhoneList){

	//save to friend List

	$conn = db();
	$rs = $conn->prepare("INSERT IGNORE INTO memberPhoneList set phone = :phone");

	$newPhoneList =  ereg_replace("'", '', $uploadPhoneList);
	$lists = split(',',$newPhoneList);

	foreach ($lists as $key => $value) {
		// $rs->bindParam(':id',$id);
		$rs->bindParam(':phone',$value);
		$rs->execute();
	}
}

function updatePhoneFriend($MemberId,$uploadPhoneList){
	$stat = 'F';
	$conn = db();
	$rs = $conn->prepare("REPLACE INTO memberPhoneStatus (id,mid,type) SELECT $MemberId,id,:stat from members where phoneNumber in (".$uploadPhoneList.") and status = 'A' " );
	// $rs->bindParam(':id',$MemberId);
	$rs->bindParam(':stat',$stat);
	$rs->execute();
	// echo $rs->rowCount();
}

function getFrientsContent($MemberId){

	$conn = db();
	$rs = $conn->prepare("select id, Type,Value from `memberContent` where id in (select mid from memberPhoneStatus where id = :id);");
	$rs->bindParam(':id',$MemberId);
	$rs->execute();
	$rs->setFetchMode(PDO::FETCH_ASSOC);
	// $rows = $rs->fetchAll(PDO::FETCH_FUNC, "IDcontents");
	$rows = $rs->fetchAll();
	$newRow = IDcontents($rows);
	 // print_r($rs->fetchAll());
	// print_r($newRow);
	return ($newRow);
}


function putFriendsList($token,$uploadPhoneList){
	//input the friend List to server
	
	$conn = db();
	$MemberId = getMemberId($token);

	$newPhoneList = rtrim($uploadPhoneList,",");

	savePostData($newPhoneList);

	if($MemberId){

		splitUploadPhoneNumberList($newPhoneList);

		$rows = $conn->query('SELECT id,phoneNumber from members where phoneNumber in ('.$newPhoneList.') and status = "A"');

		$NumberOfFriend = $rows->rowCount();

		foreach ($rows->fetchAll() as $key => $value) {

			$tmp = getContent($value['id']);
			if($tmp)
				$totalContents[] = $tmp;
		};

		updatePhoneFriend($MemberId,$newPhoneList);

		$totalContents = getFrientsContent($MemberId);

		// print_r($totalContents);

	// echo $rows->rowCount();
		return array('NumberOfFriend'=>$NumberOfFriend,'friendContent'=>$totalContents);

	}else{
		return array('Success'=> false);
	}

}


function putMemberImage($token,$base64){

	$MemberId = getMemberId($token);

	if($MemberId){
		define('UPLOAD_DIR', '../assets/img/m/'); //要留意
		// $img = $_POST['img'];
		// $img = str_replace('data:image/png;base64,', '', $img);
		// $img = str_replace(' ', '+', $img);
		$data = base64_decode($base64);
		$file = UPLOAD_DIR . $MemberId . '.png';
		$success = file_put_contents($file, $data);
		$success ? $file : false ;

		if($success){
			$conn = db();
			$sql1 = "delete from memberContent where id=$MemberId and Type = 'imagePath'";
			$conn->query($sql1);

			$imagePath = '/assets/img/m/'.$MemberId . '.png';

			$sql2 = "insert into memberContent (id,Type,Value) VALUES(:mid,'imagePath',:file)";
			$rs = $conn->prepare($sql2);
			$rs->bindParam(':mid', $MemberId);  
			$rs->bindParam('file', $imagePath);  
			$rs->execute();

			return $success =  true ;

		}else{

			return $success =  false ;

		}
			return $success =  false ;
		}
	}

	function putMemberDisplayName($token,$DisplayName){

		$MemberId = getMemberId($token);

		if($MemberId){

			$conn = db();

			$sql1 = "delete from memberContent where id=$MemberId and Type = 'displayName'";
			$conn->query($sql1);

			$sql2 = "insert into memberContent (id,Type,Value) VALUES($MemberId,'displayName','$DisplayName')";		
			$conn->query($sql2);

		}else{
			return $success =  false ;
		}

	}


	function getSubjectList($token = null,$teacher = null){
		$conn = db();
		$rows = $conn->query('SELECT * from mSubjects order by id');
		return $rows->fetchAll();
	}

	function getTeacherList($token = null,$subject = null){
		$conn = db();
		$rows = $conn->query('SELECT * from mTeachers order by id');
		return $rows->fetchAll();
	}

	function getExamList($token = null,$subject = null,$teacher = null){
		//找出指定的遊戲成式

		$conn = db();

		if($subject && $teacher){

			$sql = "SELECT * from mExamH a left join mSubjects b  on a.subjectCode = b.id left join mTeachers c  on a.teacherCode = c.id where subjectCode = $subject and teacherCode = $teacher order by a.id";

		}else{

			$sql = "SELECT * from mExamH a left join mSubjects b  on a.subjectCode = b.id left join mTeachers c  on a.teacherCode = c.id  where mStatus = 'A' ";
		}

		$rows=$conn->query($sql);
		return $rows->fetchAll();
	}

	function getQuestionsByTeacher($token = null,$subject = null,$teacher = null,$GroupID = null){

                $MemberId = getMemberId($token);
                
		$conn = db();
		$sql = " SELECT * from mQuestions where SubjectCode = $subject and TeacherCode = $teacher ";

		$sql = "select a.*,b.`sortTitle`,b.`fullTitle` from mQuestions a
		left join mQuestionsH b on a.subjectCode = b.subjectCode and a.teacherCode = b.teacherCode and a.GroupID = b.`GroupID`
		where a.subjectCode = $subject and a.teacherCode = $teacher ";

		// if($GroupID) $sql .= " and GroupID = "

		// $sql = "select code,SeqNo,a.SubjectCode,a.TeacherCode,mStatus,b.sortTitle as `GroupID`,a.`DescriptionCN`,a.`DescriptionEN`,a.PicPath,a.YouTubePath,a.`CorrectAnswer`,
		// a.`ChoiceACN`,a.`ChoiceACNPath`,a.`ChoiceAEN`,a.`ChoiceAENPath`,a.`ChoiceBCN`,a.`ChoiceBCNPath`,a.`ChoiceBEN`,a.`ChoiceBENPath`,
		// a.`ChoiceCCN`,a.`ChoiceCCNPath`,a.`ChoiceCEN`,a.`ChoiceCENPath`,a.`ChoiceDCN`,a.`ChoiceDCNPath`,a.`ChoiceDEN`,a.`ChoiceDENPath`,
		// a.`ChoiceECN`,a.`ChoiceECNPath`,a.`ChoiceEEN`,a.`ChoiceEENPath`,
		// a.`CreatedTime`,a.`CreateUser`,a.`EditTime`,a.`EditUser`
		// from mQuestions a
		// left join mQuestionsH b on a.subjectCode = b.subjectCode and a.teacherCode = b.teacherCode and a.GroupID = b.`GroupID`
		// where a.subjectCode = $subject and a.teacherCode = $teacher ";


		// if($GroupID){
		// 	// $sql = $sql . " and GroupID = $GroupID ";
		// 	$sql = $sql . " and a.GroupID = $GroupID ";
		// }
		
		// $sql = $sql .  " and mStatus = 'A' order by GroupID";
		$sql = $sql .  " and mStatus = 'A' order by a.`SeqNo`, a.GroupID";

		$rows = $conn->query($sql);
		return $rows->fetchAll();
	}

	function getSubjectandTeacherList($token = null, $subject = null,$teacher = null){

		$conn = db();
		$sql = "SELECT subjectCode, TeacherCode, b.`NameCN` as 'subNameCN', b.`NameEN` as 'subNameEN',b.`PicPath` as 'subPicPath',c.`NameCN` as 'tecNameCN', case when c.`nameEN` <> '' then CONCAT('http://openbook.la/index.php', c.`nameEN`) else '' end as 'tecNameEN',c.`PicPath` as 'tecPicPath' from `mQuestions` a ";
		$sql = $sql . " left join mSubjects b on a.subjectCode = b.id left join mTeachers c on a.TeacherCode = c.id ";
		if($subject){
			$sql = $sql . " where SubjectCode = $subject ";
		}
		if($teacher){
			$sql =  $sql . " where TeacherCode = $teacher ";
		}
		$sql =  $sql . " group by a.subjectCode,a.teacherCode order by c.sorting ";

		 // echo $sql;

		$rows = $conn->query($sql);
		return $rows->fetchAll();
	}

	function putExamData($token,$examID = null,$qr=null,$examStart=null,$examEnd=null,$deveice = 'an'){

		$conn = db();
		$MemberId = getMemberId($token);
		$lastLogNo = getLastExamLogID();


		$rs =$conn->prepare("insert into tExamH (ExamCode,MemberCode,LogNo,StartTime,EndTime,Deveice) values(?,?,?,?,?,?)");
		$rs->execute(array($examID,$MemberId,$lastLogNo+1,$examStart,$examEnd,$deveice));

		$data = split(',', $qr);

		$rs2 = $conn->prepare('insert into tExamD (ExamCode,MemberCode,LogNo,qCode,QuestionNo,MemberAns,Correct) values (?,?,?,?,?,?,?)');

		foreach ($data as $key => $value) {
		# code...
			list($tmpQid,$tmpAns,$tmpCorrect) = split(':', $value);
			$rs2->bindParam(1, $examID);  
			$rs2->bindParam(2, $MemberId);  
			$rs2->bindParam(3, $lastLogNo);
			$rs2->bindParam(4, $tmpQid);  
			$rs2->bindParam(5, $tmpQid);  
			$rs2->bindParam(6, $tmpAns);
			$rs2->bindParam(7, $tmpCorrect);
			$rs2->execute();  
		}
		
		return array("Success"=>true);
	}

	function putTimeAttackExamData($token,$examID = null,$qr=null,$examStart=null,$examEnd=null,$deveice = 'an'){

		$conn = db();
		$MemberId = getMemberId($token);
		$lastLogNo = getLastTimeAttackExamLogID();

		$rs =$conn->prepare("insert into tExamH_t (ExamCode,MemberCode,LogNo,StartTime,EndTime,Deveice) values(?,?,?,?,?,?)");
		$rs->execute(array($examID,$MemberId,$lastLogNo+1,$examStart,$examEnd,$deveice));

		$data = split(',', $qr);

		$rs2 = $conn->prepare('insert into tExamD_t (ExamCode,MemberCode,LogNo,qCode,QuestionNo,MemberAns,Correct) values (?,?,?,?,?,?,?)');

		foreach ($data as $key => $value) {
		# code...
			list($tmpQid,$tmpAns,$tmpCorrect) = split(':', $value);
			$rs2->bindParam(1, $examID);  
			$rs2->bindParam(2, $MemberId);  
			$rs2->bindParam(3, $lastLogNo);
			$rs2->bindParam(4, $tmpQid);  
			$rs2->bindParam(5, $tmpQid);  
			$rs2->bindParam(6, $tmpAns);
			$rs2->bindParam(7, $tmpCorrect);
			$rs2->execute();  
		}
		
		return array("Success"=>true);
	}


	function getTopRank($SubjectCode = null){
		
		$conn = db();

		if($SubjectCode > 0){
			$rows = $conn->prepare('CALL `allstar`.`getTopRank`(?);');
			$rows->bindParam(1,$SubjectCode);
		}else{
			$rows = $conn->prepare('CALL `allstar`.`getTopRank`(0);');
		}

        $rows->execute();
        $rows->setFetchMode(PDO::FETCH_ASSOC);
        // print_r($rows->fetchAll());
                
		return $rows->fetchAll();

	} 

	function getMyRank($SubjectCode = null,$MemberCode = null){

		$conn = db();
		$conn2 = db();

		if($MemberCode && $SubjectCode){

			$rows = $conn->prepare('CALL `allstar`.`getMyrank`(?,?);');
			$rows->bindParam(1,$SubjectCode);
			$rows->bindParam(2,$MemberCode);

	        $rows->execute();
	        $rows->setFetchMode(PDO::FETCH_ASSOC);

	        $rank['my'] = $rows->fetchAll();

	        // print_r($rank);


			$rows2 = $conn2->prepare('CALL `allstar`.`getFriendsRank`(?,?);');
			$rows2->bindParam(1,$SubjectCode);
			$rows2->bindParam(2,$MemberCode);

	        $rows2->execute();
	        $rows2->setFetchMode(PDO::FETCH_ASSOC);
	        $rank['friends'] = $rows2->fetchAll();

	        // print_r($rank);

	        // print_r($rows->fetchAll());
	                
			return $rank;

		}else{

			return false;
		}


	}



	function getMemberRank($token,$subject = null){

		$MemberId = getMemberId($token);

		$topRank = getTopRank($subject);
		$myRank = getMyrank($subject,$MemberId);

		// print_r($MemberId);

		// print_r($topRank);
		// print_r($myRank);
		// $myRank[0]['row_number'] = 3;

		// print_r($myRank);

		// if($myRank){
		// 	if($myRank[0]['row_number'] < 8){
		// 		$topRank[$myRank[0]['row_number']-1] = $myRank[0];
		// 		// $topRank[3] = array()
		// 	}else{
		// 		$topRank[9] = $myRank[0];
		// 	}	
		// }


		// print_r($topRank);

		if($MemberId){

		$HKRank = array();

		foreach ($topRank as $row){
			$HKRank[str_replace(' ', '.', $row['displayName'])] = $row['POINT'].':'.$row['imagePath'];
		}

		$friendsRank = array();
		foreach ($myRank['friends'] as $row2){
			$friendsRank[str_replace(' ', '.', $row2['displayName'])] = $row2['POINT'].':'.$row2['imagePath'];
		}



//			if($subject){
				// 得到按學料成績的排列
				// $friendsRank = array(
				// 	'Lung'=>'98:/assets/img/m/default.icon.png',
				// 	'Yuku'=>'98:/assets/img/m/default.icon.png',
				// 	'Taka'=>'86:/assets/img/m/default.icon.png',
				// 	'Mr.x'=>'98:/assets/img/m/default.icon.png',
				// 	'Mr.y'=>'78:/assets/img/m/default.icon.png',
				// 	'Mr.z'=>'98:/assets/img/m/default.icon.png',
				// 	'Mr.a'=>'83:/assets/img/m/default.icon.png',
				// 	'Mr.c'=>'52:/assets/img/m/default.icon.png',
				// 	'Mr.d'=>'67:/assets/img/m/default.icon.png',
				// 	'me'=>'100:/assets/img/m/1.png',
				// );

				// $HKRank = array(
				// 	'HK-17'=>'98:/assets/img/m/default.icon.png',
				// 	'HK-22'=>'98:/assets/img/m/default.icon.png',
				// 	'HK-16'=>'86:/assets/img/m/default.icon.png',
				// 	'HK-8'=>'98:/assets/img/m/default.icon.png',
				// 	'HK-11'=>'78:/assets/img/m/default.icon.png',
				// 	'HK-31'=>'98:/assets/img/m/default.icon.png',
				// 	'HK-7'=>'83:/assets/img/m/default.icon.png',
				// 	'HK-115'=>'52:/assets/img/m/default.icon.png',
				// 	'HK-21'=>'67:/assets/img/m/default.icon.png',
				// 	'me'=>'67:/assets/img/m/1.png'
				// 	);
				$Ranks[] = array('subjectName'=>$subject,'friendRank'=>$friendsRank,'HKRank'=>$HKRank);
                // $Ranks[] = array('subjectName'=>2,'friendRank'=>$friendsRank,'HKRank'=>$HKRank);
				return $Ranks;
//			}

			// if($teacher){
			// 	// 得到導師成綪
			// }
		}

	}

	function getQuestionUpdate($token,$lastupdate = null,$subjectCode = null,$teacherCode = null){

        $MemberId = getMemberId($token);
		//CHECK lastupdate or created 

		$conn = db();

		$sqlQ ='SELECT * from mQuestions where subjectCode = :subject and teacherCode = :teacher and (CreatedTime < :date or EditTime < :date)';

		$rs = $conn->prepare($sqlQ);
		$rs->bindParam(':date',$lastupdate);
		$rs->bindParam(':subject',$subjectCode);
		$rs->bindParam(':teacher',$teacherCode);
		$rs->execute();

		// print_r($rs->rowCount());

		if($rs->rowCount() > 0){

			return array('haveUpdate'=>true);
		}else{
			return array('haveUpdate'=>true);
		}

		// $sqlQ ='SELECT * from mQuestions where CreatedTime < $lastupdate or EditTime < $lastupdate';

	}



	function getSubjectAndTeacherUpdate($token,$subjectCode = null,$teacherCode = null){

        $MemberId = getMemberId($token);
		//CHECK lastupdate or created 

		$conn = db();

		$sqlQ ="SELECT * from mQuestions where subjectCode not in ($subjectCode) or teacherCode not in ($teacherCode)";

		// print_r($sqlQ);
		$rs = $conn->prepare($sqlQ);
		// $rs->bindParam(':subject',$subjectCode);
		// $rs->bindParam(':teacher',$teacherCode);
		$rs->execute();

		if($rs->rowCount() > 0){
			return array('haveUpdate'=>true);
		}else{
			return array('haveUpdate'=>true);
		}

		// $sqlQ ='SELECT * from mQuestions where CreatedTime < $lastupdate or EditTime < $lastupdate';

	}

	function getVersionUpdate($app = null){

		$sql = 'SELECT lastversion,url from versions where app = :app order by CreatedTime desc limit 1';

		$conn = db();
		$rows = $conn->prepare($sql);
		$rows->bindParam(':app',$app);
                $rows->execute();
                $rows->setFetchMode(PDO::FETCH_ASSOC);
//                print_r($rows->fetchAll());
                
		return array('lastversion'=>$rows->fetchAll());

	}


	function getStatPoint($MemberCode = null){

		$conn= db();
		$sql = "SELECT COUNT(a.id) as `starPoint`, a.memberid, b.Value as `displayName` from starPoint a";
		$sql .= " left join `memberContent` b on a.memberid = b.id and b.Type = 'displayName' " ;
		if ($MemberCode) {$sql .= " where a.memberid = :memberid ";}
		$sql .= " group by a.memberid ";
		$sql .= " order by `starPoint` DESC ";
		$sql .= " limit 20 ";

		$rows = $conn->prepare($sql);
		$rows->bindParam(':memberid',$MemberCode);
                $rows->execute();
                $rows->setFetchMode(PDO::FETCH_ASSOC);
//                print_r($rows->fetchAll());
                

        // echo $sql;
                
		return $rows->fetchAll();
                                                                                    
	}


	function getStarStatus($token){

       $MemberId = getMemberId($token);
		//CHECK lastupdate or created 
       $points = getStatPoint();
        // print_r($points);

       if($MemberId){
		$conn = db();

		$starStstus = array();
		$starStstus['myStar'] = 10;
		$starStstus['myStarLevel'] = 12;

		foreach ($points as $row){
			$starStstus['StarRank'][str_replace(' ', '.', $row['displayName'])] = $row['starPoint'];
		}

		// $starStstus = array('myStar'=>5,'myStarLevel'=>55,'StarRank'=>
		// 	array('User1'=>88,
		// 		  'User2'=>87,
		// 		  'User3'=>77,
		// 		  'User4'=>75,
		// 		  'User5'=>73,
		// 		  'User6'=>69,
		// 		  'User7'=>69,
		// 		  'User8'=>65,
		// 		  'User9'=>63,
		// 		  'User10'=>61,
		// 		  'User11'=>60,
		// 		  'User12'=>58,
		// 		  'User13'=>57,
		// 		  'User14'=>56,
		// 		  'User15'=>55,
		// 		  'User16'=>55,
		// 		  'User17'=>53,
		// 		  'User18'=>52,		
		// 		  'User19'=>46,
		// 		  'User20'=>34,
		// 		)
		// 	);

		return $starStstus;

       }else{

       	return $success = false;
       }
	}

	function putStarPoint($token,$point,$pointStartDate){
		$conn = db();
		$MemberId = getMemberId($token);

		$rs2 = $conn->prepare('INSERT INTO `starPoint` (`id`, `memberid`, `point`, `createDate`) VALUES (NULL, ?, ?, ?)');
		$rs2->bindParam(1, $MemberId);
		$rs2->bindParam(2, $point);
		$rs2->bindParam(3, $pointStartDate);
		$rs2->execute();

		return array("Success"=>true);

	}