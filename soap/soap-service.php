﻿<?php
ini_set('display_errors', '1');

ini_set("soap.wsdl_cache_enabled", "0"); // disabling WSDL cache
$server = new SoapServer("http://webdemo14.gaias.com.hk/soap/wsdl.php?WSDL");	// Locate WSDL file to learn structure of functions
$server->addFunction("echoServices");
$server->addFunction("checkPhoneNumber");
$server->addFunction("login");
$server->addFunction("getSMSCode");
$server->addFunction("registerMember");// Same func name as in our WSDL XML, and below
$server->handle();

function echoServices($formdata){
    $attempt = false; // File writing attempt successful or not
    $formdata = get_object_vars($formdata); // Pull parameters from SOAP connection
    
    // Sort out the parameters and grab their data
    $myname = $formdata['Name'];
    $myText = $formdata['Text'];
    
    $str =  "Name: " . $myname . ", ";
    $str .= "Text: " . $myText .  "\r\n";
    
//    $filename = "./echodata.txt";
//    if (($fp = fopen($filename, "a")) == false) return array('Success' => false);
//    if (fwrite($fp, $str)) {
//    	$attempt = true;
//    }
//    fclose($fp);     

    return array('Success' => $attempt,'sendName'=>$myname,'sendText'=>$myText);    

}

function db(){

	$username = 'webdemo14';
	$password = 'webdemo14-2013';

	$conn = new PDO('mysql:host=;dbname=allstar', $username, $password);

	return $conn;
}

function genToken(){
	return  md5(uniqid(mt_rand(),true));
}

function setDisplayName($id,$displayName){

	$conn = db();
	$conn->prepare("update members set displayName");

}

function login($formdata){

	$formdata = get_object_vars($formdata); // Pull parameters from SOAP connection
    
    $phoneNumber = $formdata['phoneNumber'];
    $password = $formdata['password'];
    $smsCode = $formdata['smsCode'];

    $sql = 'SELECT * from members where phoneNumber = "'.$phoneNumber.'" and smsCode = "'.$smsCode.'" and status = "A" limit 1';

    $conn = db();

    $row = $conn->query($sql);

    foreach ($row as $key) {
    	# code...
    	$memberId = $key['id'];
    }

    if($row->rowCount() > 0){
    	$token = genToken();

    	$updateSql = 'update members set token = "'.$token.'", smsCode ="" ,lastLogin_date =  NOW(), lastUpdate_date = NOW(), status = "A" where id = "'.$memberId.'"';

    	$conn->query($updateSql);

    	$sql = 'SELECT Type,Value from memberContent where id ="'.$memberId.'"';

    	$rs = $conn->prepare('SELECT Type,Value from memberContent where id ="'.$memberId.'"');
    	$rs->execute();
    	$rs->setFetchMode(PDO::FETCH_ASSOC);
    	$rows = $rs->fetchAll();


    	return  array('Success' => true , 'token' => $token, 'memberContent'=>$rows);    	

    	}else{

    	return  array('Success' => false , 'token' => '','memberContent' => '');    	
    	}

}


function checkPhoneNumber($formdata){

	$formdata = get_object_vars($formdata); // Pull parameters from SOAP connection
    
    $phoneNumber = $formdata['phoneNumber'];

    $sql = 'SELECT id from members where phoneNumber = "'.$phoneNumber.'" and status = "A"';
	
	$conn = db();
	$rs = $conn->query($sql);
	if($rs->rowCount() > 0){
		//is register
		return array('Success'=> true);
	}else{
		//not register
		return array('Success'=> false);
	}

}

function registerMember($formdata){

    $formdata = get_object_vars($formdata); // Pull parameters from SOAP connection
    
    $phoneNumber = $formdata['phoneNumber'];
    $setPassword = $formdata['password'];
    $smsCode = $formdata['smsCode'];

    $conn =db();

    $sql = 'SELECT id from members where phoneNumber = "'.$phoneNumber.'" and smsCode = "'.$smsCode.'" and status = "V"';
    $row = $conn->query($sql);
    foreach ($row as $key) {
    	$memberId = $key['id'];
    }

    $STH = $conn->prepare("UPDATE members SET password = :pass, smsCode = '', status = 'A' , token = :token ,created_date = NOW() where id = :id");
    $STH->bindParam(':pass', md5($setPassword));  
	$STH->bindParam(':id', $memberId);
	$STH->bindParam(':token',genToken());  
	$STH->execute();

	$row = $conn->query('SELECT * from members where id = "'.$memberId.'" and status = "A"');
    if($rom->rowCount() > 0){
	    foreach ($row as $key) {
	    	$token = $key['token'];
	    }

	       return array('Success' => true,'token'=>$token);

	}else{
		   return array('Success' => false,'token'=>'');


	}

}

function getSMSCode($formdata){

    $formdata = get_object_vars($formdata); // Pull parameters from SOAP connection
    $phoneNumber = $formdata['phoneNumber'];

	$conn = db();

    // check is new number yes or not

    $sql = 'SELECT id from members where phoneNumber = "'.$phoneNumber.'" and status = "A"';
	
	$rs = $conn->query($sql);

	if($rs->rowCount() > 0){
		//is register
		$newNumber = false;
		foreach ($rs as $key) {
			# code...
			$memberId = $key['id'];
		}

	}else{
		//not register
		$newNumber = true;
	}  

	//auto 6 dig password
	$password = mt_rand(100000,999999);

    // send the SMS and save to mamber table
    $smsCode = $password = mt_rand(1000,9999);

    sendSMS($phoneNumber,$smsCode);

    if($newNumber){

	    $status ='A';

	    $STH = $conn->prepare("INSERT INTO members (phoneNumber, password,smsCode, status) value (:phone, :pass,:sms, :stat)");
	    $STH->bindParam(':phone', $phoneNumber); 
	    $STH->bindParam(':pass', $password);
		$STH->bindParam(':sms', $smsCode);  
		$STH->bindParam(':stat', $status);
		$STH->execute();

		return array('Success' =>true ,'smsCode'=>$smsCode, 'password'=>$password,'newNumber'=>$newNumber);

    }else{

	    $STH = $conn->prepare("UPDATE members set smsCode = :sms where id = :id and phoneNumber =:phone ");
	   	$STH->bindParam(':id', $memberId);  
	    $STH->bindParam(':phone', $phoneNumber);  
		$STH->bindParam(':sms', $smsCode);  
		$STH->execute();

		return array('Success' =>true ,'smsCode'=>$smsCode, 'password'=>'','newNumber'=>$newNumber);

    }

}

function sendSMS($phoneNumber,$message){

	ini_set("max_execution_time","60");

	$msg=urlencode($message);

	$phone=$phoneNumber;
	$pwd="99778909";
	$accountno="11008002";

	$handle = fopen("http://api.accessyou.com/sms/sendsms-utf8.php?msg=$msg&phone=$phone&pwd=$pwd&accountno=$accountno", "r");

	$contents = trim(fread($handle, 8192));
	//echo $contents."\n";
	if (!is_numeric($contents)){
	//echo "failure\n";
	return false;

	}

}


function getSMSCodeBak($formdata){

    $formdata = get_object_vars($formdata); // Pull parameters from SOAP connection
    $phoneNumber = $formdata['phoneNumber'];

    // send the SMS and save to mamber table
    $smsCode = '6523';
    $status ='V';

    $conn = db();

    $STH = $conn->prepare("INSERT INTO members (phoneNumber, smsCode, status) value (:phone, :sms, :stat)");
    $STH->bindParam(':phone', $phoneNumber);  
	$STH->bindParam(':sms', $smsCode);  
	$STH->bindParam(':stat', $status);
	$STH->execute();

	if(isset($smsCode)){
			return array('Success' =>true ,'smsCode'=>$smsCode);
	}else{
			return array('Success' =>false ,'smsCode' => '');
	}

}

function getFriendList($formdata){

    $formdata = get_object_vars($formdata); // Pull parameters from SOAP connection
    $phoneNumber = $formdata['phoneNumber'];

    $conn = db();
    $rs = $conn->exec();

}



