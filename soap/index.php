<?php
$demo = array(
  "sam" => 98.42
);  
 
function getDemo($name) {
  global $demo;
  return $demo[$name];
}
 
ini_set("soap.wsdl_cache_enabled", "0"); // disabling WSDL cache
$server = new SoapServer("demo.wsdl");
$server->addFunction("getDemo");
$server->handle();