	/*----------------- start menuListeners -------------------*/
	var currModuleType 	= '';
	var prevModuleType 	= '';
    var sideType        = 'none';
    var prevURL         = '';
	var loadURL			= '';
	var menuOptionsArr	= new Array();
	var menuOptionID	= 0;
	var submenuOptionID	= -1;
    

    var touchDevice = 0;
    function checkIfTouchDevice(){
        touchDevice = !!("ontouchstart" in window) ? 1 : 0;
        if( touchDevice == 1 ){
            var elem = document.getElementById("template-wrapper");
            elem.addEventListener('touchmove', function(event) {
              if(currModuleType != "slideshow"){
                event.preventDefault();
              }
            }, false);
        }
        //mobileConsole( "Screen:", $(window).width() + "x" + $(window).height(), false, true );
        prepareTemplate();
    }

	function storeMenuArr(){
       var extraWidth = $(".template-menu").width() + $("#menu-hider").width();
       $("#template-menu").children().each(function(index, element){
             var menu = $(element),
                 subOptArr = "null",
                 subHol = "null",
                 menOptText = $(".menu-option-text a", menu);
             menuOptionsArr[ index ]         = [];
             menuOptionsArr[ index ][ 0 ]    = menu;

             if( menu.attr("data-module-type") == undefined ){
                var subMenu = [];
                subHol = menu.find(".sub-menu-holder");
                subHol.children().each(function(index, elem){
                    var submenu = $(elem),
                        subOptTxt = $(".sub-menu-option-text a", this),
                        subOptHref = String(subOptTxt.attr("href")),
                        subOptHrefPath = subOptTxt.attr("data-path-href");
                    subMenu[ index ] = [];
                    subMenu[ index ][ 0 ] = urlCharDeeplink + menOptText.text().toLowerCase() + "/" + subOptHref.replace(urlCharDeeplink,"");
                    subMenu[ index ][ 1 ] = [submenu.attr("data-module-type"), submenu.attr("data-side"),  subOptHref.replace(urlCharDeeplink,""), subOptHrefPath];
                    subMenu[ index ][ 2 ] = $(".sub-menu-option-background", this);
                    subMenu[ index ][ 3 ] = subOptTxt;
                });
                subOptArr = subMenu;
                menuOptionsArr[ index ][ 1 ] = "null";
             }
             else{
                menuOptionsArr[ index ][ 1 ]    = [menu.attr("data-module-type"), menu.attr("data-side"),  String(menOptText.attr("href")).replace(urlCharDeeplink,""), menOptText.attr("data-path-href")];
             }
             menuOptionsArr[ index ][ 2 ]    = $(".menu-option-background", menu);
             menuOptionsArr[ index ][ 3 ]    = ( subOptArr == "null" ) ? [menOptText] : [menOptText, $(".menu-option-text div", menu)];
             menuOptionsArr[ index ][ 4 ]    = extraWidth - parseInt( menOptText.css("padding-left"), 10);
             menuOptionsArr[ index ][ 5 ]    = subHol;
             menuOptionsArr[ index ][ 6 ]    = subOptArr;
             if( touchDevice == 0 )menu.bind("mouseenter mouseleave", {idx: index}, menuOptionHover);
       });
	}

    function menuOptionIn( idx1, idx2 ){
        menuOptionsArr[ idx1 ][ 2 ].attr("class", "menu-option-background-selected");
        TweenMax.to( menuOptionsArr[ idx1 ][ 2 ], menuAnimDuration, { css:{marginLeft: "0px", width: menuWidth}, ease:menuAnimEase });
		TweenMax.to( menuOptionsArr[ idx1 ][ 3 ], menuAnimDuration, { css:{color: menuTextOverColor}, ease:menuAnimEase });
        if( idx2 != -1 ){
            var subMenu = menuOptionsArr[ idx1 ][ 6 ];
            subMenu[ idx2 ][ 2 ].attr("class", "sub-menu-option-background-selected");
            TweenMax.to( subMenu[ idx2 ][ 2 ], menuAnimDuration, { css:{marginLeft: "0px", width: menuWidth}, ease:menuAnimEase });
            TweenMax.to( subMenu[ idx2 ][ 3 ], menuAnimDuration, { css:{color: menuTextOverColor}, ease:menuAnimEase });
        }
    }
    function menuOptionOut( idx1, idx2, disableIdx1){
        if( disableIdx1 == undefined){
            menuOptionsArr[ idx1 ][ 2 ].attr("class", "menu-option-background");
            TweenMax.to( menuOptionsArr[ idx1 ][ 2 ], menuAnimDuration, { css:{marginLeft: menuWidth, width: "0px"}, ease:menuAnimEase });
            TweenMax.to( menuOptionsArr[ idx1 ][ 3 ], menuAnimDuration, { css:{color: menuTextOutColor}, ease:menuAnimEase });
        }
        if( idx2 != -1 ){
            var subMenu = menuOptionsArr[ idx1 ][ 6 ];
            subMenu[ idx2 ][ 2 ].attr("class", "sub-menu-option-background");
            TweenMax.to( subMenu[ idx2 ][ 2 ], menuAnimDuration, { css:{marginLeft: menuWidth, width: "0px"}, ease:menuAnimEase });
            TweenMax.to( subMenu[ idx2 ][ 3 ], menuAnimDuration, { css:{color: menuTextOutColor}, ease:menuAnimEase });
        }
    }
    function setMenuData( val ){
        oldMenuData = val;
        currModuleType  = val[0];
        sideType        = val[1];
        url             = val[2];
        $("#template-menu").attr("data-current-module-type", val[0]);
        $("#template-menu").attr("data-side",  val[1]);
        $("#template-menu").attr("data-href",  val[2]);
        endPreviousModule = false;
    }
    function menuOptionHover( event ){
       var idx = event.data.idx,
           subMenuHol = menuOptionsArr[ idx ][ 5 ];
       if( event.type == "mouseenter" ){
            menuOptionsArr[ idx ][ 3 ][ 0 ].css("width", menuOptionsArr[ idx ][ 4 ]);
            TweenMax.to( menuOptionsArr[ idx ][ 2 ], menuAnimDuration, { css:{marginLeft: "0px", width: menuWidth}, ease:menuAnimEase });
			TweenMax.to( menuOptionsArr[ idx ][ 3 ], menuAnimDuration, { css:{color: menuTextOverColor}, ease:menuAnimEase });

            if( subMenuHol != "null" ){
                subMenuHol.css( 'height', '').css( 'width', '');
                var initialHeight = subMenuHol.css( 'height'),
				    initialWidth  = subMenuHol.css( 'width');
				subMenuHol.css( 'width', '0px' ).css( 'height', '0px' );
				TweenMax.to( subMenuHol, menuAnimDuration, { css:{height:initialHeight, width:initialWidth}, delay:0.2, ease:menuAnimEase, onStart:
                    function(){
                        subMenuHol.css( 'opacity', '1' ).css( 'display', 'block' );
                    }
                 });
            }
       }
       else{
            menuOptionsArr[ idx ][ 3 ][ 0 ].css("width", "");
            if( menuOptionsArr[ idx ][ 2 ].hasClass('menu-option-background-selected') == false ){
                TweenMax.to( menuOptionsArr[ idx ][ 2 ], menuAnimDuration, { css:{marginLeft: menuWidth, width: "0px"}, ease:menuAnimEase });
			    TweenMax.to( menuOptionsArr[ idx ][ 3 ], menuAnimDuration, { css:{color: menuTextOutColor}, ease:menuAnimEase });
            }
            if( subMenuHol != "null" ){
                subMenuHol.css( 'overflow', '' );
			    TweenMax.killTweensOf( subMenuHol );
			    TweenMax.to( subMenuHol, menuAnimDuration, { css:{height:"0px", width:"0px" }, ease:menuAnimEase, onComplete:hideSubmenu, onCompleteParams:[subMenuHol]});
            }
       }
    }
    function hideSubmenu( obj ){ obj.css( 'opacity', '0' ).css( 'display', 'none' ); }

	var menuAnimEase       = Quad.easeOut; /* Circ.easeOut  Quad.easeOut*/
    var menuAnimDuration   = 0.4; /* 0.6  or  0.3*/
    var menuWidth = 0;
    var submenuWidth = 0;
    var oldMenuData        = "";
    var menuData           = "";
	function menuListeners(){
        menuWidth = $(".template-menu").width() + "px";
        /* We add 2 px in order to fix the 2px margin on the right. Since sub menu holder */
        /* has overflow hidden the 2px will fill the gap in IE 8 and in the other browser it won't be shown. */
        submenuWidth = $(".template-menu").width() + 2 + "px";
		/* MENU & SUBMENU -- OVER & OUT LISTENER */

		function hideSubmenu( obj ){ obj.css( 'opacity', '0' ).css( 'display', 'none' ); }
        var submOptBackSel = "sub-menu-option-background-selected";
		if( touchDevice == 0 )$(".sub-menu-option-holder").hover(
    		function(){
    		    var submOptBack = $(".sub-menu-option-background", this);
    			var elem = submOptBack.length == 1  ? submOptBack : $("." + submOptBackSel, this);
    			TweenMax.to( elem, menuAnimDuration, { css:{marginLeft:"0px", width: submenuWidth}, ease:menuAnimEase });
    			TweenMax.to( $(".sub-menu-option-text a", this), menuAnimDuration, { css:{color: menuTextOverColor}, ease:menuAnimEase });
    		},
    		function(){
    			if( $('div:first', this ).hasClass(submOptBackSel) == false ){
    			    var submOptBack = $(".sub-menu-option-background", this);
    				var elem = submOptBack.length == 1  ? submOptBack : $("." + submOptBackSel, this);
    				TweenMax.to( elem, menuAnimDuration, { css:{marginLeft: submenuWidth, width:"0px"}, ease:menuAnimEase });
    				TweenMax.to( $(".sub-menu-option-text a", this), menuAnimDuration, { css:{color: menuTextOutColor}, ease:menuAnimEase });
    			}
	   	});
		// MENU & SUBMENU -- CLICK LISTENER
        $(".menu-option-holder").click(
    		function(event){
    		    event.preventDefault();
    		    var idx =  $(".menu-option-holder").index(this);
                if( menuOptionsArr[ idx ][ 1 ][ 0 ] == "external" ){
                    window.open(menuOptionsArr[ idx ][ 1 ][2]);
                    return;
                }
                if( touchDevice == 1 ){
                    if( menuOptionsArr[ idx ][ 6 ] != "null" ){
                        if( touchMenuID != -1 && touchMenuID != idx ){
                            menuOptionHover({data:{idx:touchMenuID}, type:"mouseleave"});
                        }
                        menuOptionHover({data:{idx:idx}, type:"mouseenter"});
                        touchMenuID = idx;
                        if( touchRemoveOn == false ){
                            touchRemoveOn = true;
                            var moduleContainer = document.getElementById("module-container");
                            moduleContainer.addEventListener("touchstart", touchContainer, false);
                        }
                    }
                    else{
                        if( touchMenuID != -1 && touchMenuID != idx ){
                            menuOptionHover({data:{idx:touchMenuID}, type:"mouseleave"});
                            touchMenuID = -1;
                        }
                    }
                }

    			if( menuOptionID != idx && $(this).attr("data-module-type") != undefined && $(this).attr("data-module-type") != urlCharDeeplink ){
                    if(loadedContent == false )return;
                    menuOptionsArr[ idx ][ 2 ].attr("class", "menu-option-background-selected");
                    menuOptionOut( menuOptionID, submenuOptionID );
                    menuOptionID = idx;
                    submenuOptionID = -1;

                    if( touchDevice == 1){
                       menuOptionIn( menuOptionID, submenuOptionID );
                    }

                    var hashURL = urlCharDeeplink + menuOptionsArr[ idx ][ 1 ][ 2 ];
                    menuData = menuOptionsArr[ idx ][ 1 ];
                    $(window).unbind('hashchange', onTemplateHashChange);
                    window.location.hash = hashURL;
                    clearCustomInterval( delayInterval );
                    delayInterval = setInterval(function(){
                        menuOptionClicked(  menuData[2],
                                            menuData[0],
                                            menuData[1],
                                            menuData[3]);
                        clearCustomInterval( delayInterval );
                        $(window).bind('hashchange', onTemplateHashChange);
                    }, 400);
    			}
		});


		subCloseInterval = "";
		$(".sub-menu-holder .sub-menu-option-holder").click(
		function(event){
		    event.preventDefault();
			var submenuParent 	= $(this).parent().get(0);
			var menuParent		= $(submenuParent).parent().get(0);
			currMenuOptionID 	= $(menuParent).index();
            var submenuOptIdx   = $(this).index();
            var subMenu         = menuOptionsArr[ currMenuOptionID ][ 6 ];
            if( subMenu[ submenuOptIdx ][ 1 ][ 0 ] == "external" ){
                window.open(subMenu[ submenuOptIdx ][ 1 ][ 2 ]);
                return;
            }
			if( submenuOptionID == submenuOptIdx && menuOptionID == currMenuOptionID){ return false; }
			else{
			    if(loadedContent == false ){return;}
                var subMenu = menuOptionsArr[ currMenuOptionID ][ 6 ];
			    menuOptionsArr[ currMenuOptionID ][ 2 ].attr("class", "menu-option-background-selected");
                subMenu[ submenuOptIdx ][ 2 ].attr("class", "sub-menu-option-background-selected");

                if( touchDevice == 1 ){
                    menuOptionIn(currMenuOptionID, submenuOptIdx);
                    clearCustomInterval( subCloseInterval );
                    subCloseInterval = setInterval(function(){
                        touchContainer();
                        clearCustomInterval( subCloseInterval );
                    }, 200);

                }

                var disableIdx1 = undefined;
                if(menuOptionID == currMenuOptionID){ disableIdx1 = true; }
                menuOptionOut( menuOptionID, submenuOptionID, disableIdx1 );
                menuOptionID = currMenuOptionID;
                submenuOptionID = submenuOptIdx;

                var hashURL = subMenu[ submenuOptIdx ][ 0 ] ;

                menuData = subMenu[ submenuOptIdx ][ 1 ];
                $(window).unbind('hashchange', onTemplateHashChange);
                window.location.hash = hashURL;

                clearCustomInterval( delayInterval );
                delayInterval = setInterval(function(){
                    menuOptionClicked(  menuData[2],
                                        menuData[0],
                                        menuData[1],
                                        menuData[3]);
                    clearCustomInterval( delayInterval );
                    $(window).bind('hashchange', onTemplateHashChange);
                }, 400);
			}
			event.stopPropagation();
		});
	}

    var touchRemoveOn = false;
    var touchMenuID = -1;
    function touchContainer(){
        touchRemoveOn = false;
        if( touchMenuID != -1 &&  menuOptionsArr[ touchMenuID ][ 6 ] != "null" ){
            var evt = {data:{idx:touchMenuID}, type:"mouseleave"};
            menuOptionHover(evt);
        }
        var moduleContainer = document.getElementById("module-container");
        moduleContainer.removeEventListener("touchstart", touchContainer, false);
    }
    function urlChanged(){
        loadedContent = true;
        menuOptionClicked(  menuData[2], menuData[0], menuData[1], menuData[3]);
    }
    function isOtherURL(){
        var val = ( menuData != "" && menuData[2] != prevURL ) ? true : false;
        return val;
    }
    function menuOptionClicked( val, mType, sType, hrefPath ){
		if( val != urlCharDeeplink ){
			var url = '';
			if( $("#template-menu").attr("data-current-module-type")  == "slideshow" ){deleteSlideshowTimer();}
            if( $("#template-menu").attr("data-current-module-type")  == "home2" ){deleteBannerTimer();}
			currModuleType   = mType;
            sideType         = sType;
            hrefPath         = (hrefPath == undefined) ? "" : hrefPath;
            setMobileMenuOption(val);
			url              = templateBaseURL + hrefPath + val.replace(urlCharDeeplink,'');

            if( prevURL == '' ){prevURL = url;}
            else{prevURL = loadURL;}
            loadURL = url;

            stopCurrentLoading();
			if( endModuleFunction != null ){
                delayAnimationLoading = 0.3;
                if( moduleEnd == true ){
                    moduleEnd = false;
                    endModuleFunction();
                }
            }
            else{delayAnimationLoading = 0.1;}

            if(menuData[2] != oldMenuData[2] ){
                stopAnimBack( false );
                loadedContent = true;
                activateAnimationLoading();
            }
            else{
                loadedContent =  true;
                var loadAnim = $("#loading-animation");
                if(loadAnim.length > 0 ){ TweenMax.to( loadAnim, .3, { css:{right:"-104px"}, ease:Circ.easeOut }); }
                if(endModuleFunction == null){

                   switch( menuData[0] ){
                        case "news":
                            var textPageInstanceHolder    = $( txt_modCont);
                            TweenMax.to(  textPageInstanceHolder, .6, { css:{left: "0px" }, delay:0.1,  ease:Circ.easeInOut   });/*get_OffsetWidth() +*/
                    		endModuleFunction = endModuleTextPage;
                            moduleEnd = true;
                            startOn = true;
                            startAnimBack();
                            break;
                        case "text_page":
                            moduleTextPage();
                            startOn = true;
                            startAnimBack();
                            break;
                        case "gallery":
                            endModuleGallery(true);
                            endModuleFunction =  endModuleGallery;
                            startOn = true;
                            startAnimBack();
                            break;
                        case "showreel":
                            reverseEndModuleShowreel();
                            startOn = true;
                            startAnimBack();
                            break;
                   }
                }
            }
		}
	}
    function stopCurrentLoading(){
        if( loadInterval != ""){
            clearInterval( loadInterval );
            loadInterval = "";
        }
        if( showModuleInterval != ""){
            clearInterval( showModuleInterval );
            showModuleInterval = "";
        }
    }
    var delayAnimationLoading = 0.3;
	function activateAnimationLoading(){
	    var loadAnim = $("#loading-animation");
        TweenMax.killTweensOf( loadAnim );
		loadAnim.css("right", "-104px" ).css("display", "inline").css("visibility", "visible" );
		if(loadAnim.length > 0 ){ TweenMax.to( loadAnim, .3, { css:{right:"0px"}, delay: delayAnimationLoading, ease:Circ.easeOut, onComplete: doLoad }); }
		else{doLoad();}
	}
    var loadInterval = ""
	function doLoad(){
	   if( loadedContent == true ){
           var toLoad = loadURL;
           clearCustomInterval( loadInterval );
    	   var modGallPrev = $("#module-galleries-preview");
           var fullWidPrev = $("#full-width-preview");
    	   if( modGallPrev.length > 0 ){ modGallPrev.remove(); }
           if( fullWidPrev.length > 0 ){ fullWidPrev.remove(); }

            loadedContent = false;
            loadInterval = setInterval(function(){
                loadModule( toLoad )
                clearCustomInterval( loadInterval );
            }, 50);
       }
	}
	/*------------------ end menuListeners --------------------*/